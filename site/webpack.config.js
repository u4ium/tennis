const path = require("path");
const RealFaviconPlugin = require("real-favicon-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const NodeExternals = require("webpack-node-externals");
const RobotstxtPlugin = require("robotstxt-webpack-plugin");

const dir = "dist";

module.exports = function (env, argv) {
    // default to the server configuration
    const base = {
        mode: process.env.NODE_ENV,
        watch: process.env.NODE_ENV === "development",
        watchOptions: {
            aggregateTimeout: 200,
            ignored: /node_modules/,
        },
        cache: true,
        //cache: {
        //    type: "filesystem",
        //    name: env.platform,
        //},
        output: {
            // path needs to be an ABSOLUTE file path
            path: path.resolve(process.cwd(), dir),
            publicPath: "/",
        },
        // Enable sourcemaps for debugging webpack's output.
        devtool: "source-map",
        resolve: {
            // Add ".ts" and ".tsx" as resolvable extensions.
            extensions: [".ts", ".tsx", ".js", ".json"],
        },
        module: {
            rules: [
                // Lint and fix all Typescript files with ts-lint
                {
                    test: /\.tsx?$/,
                    enforce: "pre",
                    use: [
                        {
                            loader: "tslint-loader",
                            options: {
                                fix: true,
                            }
                        },
                    ]
                },
                // All files with a ".ts" or ".tsx" extension will be handled by "ts-loader".
                {
                    test: /\.tsx?$/,
                    use: [
                        { loader: "ts-loader", },
                    ],
                },
            ],
        },
    };

    // server-specific configuration
    if (env.platform === "server") {
        base.entry = "./src/server/server.ts";
        base.output.filename = "js/server.js";
        base.target = "node";
        base.externals = [NodeExternals()];
    }

    // client-specific configurations
    if (env.platform === "web") {
        const publicDir = dir + "/public";
        base.plugins = [
            new HtmlWebpackPlugin({
                meta: {
                    viewport: "width=device-width, initial-scale=1, shrink-to-fit=no",
                    description: "A Tennis website. Find players, organize matches, record scores, send messages, track progress and more.",
                },
                template: "!!raw-loader!src/index.ejs",
                filename: ".template.ejs",
                title: "Tennis Ladder",
                scriptLoading: "defer",
                minify: "auto",
            }),
            new RealFaviconPlugin({
                faviconJson: "favicon.json",
                outputPath: publicDir + "/favicon",
                inject: true,
            }),
            new MiniCssExtractPlugin({
                filename: "styles.css",
            }),
            new RobotstxtPlugin({
                filePath: "robots.txt",
                host: `${process.env.SERVER_PROTOCOL}://${process.env.SERVER_ADDRESS}:${process.env.SERVER_HTTP_PORT}`,
                policy: [
                    {
                        userAgent: "Googlebot",
                        allow: ["/", "/about", "/login", "/privacy"],
                    },
                ],
            })
        ];
        base.output.path = path.resolve(process.cwd(), publicDir);
        base.entry = "./src/app/entry.tsx";
        base.output.filename = "js/client.js";

        base.resolve.extensions.push("css");
        base.module.rules.push(
            {
                test: /\.css$/,
                use: [
                    { loader: MiniCssExtractPlugin.loader },
                    { loader: "css-loader" }
                ]
            }
        );
    }

    return base;
};

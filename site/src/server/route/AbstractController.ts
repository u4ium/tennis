import { Logger } from "@overnightjs/logger";
import DB from "../database/db";
import EmailServer from "../email/EmailServer";

export default abstract class AbstractController {
    protected db: DB;
    protected logger: Logger;
    protected children: AbstractController[] = [];
    protected emailServer: EmailServer;

    protected initialize(db: DB, emailServer: EmailServer, logger: Logger) {
        this.db = db;
        this.emailServer = emailServer;
        this.logger = logger;
        this.children.forEach((child) =>
            child.initialize(db, emailServer, logger)
        );
    }
}

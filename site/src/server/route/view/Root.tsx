import {
    ChildControllers,
    ClassErrorMiddleware,
    ClassOptions,
    Controller,
    Get,
    Middleware,
} from "@overnightjs/core";
import { Logger } from "@overnightjs/logger";
import { Request, Response } from "express";
import { Query } from "express-serve-static-core";
import React from "react";
import { renderToString } from "react-dom/server";
import { StaticRouter, StaticRouterContext } from "react-router";
import validator from "validator";
import App from "../../../app/App";
import {
    allViews,
    AppProps,
    AuthUserProps,
    defaultView,
    View,
} from "../../../app/props";
import { ADMIN_EMAIL } from "../../../config";
import DB from "../../database/db";
import EmailServer from "../../email/EmailServer";
import HTTPStatus from "../../util/HTTPStatus";
import AbstractController from "../AbstractController";
import APIController from "../api/APIController";
import { checkQuery } from "../middleware/checkQuery";
import { checkView } from "../middleware/checkView";
import { handleError, pageNotFound } from "../middleware/errorHandling";

interface InviteQuery {
    email: string;
    code: string;
}

const isInviteQuery = (query: Query | InviteQuery): query is InviteQuery => {
    if (query === undefined || query === null) {
        throw new HTTPStatus(
            400,
            "Must provide URL query of the form ?email=EMAIL&code=CODE"
        );
    } else if (
        typeof query.email !== "string" ||
        !validator.isEmail(query.email)
    ) {
        throw new HTTPStatus(400, "Invalid email");
    } else if (
        typeof query.code !== "string" ||
        !validator.isHexadecimal(query.code)
    ) {
        throw new HTTPStatus(400, "Invalid code (must be hexadecimal)");
    } else {
        return true;
    }
};

const children = [new APIController()];
@Controller("")
@ClassOptions({ mergeParams: true })
@ClassErrorMiddleware([handleError, pageNotFound])
@ChildControllers(children)
export default class Root extends AbstractController {
    constructor(
        databaseConnection: DB,
        emailServer: EmailServer,
        logger: Logger
    ) {
        super();
        this.children = children;
        this.initialize(databaseConnection, emailServer, logger);
    }

    @Get("")
    protected getIndex(req: Request, res: Response) {
        return res.redirect(`/${defaultView}`);
    }

    @Get("invite")
    @Middleware(checkQuery<InviteQuery>(isInviteQuery))
    protected async getInvite(
        req: Request<null, null, null, InviteQuery>,
        res: Response<string, { inviteEmail: string }>
    ) {
        await this.db.api.invite.confirmInvite(
            req.query.email,
            res.locals.clubId,
            req.query.code
        );
        req.session.inviteEmail = req.query.email;
        return res.redirect("/about");
    }

    @Get(`:view(${allViews.join("|")})/:subView?/?*`) // TODO: restrict subviews
    @Middleware(checkView)
    protected async getViews(
        req: Request<{ view: View }, null, null, InviteQuery | null>,
        res: Response<string>
    ) {
        const clubId = res.locals.clubId;
        const userId = req.session?.userID;
        let appProps;
        if (req.session && userId) {
            try {
                const user: AuthUserProps = await this.db.api.user.getUser(
                    clubId,
                    userId
                );
                const matches = await this.db.api.match.getMyMatches(clubId,
                    userId);
                appProps = await this.getAppProps(clubId, { user, matches });
            } catch (err) {
                this.logger.err(err.message);
                appProps = await this.getAppProps(clubId, {});
            }
        } else {
            appProps = await this.getAppProps(clubId, {
                inviteEmail: req.session?.inviteEmail || ""
            });
        }
        const context: StaticRouterContext = {};
        const reactApp = renderToString(
            <StaticRouter location={req.url} context={context}>
                <App {...appProps} />
            </StaticRouter>
        );
        if (context.url) {
            return res.redirect(301, context.url);
        } else {
            return res.render(".template", {
                reactApp,
                appProps,
            });
        }
    }

    private async getAppProps(
        clubId: number,
        props: Partial<AppProps>
    ): Promise<AppProps> {
        const constants = {
            ADMIN_EMAIL
        };
        const { user, inviteEmail, matches } = props;

        const clubProps = await this.db.api.club.getClub(clubId);
        const rules = await this.db.api.rule.getAllRules();
        const leagues = await this.db.api.league.getActiveLeagues(clubId);

        return {
            ...constants,
            ...clubProps,
            user,
            inviteEmail,
            rules,
            leagues,
            matches,
        };
    }
}

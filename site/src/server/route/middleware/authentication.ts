import { NextFunction, Request, Response } from "express";

export function checkAuthenticated(
    req: Request,
    res: Response,
    next: NextFunction
) {
    return req.session && req.session.userID
        ? next()
        : res.status(401).end("Access Denied");
}

export function checkAdministrator(
    req: Request,
    res: Response,
    next: NextFunction
) {
    return req.session && req.session.isAdministrator
        ? next()
        : res.status(403).end("Access Denied");
}

export function checkOwnerOrAdministrator(
    req: Request,
    res: Response,
    next: NextFunction
) {
    return req.session &&
        (req.session.isAdministrator ||
            req.session.userID === Number(req.params.id))
        ? next()
        : res.status(403).end("Access Denied");
}

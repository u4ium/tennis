import { NextFunction, Request, Response } from "express-serve-static-core";

/**
 * A middleware to check if GET request's body is an instance of a specific
 * interface.
 *
 * @param verify The type guard function to check body's interface. Throws an
 *               Error if body is invalid.
 */
export function checkBody<B>(verify: (body: B) => body is B) {
    return (req: Request<{}, {}, B>, res: Response, next: NextFunction) => {
        try {
            verify(req.body);
            return next();
        } catch (err) {
            return next(err);
        }
    };
}

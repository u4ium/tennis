import {
    NextFunction,
    ParamsDictionary,
    Request,
    Response,
} from "express-serve-static-core";
import HTTPStatus from "../../util/HTTPStatus";

export interface CustomResponse<T> extends Response {
    json: (body: T) => this;
}

export interface IdParams extends ParamsDictionary {
    id: string;
}

/**
 * A middleware to check if a GET request's Params is an instance of a specific
 * interface.
 *
 * @param isType The type guard function to check the Params interface. Throws
 *               an Error if params is invalid.
 */
export function checkParams<P = IdParams>(
    verify: (params: P | ParamsDictionary) => params is P
) {
    return (
        req: Request<P | ParamsDictionary>,
        res: Response,
        next: NextFunction
    ) => {
        if (!req.params) {
            return next(new HTTPStatus(400, "Missing Params in request."));
        }
        try {
            verify(req.params);
            return next();
        } catch (err) {
            return next(err);
        }
    };
}

export function makeIsParams<T extends ParamsDictionary>(
    name: string
) {
    return (params: ParamsDictionary): params is T => {

        if (params === undefined ||
            params === null ||
            Object.keys(params).length === 0
        ) {
            throw new HTTPStatus(400, `Must provide ${name} ID parameter`);
        } else if (typeof params.id !== "string" || !Number(params.id)) {
            throw new HTTPStatus(400, `${name} ID parameter must be a number`);
        }
        return true;
    };
}

export const isIdParams = (n: string) => makeIsParams<IdParams>(n);

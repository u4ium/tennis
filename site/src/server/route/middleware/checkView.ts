import { NextFunction, Request, Response } from "express";
import { defaultView, isUnauthorizedView } from "../../../app/props";

export function checkView(req: Request, res: Response, next: NextFunction) {
    const isViewAvailable = req.session && req.session.userID ||
        isUnauthorizedView(req.params.view);
    if (isViewAvailable) { return next(); }
    return res.redirect(`/${defaultView}`);
}

import {
    NextFunction,
    Query,
    Request,
    Response,
} from "express-serve-static-core";

/**
 * A middleware to check if a GET request's query is an instance of a specific
 * interface.
 *
 * @param verify The type guard function to check query's interface.
 *               Throws an Error if query is invalid.
 */
export function checkQuery<Q>(verify: (query: Q | Query) => query is Q) {
    return (
        req: Request<{}, any, any, Q | Query>,
        res: Response,
        next: NextFunction
    ) => {
        try {
            verify(req.query);
            return next();
        } catch (err) {
            return next(err);
        }
    };
}

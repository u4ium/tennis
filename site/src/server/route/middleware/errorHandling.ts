import { NextFunction, Request, Response } from "express";
import {
    ConnectionError,
    DatabaseError,
    UniqueConstraintError,
    ValidationError
} from "sequelize";
import HTTPStatus from "../../util/HTTPStatus";

export function handleError(
    err: Error,
    req: Request,
    res: Response,
    next: NextFunction
) {
    if (err instanceof UniqueConstraintError) {
        const messages: string = err.errors.map((e) => e.message).join(", ");
        return res.status(409).send(messages);
    } else if (err instanceof ValidationError) {
        return res.status(400).send(err.message);
    } else if (err instanceof HTTPStatus) {
        return res.status(err.code).send(err.message);
    } else if (err instanceof DatabaseError || err instanceof ConnectionError) {
        return res.status(500).send("Database Failure");
    } else if (err instanceof Error) {
        return res.status(400).send(err.message);
    } else {
        return res.status(500).send("Unknown Server Error");
    }
}

export function pageNotFound(
    err: Error,
    req: Request,
    res: Response,
    next: NextFunction
) {
    return res.status(400).send("Page not found");
}

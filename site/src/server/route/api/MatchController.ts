import {
    ChildControllers,
    ClassOptions,
    Controller,
    Get,
    Middleware,
    Post,
    Put
} from "@overnightjs/core";
import { Request, Response } from "express";
import {
    IDoublesMatch,
    ISinglesMatch
} from "../../../common/model/match/MatchModel";
import AbstractController from "../AbstractController";
import { checkAuthenticated } from "../middleware";
// import { checkBody } from "../middleware/checkBody";
import { checkParams, IdParams, isIdParams } from "../middleware/checkParams";

const children: AbstractController[] = [];

@Controller("match")
@ClassOptions({ mergeParams: true })
@ChildControllers(children)
export default class MatchController extends AbstractController {
    constructor() {
        super();
        this.children = children;
    }

    @Get("")
    @Middleware(checkAuthenticated)
    protected async getMyMatches(req: Request, res: Response) {
        return res.end(); // TODO
    }

    @Post("singles")
    // TODO: @Middleware(checkBody<ISinglesMatch>(isSinglesMatch))
    protected async addSinglesMatch(
        req: Request<{}, {}, ISinglesMatch>,
        res: Response<{ id: number }>
    ) {
        const match = req.body;
        return res.json({
            id: await this.db.api.match.
                addSinglesMatch(res.locals.clubId, req.session.userID, match)
        });
    }

    @Put("singles/:id")
    @Middleware(checkParams(isIdParams("Singles Match")))
    // TODO: @Middleware(checkBody<ISinglesMatch>(isSinglesMatch))
    protected async recordSinglesMatch(
        req: Request<IdParams, ISinglesMatch>,
        res: Response<null>
    ) {
        const match = req.body;
        const id = Number(req.params.id);
        await this.db.api.match.
            recordSinglesMatch(id, req.session.userID, match);
        return res.status(204).send();
    }

    @Post("doubles")
    // TODO: @Middleware(checkBody<IDoublesMatch>(isDoublesMatch))
    protected async addDoublesMatch(
        req: Request<{}, {}, IDoublesMatch>,
        res: Response<{ id: number }>
    ) {
        const match = req.body;
        return res.json({
            id: await this.db.api.match.
                addDoublesMatch(res.locals.clubId, req.session.userID, match)
        });
    }

    @Put("doubles/:id")
    @Middleware(checkParams(isIdParams("Doubles Match")))
    // TODO: @Middleware(checkBody<IDoublesMatch>(isDoublesMatch))
    protected async recordDoublesMatch(
        req: Request<IdParams, {}, IDoublesMatch>,
        res: Response<null>
    ) {
        const match = req.body;
        const id = Number(req.params.id);
        await this.db.api.match.
            recordDoublesMatch(id, req.session.userID, match);
        return res.status(204).send();
    }

}

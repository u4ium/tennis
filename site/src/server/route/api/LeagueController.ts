import {
    ChildControllers,
    ClassOptions,
    Controller,
    Get,
    Middleware,
    Post,
    Put
} from "@overnightjs/core";
import { Request, Response } from "express";
import ILeague, {
    extractLeague,
    isNewLeague
} from "../../../common/model/league/LeagueModel";
import { ISeason } from "../../../common/model/season/Season";
import AbstractController from "../AbstractController";
import { checkAuthenticated } from "../middleware";
import { checkBody } from "../middleware/checkBody";
import { checkParams, IdParams, isIdParams } from "../middleware/checkParams";

const children: AbstractController[] = [];

@Controller("league")
@ClassOptions({ mergeParams: true })
@ChildControllers(children)
export default class LeagueController extends AbstractController {
    constructor() {
        super();
        this.children = children;
    }

    @Get("")
    @Middleware(checkAuthenticated)
    protected async getActiveLeagues(req: Request, res: Response) {
        return res.json({
            id: await this.db.api.league
                .getActiveLeagues(res.locals.clubId)
        });
    }

    @Post("")
    @Middleware(checkAuthenticated) // TODO: check isMod
    @Middleware(checkBody<ILeague>(isNewLeague))
    protected async postNewLeague(
        req: Request<null, null, ILeague>,
        res: Response<{ id: number }>
    ) {
        const league = extractLeague(req.body); // TODO: validate, extract Rule
        return res.json({
            id: await this.db.api.league.addLeague(
                res.locals.clubId,
                req.session.userID,
                league
            )
        });
    }

    @Put("singles/:id/player")
    @Middleware(checkAuthenticated)
    @Middleware(checkParams(isIdParams("Singles League")))
    protected async joinSinglesLeague(
        req: Request<IdParams>,
        res: Response<{ id: number }>
    ) {
        const id = await this.db.api.league.joinSinglesLeague(
            res.locals.clubId,
            Number(req.params.id),
            req.session.userID
        );
        return res.json({ id });
    }

    @Put("doubles/:id/player")
    @Middleware(checkAuthenticated)
    @Middleware(checkParams(isIdParams("Doubles League")))
    protected async joinDoublesLeague(
        req: Request<IdParams>,
        res: Response<{ id: number }>
    ) {
        const id = await this.db.api.league.joinDoublesLeague(
            res.locals.clubId,
            Number(req.params.id),
            req.session.userID
        );
        return res.json({ id });
    }

    @Post(":type(singles|doubles)/:id/season")
    @Middleware(checkAuthenticated)
    @Middleware(checkParams(isIdParams("League")))
    // TODO: @Middleware(checkLeagueOwnerOrAdmin)
    // TODO: @Middleware(checkBody<ISeason>(isNewSeason))
    protected async addSeason(
        req: Request<IdParams & { type: "singles" | "doubles" }, null, ISeason>,
        res: Response<{ id: number }>
    ) {
        // TODO: const season = extractSeason(req.body);
        const id = await this.db.api.season.addSeason(
            Number(req.params.id), req.params.type, req.body);
        return res.json({ id });

    }
}

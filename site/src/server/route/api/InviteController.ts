import { Controller, Delete, Middleware, Post, Put } from "@overnightjs/core";
import { Request, Response } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import validator from "validator";
import InviteEmail from "../../email/templates/InviteEmail";
import HTTPStatus from "../../util/HTTPStatus";
import AbstractController from "../AbstractController";
import { checkAdministrator, checkOwnerOrAdministrator } from "../middleware";
import { checkParams } from "../middleware/checkParams";

interface InviteParams {
    email: string;
}

const isInviteParams = (
    inviteParams: InviteParams | ParamsDictionary
): inviteParams is InviteParams => {
    if (!inviteParams) {
        throw new HTTPStatus(400, "Must provide Invite email parameter");
    } else if (!validator.isEmail(inviteParams.email)) {
        throw new HTTPStatus(400, "Invite email must be a valid email address");
    }
    return true;
};

@Controller("invite")
export default class InviteController extends AbstractController {
    @Post(":email")
    @Middleware(checkAdministrator)
    @Middleware(checkParams<InviteParams>(isInviteParams))
    protected async addInvite(req: Request<InviteParams>, res: Response<null>) {
        const emailAddress = req.params.email.toLowerCase().trim();
        const inviteCode: string = await this.db.api.invite.inviteUser(
            emailAddress, res.locals.clubId
        );
        // TODO: use club info
        // const club = await this.db.api.club.getClubInfo(res.locals.clubId);
        await this.emailServer.sendEmail(
            new InviteEmail(
                emailAddress,
                inviteCode,
                // club
            )
        );
        return res.status(204).send();
    }

    @Put()
    @Middleware(checkOwnerOrAdministrator)
    protected async confirmInvite(
        req: Request,
        res: Response<null>
    ) {
        await this.db.api.invite.confirmInviteAuthenticated(
            req.session.userID,
            res.locals.clubId
        );
        return res.status(204).send();
    }

    @Delete(":email")
    @Middleware(checkOwnerOrAdministrator)
    @Middleware(checkParams<InviteParams>(isInviteParams))
    protected async deleteInvite(
        req: Request<InviteParams>,
        res: Response<null>
    ) {
        const emailAddress: string = req.params.email.toLowerCase().trim();
        await this.db.api.invite
            .deleteInviteForUser(emailAddress, res.locals.clubId);
        return res.status(204).send();
    }
}

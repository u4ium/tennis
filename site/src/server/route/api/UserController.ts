import { Controller, Delete, Get, Middleware, Patch } from "@overnightjs/core";
import { Request, Response } from "express";
import { AuthUserProps } from "../../../app/props";
import {
    extractPatchUser,
    IPatchUser,
    isPatchUser,
} from "../../../common/model/user/PatchUser";
import AbstractController from "../AbstractController";
import { checkOwnerOrAdministrator } from "../middleware";
import { checkBody } from "../middleware/checkBody";
import { checkParams, IdParams, isIdParams } from "../middleware/checkParams";

@Controller("user")
export default class UserController extends AbstractController {
    @Get(":id")
    @Middleware(checkOwnerOrAdministrator)
    @Middleware(checkParams<IdParams>(isIdParams("user")))
    protected async getUser(
        req: Request<IdParams>,
        res: Response<AuthUserProps>
    ) {
        const user: AuthUserProps = await this.db.api.user.getUser(
            res.locals.clubId,
            Number(req.params.id)
        );
        return res.status(200).json(user);
    }

    @Patch(":id")
    @Middleware(checkOwnerOrAdministrator)
    @Middleware(checkParams<IdParams>(isIdParams("user")))
    @Middleware(checkBody<IPatchUser>(isPatchUser))
    protected async patchUser(
        req: Request<IdParams, {}, IPatchUser>,
        res: Response<null>
    ) {
        const user: IPatchUser = extractPatchUser(req.body);
        await this.db.api.user.modifyUser(Number(req.params.id), user);
        return res.status(204).send();
    }

    @Delete(":id")
    @Middleware(checkOwnerOrAdministrator)
    @Middleware(checkParams<IdParams>(isIdParams("user")))
    protected async deleteUser(
        req: Request<IdParams>,
        res: Response<null>
    ) {
        await this.db.api.user.deleteUser(Number(req.params.id));
        return res.status(204).send();
    }
}

import { Controller, Delete, Middleware, Post, Put } from "@overnightjs/core";
import { Request, Response } from "express";
import validator from "validator";
import { IUserP } from "../../../app/models/User";
import { AuthUserProps } from "../../../app/props";
import { extractSignUpUser } from "../../../common/model/user/SignUpUser";
import { validateUser } from "../../../common/validate";
import HTTPStatus from "../../util/HTTPStatus";
import ConnectedController from "../AbstractController";
import { checkAuthenticated } from "../middleware/authentication";
import { checkBody } from "../middleware/checkBody";

interface SignInBody {
    email: string;
    password: string;
}

const isSignInBody = (signInBody: SignInBody): signInBody is SignInBody => {
    if (signInBody === undefined ||
        signInBody === null ||
        Object.keys(signInBody).length === 0
    ) {
        throw new HTTPStatus(400, "Must provide email and password");
    } else if (!validator.isEmail(signInBody.email)) {
        throw new HTTPStatus(400, "Invalid email for sign in");
    } else if (typeof signInBody.password !== "string") {
        throw new HTTPStatus(400, "Invalid password for sign in");
    }
    return true;
};

@Controller("auth")
export default class AuthenticationController extends ConnectedController {
    @Put("")
    @Middleware(checkBody<SignInBody>(isSignInBody))
    protected async signIn(
        req: Request<null, null, SignInBody, null>,
        res: Response<AuthUserProps>
    ) {
        req.session.userID = await this.db.api.user.checkUserLogin(
            res.locals.clubId,
            req.body.email,
            req.body.password
        );
        return this.authenticate(req, res);
    }

    @Post("")
    @Middleware(
        checkBody<IUserP>((u: IUserP): u is IUserP => validateUser(u, false))
    )
    protected async signUp(
        req: Request<null, null, IUserP, null>,
        res: Response<AuthUserProps>
    ) {
        const user: IUserP = extractSignUpUser(req.body);
        req.session.userID = await this.db.api.user.addUser(
            res.locals.clubId, user);
        return this.authenticate(req, res);
    }

    @Delete("")
    protected signOut(
        req: Request<null, null, null, null>,
        res: Response<null | string>
    ) {
        if (req.session) {
            return req.session.destroy((err) => {
                if (err) {
                    this.logger.err(err, true);
                    return res.status(500).send("Server Failure");
                } else {
                    return res.status(204).end();
                }
            });
        } else {
            return res.status(204).end();
        }
    }

    @Middleware(checkAuthenticated)
    private async authenticate(
        req: Request<null, null, SignInBody, null>,
        res: Response<AuthUserProps>
    ) {
        const user: AuthUserProps = await this.db.api.user.getUser(
            res.locals.clubId,
            req.session.userID
        );
        req.session.isAdministrator = user.isAdministrator;
        req.session.inviteEmail = null;
        return res.json(user);
    }
}

import { ChildControllers, ClassOptions, Controller } from "@overnightjs/core";
import AbstractController from "../AbstractController";
import AdminController from "./AdminController";
import AuthenticationController from "./AuthenticationController";
import AvatarController from "./AvatarController";
import InviteController from "./InviteController";
import LeagueController from "./LeagueController";
import MatchController from "./MatchController";
import PlayerController from "./PlayerController";
import RuleController from "./RuleController";
import ScheduleController from "./ScheduleController";
import UserController from "./UserController";

const children: AbstractController[] = [
    new AuthenticationController(),
    new UserController(),
    new InviteController(),
    new AdminController(),
    new RuleController(),
    new LeagueController(),
    new ScheduleController(),
    new MatchController(),
    new PlayerController(),
    new AvatarController(),
];

@Controller("api")
@ClassOptions({ mergeParams: true })
@ChildControllers(children)
export default class APIController extends AbstractController {
    constructor() {
        super();
        this.children = children;
    }
}

import {
    ChildControllers,
    ClassOptions,
    Controller,
    Get,
    Middleware,
} from "@overnightjs/core";
import { Request, Response } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import validator from "validator";
import { IPlayerUser } from "../../../common/model/user/PlayerUser";
import HTTPStatus from "../../util/HTTPStatus";
import AbstractController from "../AbstractController";
import { checkAuthenticated } from "../middleware";
// import { checkBody } from "../middleware/checkBody";
import { checkParams } from "../middleware/checkParams";

interface INameParams extends ParamsDictionary {
    name: string;
}

const isNameParams = (params: ParamsDictionary): params is INameParams => {
    if (params === undefined ||
        params === null ||
        Object.keys(params).length === 0
    ) {
        throw new HTTPStatus(400, `Must provide Name search parameter`);
    } else if (typeof params.name !== "string") {
        throw new HTTPStatus(400, `Name search parameter must be a string`);
    } else if (params.name.length < 2) {
        throw new HTTPStatus(400, "Name search parameter must be >= 2 chars");
    } else if (!params.name.split(" ").reduce(
        (p, v) => p && validator.isAlpha(v), true) // TODO use Name regex
    ) {
        throw new HTTPStatus(400, "Name search parameter must be alphabetic");
    }
    return true;
};

const children: AbstractController[] = [];

@Controller("player")
@ClassOptions({ mergeParams: true })
@ChildControllers(children)
export default class PlayerController extends AbstractController {
    constructor() {
        super();
        this.children = children;
    }

    @Get("search/:name")
    @Middleware(checkParams<INameParams>(isNameParams))
    @Middleware(checkAuthenticated)
    protected async getMyMatches(
        req: Request<INameParams>,
        res: Response<IPlayerUser[]>
    ) {
        res.json(await this.db.api.user.findUserByName(
            res.locals.clubId,
            req.params.name
        ));
    }

}

import {
    ChildControllers,
    ClassOptions,
    Controller,
    Get,
    Middleware,
} from "@overnightjs/core";
import { Request, Response } from "express";
import { IAvatar } from "../../../common/model/avatar/Avatar";
import AbstractController from "../AbstractController";
import { checkParams, IdParams, isIdParams } from "../middleware/checkParams";
// import { checkAdministrator } from "../middleware";

const children: AbstractController[] = [];

@Controller("avatar")
@ClassOptions({ mergeParams: true })
@ChildControllers(children)
export default class AvatarController extends AbstractController {
    constructor() {
        super();
        this.children = children;
    }

    @Get(":id")
    @Middleware(checkParams<IdParams>(isIdParams("avatar")))
    protected async getAvatarByUserId(
        req: Request<IdParams>,
        res: Response<IAvatar>
    ) {
        res.json(await this.db.api.avatar.getAvatar(Number(req.params.id)));
    }

}

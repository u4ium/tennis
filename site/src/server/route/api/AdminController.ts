import { ChildControllers, ClassOptions, Controller, /*Middleware,*/ Put } from "@overnightjs/core";
import { Request, Response } from "express";
import AbstractController from "../AbstractController";
// import { checkAdministrator } from "../middleware";

const children: AbstractController[] = [];

@Controller("admin")
@ClassOptions({ mergeParams: true })
@ChildControllers(children)
export default class AdminController extends AbstractController {
    constructor() {
        super();
        this.children = children;
    }

    @Put("test-data")
    // @Middleware(checkAdministrator)
    protected async addTestData(req: Request, res: Response) {
        try {
            await this.db.deleteAllData();
            await this.db.loadTestData();
        } catch (error) {
            this.logger.warn(error);
        }
        res.status(200).send("Database cleared and test data inserted.");
    }
}

import { ChildControllers, ClassOptions, Controller, Get, Middleware } from "@overnightjs/core";
import { Request, Response } from "express";
import AbstractController from "../AbstractController";
import { checkAuthenticated } from "../middleware";

const children: AbstractController[] = [];

@Controller("rule")
@ClassOptions({ mergeParams: true })
@ChildControllers(children)
export default class RuleController extends AbstractController {
    constructor() {
        super();
        this.children = children;
    }

    @Get("")
    @Middleware(checkAuthenticated)
    protected async getAllRules(req: Request, res: Response) {
        return res.json(await this.db.api.rule.getAllRules());
    }
}

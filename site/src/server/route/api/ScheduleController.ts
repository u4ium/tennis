import {
    ChildControllers,
    ClassOptions,
    Controller,
    Get,
    Middleware,
    Post
} from "@overnightjs/core";
import { Request, Response } from "express";
import { extractSchedule, extractSection, ISchedule, isSchedule } from "../../../common/model/schedule/Schedule";
import AbstractController from "../AbstractController";
import { checkOwnerOrAdministrator } from "../middleware";
import { checkBody } from "../middleware/checkBody";
import { checkParams, IdParams, isIdParams } from "../middleware/checkParams";

const children: AbstractController[] = [];

@Controller("schedule")
@ClassOptions({ mergeParams: true })
@ChildControllers(children)
export default class ScheduleController extends AbstractController {
    constructor() {
        super();
        this.children = children;
    }

    @Get(":id")
    @Middleware(checkOwnerOrAdministrator)
    @Middleware(checkParams<IdParams>(isIdParams("User")))
    protected async getSchedule(
        req: Request<IdParams>,
        res: Response<ISchedule>
    ) {
        return res.json(await this.db.api.schedule
            .getSchedule(Number(req.params.id)));
    }

    @Post(":id")
    @Middleware(checkOwnerOrAdministrator)
    @Middleware(checkParams<IdParams>(isIdParams("User")))
    @Middleware(checkBody<ISchedule>(isSchedule))
    protected async replaceSchedule(
        req: Request<IdParams, {}, ISchedule>,
        res: Response<{ id: number }>
    ) {
        const newSchedule = extractSchedule(req.body);
        const user = Number(req.params.id);
        return res.json({
            id: await this.db.api.schedule.replaceSchedule(
                {
                    user,
                    sections: newSchedule.sections.
                        reduce((p, c, i) => [
                            ...p,
                            ...c.map((s) => ({ dayOfWeek: i, ...s }))
                        ], []).
                        map(extractSection)
                }
            )
        });
    }
}

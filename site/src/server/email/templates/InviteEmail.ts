import { SendMailOptions } from "nodemailer";

export default class InviteEmail implements SendMailOptions {
    public subject = "You're invited to join the Tennis Ladder!";
    public to: string;
    public html: string;

    constructor(email: string, code: string) {
        this.to = email;
        const link: string = `http://localhost:8000/invite?email=${email}&code=${code}`;
        this.html = `
        <h2>Join the Tennis Ladder</h2>
        <p>Click the link below to confirm your email address:</p>
        <p><a href=${link}>${link}</a></p>
        `;
    }
}

import { SendMailOptions } from "nodemailer";
import { toShortNames } from "../../../app/utils";
import { IMatch } from "../../../common/model/match/MatchModel";

interface MatchInfo {
    link: string;
    info: string;
}

export function toMatchInfo(match: IMatch): MatchInfo {
    return {
        link: `/matches/upcoming/${match.id}`,
        info: `${toShortNames(match.seed)}`
            + ` VS ${toShortNames(match.challenger)} `
            + ` on ${match.scheduled}`,
    };
}

export class ScheduledMatchEmail implements SendMailOptions {
    public subject = "New Matches Scheduled!";
    public to: string;
    public html: string;

    constructor(email: string, leagueName: string, matches: MatchInfo[]) {
        this.to = email;
        this.html = `
        <h2>Upcoming Matches: ${leagueName}</h2>
        <p>
            You have been scheduled to play the following matches this session:
        </p>
        <ul
        ${matches.map((m) =>
            `<li><a href=${m.link}>${m.info}</a></li>`
        )}
        </ul>
        `;
    }
}

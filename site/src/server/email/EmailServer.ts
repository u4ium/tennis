import { Logger } from "@overnightjs/logger";
import {
    createTestAccount,
    createTransport,
    getTestMessageUrl,
    SendMailOptions,
    SentMessageInfo,
    Transporter,
} from "nodemailer";
import { isProduction } from "../util/utils";

const DEFAULTS = {
    from: '"Tennis Ladder Server 🎾" <do-not-reply@tennisladder.ca>',
};

const EMAIL_USERNAME = "admin";
const EMAIL_PASSWORD = "password";
const EMAIL_HOST = "mail";
const DOMAIN = "tennisladder.ca";
const EMAIL_PORT = 465;

export default class EmailServer {
    public static getInstance(logger?: Logger): Promise<EmailServer> {
        return new Promise(async (resolve, reject) => {
            if (null === EmailServer.instance) {
                if (logger === undefined) {
                    reject(
                        "Logger must be supplied to email server on initialization"
                    );
                }
                EmailServer.instance = new EmailServer(logger);
                await EmailServer.instance.init();
            }
            return resolve(this.instance);
        });
    }
    private static instance: EmailServer = null;
    private transporter: Transporter;
    private logger: Logger;

    private constructor(logger: Logger) {
        this.logger = logger;
    }

    public async sendEmail(email: SendMailOptions): Promise<void> {
        const info: SentMessageInfo = await this.transporter.sendMail(email);
        if (!isProduction) {
            this.logger.info(`Email Preview URL: ${getTestMessageUrl(info)}`);
        }
    }

    private async init() {
        const account = isProduction
            ? this.getProductionAccount()
            : await createTestAccount();
        this.transporter = createTransport(
            {
                host: isProduction
                    ? `${EMAIL_HOST}.${DOMAIN}`
                    : "smtp.ethereal.email",
                port: isProduction ? EMAIL_PORT : 587,
                secure: isProduction,
                auth: {
                    user: account.user,
                    pass: account.pass,
                },
            },
            DEFAULTS
        );
        EmailServer.instance = this;
    }

    private getProductionAccount() {
        return { user: EMAIL_USERNAME, pass: EMAIL_PASSWORD }; // TODO
    }
}

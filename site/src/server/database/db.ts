import { Logger } from "@overnightjs/logger";
import SequelizeStores from "connect-session-sequelize";
import expressSession from "express-session";
import fs from "fs";
import mysql from "mysql2";
import neatCsv from "neat-csv";
// import { getSequelizeSchema } from "sequelize-json-schema";
import { Sequelize } from "sequelize-typescript";
import { SequelizeAdapter, SimpleEventScheduler } from "simple-event-scheduler";
import {
    DATABASE_NAME,
    DATABASE_PASSWORD,
    DATABASE_PORT,
    DATABASE_URL,
    DATABASE_USER,
} from "../../config";
import AvatarAPI from "./api/avatar";
import ClubAPI from "./api/club";
import InviteAPI from "./api/invite";
import LeagueAPI from "./api/league";
import MatchAPI from "./api/match";
import RuleAPI from "./api/rule";
import ScheduleAPI from "./api/schedule";
import SeasonAPI from "./api/season";
import UserAPI from "./api/user";
import * as models from "./model/";

export default class DB {
    public static getInstance(logger?: Logger): Promise<DB> {
        return new Promise(async (resolve, reject) => {
            if (DB.instance === null) {
                if (logger === undefined) {
                    return reject("DB must be initialized with logger");
                }
                DB.instance = new DB(logger);
                await DB.instance.commit();
            }
            return resolve(DB.instance);
        });
    }
    private static instance: DB = null;
    public api: {
        club: ClubAPI;
        user: UserAPI;
        invite: InviteAPI;
        rule: RuleAPI;
        league: LeagueAPI;
        schedule: ScheduleAPI;
        match: MatchAPI;
        season: SeasonAPI;
        avatar: AvatarAPI;
    };

    public scheduler: SimpleEventScheduler = null;

    private logger: Logger;
    private sequelize: Sequelize;
    private sessionStore: expressSession.Store = null;

    private constructor(logger: Logger) {
        // Singleton
        this.logger = logger;
        this.sequelize = new Sequelize(
            DATABASE_NAME,
            DATABASE_USER,
            DATABASE_PASSWORD,
            {
                dialect: "mysql",
                dialectModule: mysql,
                dialectOptions: {
                    charset: "utf8",
                    multipleStatements: true,
                },
                host: DATABASE_URL,
                port: Number(DATABASE_PORT),
                logging: false,
                typeValidation: true,
                define: {
                    freezeTableName: true, // singular table names
                    underscored: true, // pothole_case attributes
                },
                models: Object.values(models),
            },
        );
        this.api = {
            club: new ClubAPI(this.logger),
            user: new UserAPI(this.logger),
            invite: new InviteAPI(this.logger),
            rule: new RuleAPI(this.logger),
            league: new LeagueAPI(this.logger),
            schedule: new ScheduleAPI(this.logger),
            match: new MatchAPI(this.logger),
            season: new SeasonAPI(this.logger),
            avatar: new AvatarAPI(this.logger),
        };
    }

    public async commit() {
        try {
            await this.sequelize.sync();
            this.logger.info("Database synced");
        } catch (e) {
            this.logger.err(e);
        }
    }

    public async destroy() {
        try {
            await this.sequelize.close();
            this.logger.info("Database connection closed");
        } catch (e) {
            this.logger.err(e);
        }
    }

    public getScheduler() {
        if (this.scheduler) { return this.scheduler; }
        const adapter = new SequelizeAdapter(this.sequelize, {
            tablename: "Job"
        });
        this.scheduler = new SimpleEventScheduler(adapter, {
            dbLoadIntervalSeconds: 1000
        });
        return this.scheduler;
    }

    public async loadTestData(): Promise<void> {
        const tableNames = [
            "Avatar",
            "User",
            "Schedule",
            "ScheduleSection",
            "Club",
            "Invite",
            "Member",
            "Rule",
            "SinglesLeague",
            "SinglesPlayer",
            "SinglesSeason",
            "SinglesSeasonParticipant",
            "SinglesMatch",
            "SinglesSet",
            "DoublesLeague",
            "DoublesTeam",
            "DoublesSeason",
            "DoublesSeasonParticipant",
            "DoublesMatch",
            "DoublesSet",
        ];
        for (const tableName of tableNames) {
            await this.loadCSVData(tableName);
        }
    }

    public async deleteAllData(): Promise<void> {
        const transaction = await this.sequelize.transaction();
        const options = { transaction, raw: true };
        await this.sequelize.query("SET FOREIGN_KEY_CHECKS = 0", options);
        await this.sequelize.truncate(options);
        await this.sequelize.query("SET FOREIGN_KEY_CHECKS = 1", options);
        await transaction.commit();
    }

    public async getSessionStore() {
        if (null === this.sessionStore) {
            const SessionStore = SequelizeStores(expressSession.Store);
            this.sessionStore = new SessionStore({ db: this.sequelize });
            await this.commit();
        }
        return this.sessionStore;
    }

    private async loadCSVData(name: string): Promise<void> {
        const model = this.sequelize.models[name];
        if (model === undefined) {
            this.logger.err(`No model named ${name} defined. Data not loaded`);
            return;
        }
        try {
            const file = await fs.readFileSync(
                `./test/server/data/${name}Data.csv`);
            const objects = await neatCsv(file, {
                mapHeaders: ({ header }) =>
                    this.convertPotholeToLowerCamel(header),
                mapValues: ({ value }) => value === "NULL" ? null : value
            });
            await model.bulkCreate(objects);
        } catch (error) {
            this.logger.err(`Could not load data for ${name}`);
            this.logger.err(error);
        }
    }

    private convertPotholeToLowerCamel(identifier: string) {
        return identifier.replace(/_(\w)/g,
            (match, capture) => capture.toUpperCase());
    }

    /*
    public outputSchema(filename: string) {
        fs.outputJSON(filename, getSequelizeSchema(this.sequelize));
    }
    */
}

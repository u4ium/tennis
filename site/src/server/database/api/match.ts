import { Logger } from "@overnightjs/logger";
import { Op } from "sequelize";
import { IMatches } from "../../../app/contexts/MatchesContext";
import {
    IDoublesMatch,
    ISet,
    ISinglesMatch
} from "../../../common/model/match/MatchModel";
import HTTPStatus from "../../../server/util/HTTPStatus";
import {
    DoublesImpromptuMatch,
    DoublesImpromptuSet,
    DoublesLeague,
    DoublesMatch,
    DoublesSeason,
    DoublesSet,
    DoublesTeam,
    Member,
    SinglesImpromptuMatch,
    SinglesImpromptuSet,
    SinglesLeague,
    SinglesMatch,
    SinglesPlayer,
    SinglesSeason,
    SinglesSet,
    User
} from "../model";
import { getDoublesParticipant } from "../model/match/getDoublesParticipant";
import { getSinglesParticipant } from "../model/match/getSinglesParticipant";

// TODO: ALL switch seed/challenger (if needed)
// And increment player/team score
// And adjust rank

function hydrateSinglesMatch(l: SinglesLeague, m: SinglesMatch) {
    return ({
        id: m.id,
        played: m.played,
        rule: l.rule,
        seed: m.Seed,
        challenger: m.Challenger,
        sets: m.sets.sort((a, b) =>
            a.position - b.position
        ).map((s) => ({
            seedScore: s.seedScore,
            challengerScore: s.challengerScore,
        }))
    });
}

function hydrateDoublesMatch(l: DoublesLeague, m: DoublesMatch) {
    return ({
        id: m.id,
        played: m.played,
        rule: l.rule,
        seed: m.Seed,
        challenger: m.Challenger,
        sets: m.sets.sort(
            (a, b) => a.position - b.position
        ).map((s) => ({
            seedScore: s.seedScore,
            challengerScore: s.challengerScore,
        }))
    });
}

export function swapScores(sets?: ISet[]): ISet[] {
    return sets?.map((s) => ({
        ...s,
        challengerScore: s.seedScore,
        seedScore: s.challengerScore
    }));
}

export default class MatchAPI {

    private logger: Logger;

    constructor(logger: Logger) {
        this.logger = logger;
    }

    public async getMyMatches(
        club: number,
        user: number
    ): Promise<IMatches> {
        // TODO: get impromptu
        const singlesLeagueMatches = await SinglesLeague.findAll({
            where: {
                club,
                [Op.or]: {
                    "$Seasons.matches.Seed.player$": user,
                    "$Seasons.matches.Challenger.player$": user,
                }
            },
            attributes: ["id", "isDoubles"],
            include: [
                {
                    model: SinglesSeason,
                    where: { start: { [Op.lte]: new Date() } },
                    include: [
                        {
                            required: false,
                            model: SinglesMatch,
                            attributes: { exclude: ["createdAt", "updatedAt"] },
                            include: [
                                {
                                    model: SinglesSet,
                                    attributes: [
                                        "seedScore",
                                        "challengerScore"
                                    ],
                                },
                                {
                                    model: SinglesPlayer,
                                    as: "Seed",
                                    attributes: ["player"],
                                    include: [
                                        {
                                            model: User,
                                            attributes: [
                                                "firstName",
                                                "lastName",
                                            ]
                                        }
                                    ]
                                },
                                {
                                    model: SinglesPlayer,
                                    as: "Challenger",
                                    attributes: ["player"],
                                    include: [
                                        {
                                            model: User,
                                            attributes: [
                                                "firstName",
                                                "lastName",
                                            ]
                                        }
                                    ]
                                },
                            ],
                        },
                    ],
                    attributes: ["id"],
                },
            ],
        });
        const doublesLeagueMatches = await DoublesLeague.findAll({
            where: {
                club,
                [Op.or]: {
                    "$Seasons.matches.Seed.player1$": user,
                    "$Seasons.matches.Seed.player2$": user,
                    "$Seasons.matches.Challenger.player1$": user,
                    "$Seasons.matches.Challenger.player2$": user,
                }
            },
            attributes: ["id", "isDoubles", "rule"],
            include: [
                {
                    model: DoublesSeason,
                    where: { start: { [Op.lte]: new Date() } },
                    include: [
                        {
                            required: false,
                            model: DoublesMatch,
                            attributes: { exclude: ["createdAt", "updatedAt"] },
                            include: [
                                {
                                    model: DoublesSet,
                                    attributes: [
                                        "seedScore",
                                        "challengerScore"
                                    ],
                                },
                                {
                                    model: DoublesTeam,
                                    as: "Seed",
                                    attributes: ["player1", "player2"],
                                    include: [
                                        {
                                            model: User,
                                            as: "Player1",
                                            attributes: [
                                                "firstName",
                                                "lastName",
                                            ]
                                        },
                                        {
                                            model: User,
                                            as: "Player2",
                                            attributes: [
                                                "firstName",
                                                "lastName",
                                            ]
                                        }
                                    ]
                                },
                                {
                                    model: DoublesTeam,
                                    as: "Challenger",
                                    attributes: ["player1", "player2"],
                                    include: [
                                        {
                                            model: User,
                                            as: "Player1",
                                            attributes: [
                                                "firstName",
                                                "lastName",
                                            ]
                                        },
                                        {
                                            model: User,
                                            as: "Player2",
                                            attributes: [
                                                "firstName",
                                                "lastName",
                                            ]
                                        }
                                    ]
                                },
                            ],
                        },
                    ],
                    attributes: ["id"],
                }
            ],
        });

        const leagueMatches = [
            ...singlesLeagueMatches.map((l) => ({
                id: l.id,
                isDoubles: l.isDoubles,
                Seasons: l.Seasons.map((s) => ({
                    id: s.id,
                    matches: s.matches.map((m) => hydrateSinglesMatch(l, m))
                }))
            })),
            ...doublesLeagueMatches.map((l) => ({
                id: l.id,
                isDoubles: l.isDoubles,
                Seasons: l.Seasons.map((s) => ({
                    id: s.id,
                    matches: s.matches.map((m) => hydrateDoublesMatch(l, m))
                }))
            }))
        ];
        const singlesMatches: ISinglesMatch[] = []; // TODO
        const doublesMatches: IDoublesMatch[] = []; // TODO
        // this.logger.warn(JSON.stringify(leagueMatches, null, 2));
        return {
            leagueMatches,
            singlesMatches,
            doublesMatches,
        };
    }

    public async addSinglesMatch(
        club: number,
        userId: number,
        match: ISinglesMatch,
        scheduled: Date[] = null // used for admins, mods, auto-scheduling
    ): Promise<number> {
        if (!scheduled) {
            this.checkUserIsSinglesMatchParticipant(userId, match);
        }

        const played = match.sets ? new Date() : null;

        if (scheduled && played) {
            throw new HTTPStatus(400, "Match cannot be scheduled and played");
        }

        if (match.season) {
            const season = await SinglesSeason.findOne({
                where: { id: match.season },
                include: [
                    {
                        model: SinglesLeague,
                        where: { club },
                        as: "League",
                        include: [{
                            model: User,
                            as: "Players",
                            required: false,
                            where: {
                                [Op.or]: [
                                    { id: match.seed.player },
                                    { id: match.challenger.player }
                                ]
                            }
                        }],
                    }
                ]
            });
            if (!season) {
                throw new HTTPStatus(400, "Season not found");
            }

            const today = new Date();
            if (season.start > today || season.end < today) {
                throw new HTTPStatus(400, "Season not current");
            }

            const seedParticipant = await getSinglesParticipant(
                season.id,
                match.seed.player);
            if (!seedParticipant) {
                throw new HTTPStatus(400, "Seed not a participant in season");
            }
            const challengerParticipant = await getSinglesParticipant(
                season.id,
                match.challenger.player);

            if (!challengerParticipant) {
                throw new HTTPStatus(400,
                    "Challenger not a participant in season");
            }
            // TODO: check if league is open to impromptu matches, if not force

            const league = season.league;
            const players = season.League.Players;

            // TODO: Replace with findOrCreate function
            let seed = players.find(
                (u) => u.id === match.seed.player
            )?.SinglesPlayer || await SinglesPlayer.create({
                league,
                player: match.seed.player,
            });

            let challenger = players.find((u) =>
                u.id === match.challenger.player
            )?.SinglesPlayer || await SinglesPlayer.create({
                league,
                player: match.challenger.player,
            });

            if (match.sets) {
                // TODO: check scores are valid, according to rule
                if (seedParticipant.rank > challengerParticipant.rank) {
                    [seed, challenger] = [challenger, seed];
                    match.sets = swapScores(match.sets);
                }
            }

            return (await SinglesMatch.create({
                sets: match.sets,
                season: season.id,
                played,
                scheduled,
                challenger: challenger.id,
                seed: seed.id,
            }, { include: [SinglesSet] })).id;
        } else {
            const members = await Member.findAll({
                where: {
                    club,
                    user: {
                        [Op.or]: [
                            match.seed.player,
                            match.challenger.player
                        ]
                    }
                }
                // TODO: order by rank
            });
            const [seed, challenger] = members.map((m) => m.id);
            if (!seed) {
                throw new HTTPStatus(403, "Not a member: seed");
            }
            if (!challenger) {
                throw new HTTPStatus(403, "Not a member: challenger");
            }
            if (members[1].user === match.seed.player
                && members[0].user === match.challenger.player) {
                match.sets = swapScores(match.sets);
            }

            return (await SinglesImpromptuMatch.create({
                sets: match.sets,
                rule: match.rule,
                played,
                seed,
                challenger
            }, { include: SinglesImpromptuSet })).id;

        }

    }

    public async recordSinglesMatch(
        id: number,
        userId: number,
        match: ISinglesMatch,
        force: boolean = false
    ): Promise<void> {
        if (!force) { this.checkUserIsSinglesMatchParticipant(userId, match); }
        const played = new Date();
        const sets = match.sets;
        // TODO: check if already played
        const [affected] = await SinglesMatch.update(
            { sets, played },
            { where: { id } }
        );
        if (affected < 1) {
            this.logger.err(`Match ${id} not found`);
            throw new HTTPStatus(404, "Match not found");
        } else if (affected > 1) {
            this.logger.err(`Multiple matches updated with ${id}`);
            throw new HTTPStatus(500, "Multiple matches updated");
        }
    }

    public async addDoublesMatch(
        club: number,
        userId: number,
        match: IDoublesMatch,
        scheduled: Date[] = null // used for admins, mods, auto-scheduling
    ): Promise<number> {
        if (!scheduled) {
            this.checkUserIsDoublesMatchParticipant(userId, match);
        }

        const played = match.sets ? new Date() : null;

        if (scheduled && played) {
            throw new HTTPStatus(400, "Match cannot be scheduled and played");
        }

        if (match.season) {
            const season = await DoublesSeason.findOne({
                where: { id: match.season },
                include: [
                    {
                        model: DoublesLeague,
                        where: { club },
                        as: "League",
                        include: [
                            {
                                model: User,
                                as: "Teams1",
                                required: false,
                                where: {
                                    [Op.or]: [
                                        { id: match.seed.player1 },
                                        { id: match.seed.player2 },
                                        { id: match.challenger.player1 },
                                        { id: match.challenger.player2 },
                                    ]
                                }
                            }, {
                                model: User,
                                as: "Teams2",
                                required: false,
                                where: {
                                    [Op.or]: [
                                        { id: match.seed.player1 },
                                        { id: match.seed.player2 },
                                        { id: match.challenger.player1 },
                                        { id: match.challenger.player2 },
                                    ]
                                }
                            }
                        ],
                    },
                ]
            });

            if (!season) {
                throw new HTTPStatus(400, "Season not found");
            }

            const today = new Date();
            if (season.start > today || season.end < today) {
                throw new HTTPStatus(400, "Season not current");
            }

            const s1P = await getDoublesParticipant(
                season.id,
                match.seed.player1);
            if (!s1P) {
                throw new HTTPStatus(400, "Seed 1 not a participant in season");
            }
            const s2P = await getDoublesParticipant(
                season.id,
                match.seed.player2);
            if (!s2P) {
                throw new HTTPStatus(400, "Seed 2 not a participant in season");
            }
            const c1P = await getDoublesParticipant(
                season.id,
                match.challenger.player1);

            if (!c1P) {
                throw new HTTPStatus(400,
                    "Challenger 1 not a participant in season");
            }
            const c2P = await getDoublesParticipant(
                season.id,
                match.challenger.player2);
            if (!c2P) {
                throw new HTTPStatus(400,
                    "Challenger 2 not a participant in season");
            }

            // TODO: check if league is open to impromptu matches, if not force

            const league = season.league;
            const teams = [...season.League.Teams1, ...season.League.Teams2];

            // TODO: Replace with findOrCreate function
            const seeds = [match.seed.player1, match.seed.player2];
            let seed = teams.find((t) =>
                seeds.includes(t.DoublesTeam.player1) &&
                seeds.includes(t.DoublesTeam.player2)
            )?.DoublesTeam || await DoublesTeam.create({
                league,
                player1: seeds[0],
                player2: seeds[1],
            });

            const challengers = [
                match.challenger.player1,
                match.challenger.player2
            ];
            let challenger = teams.find((t) =>
                challengers.includes(t.DoublesTeam.player1) &&
                challengers.includes(t.DoublesTeam.player2)
            )?.DoublesTeam || await DoublesTeam.create({
                league,
                player1: challengers[0],
                player2: challengers[1],
            });

            if (match.sets) {
                // TODO: check scores are valid, according to rule

                if (s1P.rank > c1P.rank) {
                    [seed, challenger] = [challenger, seed];

                    match.sets = swapScores(match.sets);
                }
            }

            return (await DoublesMatch.create({
                sets: match.sets,
                season: season.id,
                played,
                seed: seed.id,
                challenger: challenger.id,
            }, { include: [DoublesSet] })).id;
        } else {
            const members = await Member.findAll({
                where: {
                    club,
                    user: {
                        [Op.or]: [
                            match.seed.player1,
                            match.seed.player2,
                            match.challenger.player1,
                            match.challenger.player2,
                        ]
                    }
                }
                // TODO: order by rank
            });

            const seed = members.find((m) => m.user === match.seed.player1);
            if (!seed) {
                throw new HTTPStatus(403, "Not a member: seed1");
            }
            const seed2 = members.find((m) => m.user === match.seed.player2);
            if (!seed2) {
                throw new HTTPStatus(403, "Not a member: seed2");
            }
            // const seedRank = (seed.rank + seed2.rank) / 2;
            const challenger = members.find(
                (m) => m.user === match.challenger.player1);
            if (!challenger) {
                throw new HTTPStatus(403, "Not a member: challenger1");
            }
            const challenger2 = members.find(
                (m) => m.user === match.challenger.player2);
            if (!challenger2) {
                throw new HTTPStatus(403, "Not a member: challenger2");
            }
            // const challengerRank = (challenger.rank + challenger2.rank) / 2;

            /*
            if (challengerRank > seedRank) {
                match.sets = swapScores(match.sets);
                [seed, challenger] = [challenger, seed];
                [seed2, challenger2] = [challenger2, seed2];
            }
            */

            return (await DoublesImpromptuMatch.create({
                sets: match.sets,
                rule: match.rule,
                played,
                seed: seed.id,
                seed2: seed2.id,
                challenger: challenger.id,
                challenger2: challenger2.id,
            }, { include: [DoublesImpromptuSet] })).id;
        }
    }

    public async recordDoublesMatch(
        id: number,
        userId: number,
        match: IDoublesMatch,
        force: boolean = false,
    ): Promise<void> {
        if (!force) { this.checkUserIsDoublesMatchParticipant(userId, match); }
        const played = new Date();
        const sets = match.sets;
        // TODO: check if already played
        const [affected] = await DoublesMatch.update(
            { sets, played },
            { where: { id } }
        );
        if (affected < 1) {
            this.logger.err(`Match ${id} not found`);
            throw new HTTPStatus(404, "Match not found");
        } else if (affected > 1) {
            this.logger.err(`Multiple matches updated with ${id}`);
            throw new HTTPStatus(500, "Multiple matches updated");
        }
    }

    private async checkUserIsSinglesMatchParticipant(
        userId: number,
        match: ISinglesMatch
    ): Promise<boolean> {
        if (userId !== match.seed.player
            && userId !== match.challenger.player
        ) {
            throw new HTTPStatus(403, "User not a match participant");
        }
        return true;
    }

    private async checkUserIsDoublesMatchParticipant(
        userId: number,
        match: IDoublesMatch
    ): Promise<boolean> {
        if (userId !== match.seed.player1
            && userId !== match.seed.player2
            && userId !== match.challenger.player1
            && userId !== match.challenger.player2
        ) {
            throw new HTTPStatus(403, "User not a match participant");
        }
        return true;
    }
}

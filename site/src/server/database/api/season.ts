import { Logger } from "@overnightjs/logger";
import { Op } from "sequelize";
import { ISeason } from "../../../common/model/season/Season";
import HTTPStatus from "../../../server/util/HTTPStatus";
import {
    DoublesLeague,
    DoublesSeason,
    SinglesLeague,
    SinglesSeason
} from "../model";

export default class SeasonAPI {
    private logger: Logger;

    constructor(logger: Logger) {
        this.logger = logger;
    }

    public async getOpenSeasons(club: number) {
        const openSeason = { end: { [Op.gt]: new Date() } };
        const attributes = ["id", "matchesPerCycle", "length", "start", "end"];
        return {
            singles: await SinglesSeason.findAll({
                where: openSeason,
                include: [{
                    model: SinglesLeague,
                    where: { club },
                    attributes: [],
                }],
                attributes
            }),
            doubles: await DoublesSeason.findAll({
                where: openSeason,
                include: [{
                    model: DoublesLeague,
                    where: { club },
                    attributes: [],
                }],
                attributes
            })
        };
    }

    public async addSeason(
        league: number,
        type: "singles" | "doubles",
        season: ISeason
    ): Promise<number> {
        const startDate = new Date(season.start);
        const today = new Date();
        const endDate = new Date(season.end);
        if (startDate < today) {
            this.logger.err(`Bad Start: ${JSON.stringify(season)}`);
            throw new HTTPStatus(400, "Start must be after today");
        }
        if (endDate < today) {
            this.logger.err(`Bad End: ${JSON.stringify(season)}`);
            throw new HTTPStatus(400, "End must be after today");
        }
        if (startDate >= endDate) {
            this.logger.err(`Bad End: ${JSON.stringify(season)}`);
            throw new HTTPStatus(400, "End must be after start");
        }

        if (type === "doubles") {
            const overlap = await DoublesSeason.findOne({
                where: {
                    league,
                    end: { [Op.gt]: startDate }
                }
            });
            if (overlap) {
                this.logger.err(`Overlapping: ${JSON.stringify(season)}`);
                throw new HTTPStatus(409, "There is an overlapping season");
            }
            return (await DoublesSeason.create({ ...season, league })).id;
        } else {
            const overlap = await SinglesSeason.findOne({
                where: {
                    league,
                    end: { [Op.gt]: startDate }
                }
            });
            if (overlap) {
                this.logger.err(`Overlapping: ${JSON.stringify(season)}`);
                throw new HTTPStatus(409, "There is an overlapping season");
            }
            return (await SinglesSeason.create({ ...season, league })).id;
        }
    }
}

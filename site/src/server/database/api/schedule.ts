import { Logger } from "@overnightjs/logger";
import { col, fn } from "sequelize";
import {
    ISchedule,
    IScheduleSectionAttributes,
    IScheduleSections,
    NewSchedule
} from "../../../common/model/schedule/Schedule";
import { AvailabilityGraph } from "../../jobs/AvailabilityGraph";

import { OverlapFinder, PriorMatches } from "../../jobs/scheduling";
import HTTPStatus from "../../util/HTTPStatus";
import {
    DoublesLeague,
    DoublesMatch,
    DoublesSeason,
    DoublesTeam,
    Schedule,
    ScheduleSection,
    SinglesLeague,
    SinglesMatch,
    SinglesPlayer,
    SinglesSeason,
    User,
} from "../model";

export function convertSectionsToSeptuple(
    sections?: IScheduleSectionAttributes[]
) {
    return Array(7).fill([])
        .map((v, i) => sections?.filter((s) => s.dayOfWeek === i).
            map((s) => ({ start: s.start, end: s.end }))
        ) as IScheduleSections;

}

export default class ScheduleAPI {
    private logger: Logger;

    constructor(logger: Logger) {
        this.logger = logger;
    }

    public async getSchedule(user: number): Promise<ISchedule> {
        const schedule = await Schedule.findOne({
            where: { user },
            include: {
                model: ScheduleSection,
                attributes: {
                    exclude: [
                        "id",
                        "schedule",
                        "createdAt",
                        "updatedAt",
                    ]
                }
            }
        });
        if (schedule) {
            return {
                sections: convertSectionsToSeptuple(
                    schedule.sections as IScheduleSectionAttributes[])
            };
        } else {
            this.logger.err(`Schedule request for ${user} not found!`);
            throw new HTTPStatus(404, `No Schedule found for User: ${user}`);
        }
    }

    public async replaceSchedule(
        schedule: NewSchedule
    ): Promise<number> {
        // await Schedule.destroy({ where: { user: schedule.user } });
        return (await Schedule.create(
            schedule,
            { include: [ScheduleSection] }
        )).id;
    }

    public async scheduleMatches(
        club: number,
        isDoubles: boolean,
        season: number,
        matchesPerCycle: number,
    ) {
        const attributes: [] = [];
        const exclude = ["createdAt", "updatedAt"];
        const userSchedule = {
            model: Schedule,
            required: true,
            include: [{
                model: ScheduleSection,
                attributes: [
                    "dayOfWeek",
                    "start",
                    "end",
                ]
            }],
            attributes: {
                exclude: ["user", ...exclude]
            }
        };

        const userAttributes = [
            "id",
            "email",
            "firstName",
            "lastName",
        ];

        const users = await User.findAll({
            include: [
                userSchedule,
                {
                    model: isDoubles ? DoublesSeason : SinglesSeason,
                    through: { attributes },
                    where: { id: season },
                    attributes: []
                },
            ],
            attributes: userAttributes,
            order: ["id"]
        });

        // SELECT DISTINCT seed, challenger, COUNT(id) as num_played
        // FROM SinglesMatch
        // WHERE played IS NOT NULL
        // GROUP BY seed, challenger
        const group = [
            "seed",
            "challenger",
        ];
        const singlesMatches = await SinglesMatch.findAll<
            SinglesMatch & { numPlayed?: number }
        >({
            group,
            include: [
                {
                    model: SinglesSeason,
                    include: [{
                        model: SinglesLeague,
                        where: { club },
                        attributes,
                    }],
                    attributes,
                },
                {
                    model: SinglesPlayer,
                    as: "Seed",
                    attributes: ["player"]
                },
                {
                    model: SinglesPlayer,
                    as: "Challenger",
                    attributes: ["player"]
                }
            ],
            attributes: [
                ...group,
                [fn("COUNT", col("SinglesMatch.id")), "numPlayed"],
            ],
        });
        const doublesMatches = await DoublesMatch.findAll<
            DoublesMatch & { numPlayed?: number }
        >({
            group,
            include: [
                {
                    model: DoublesSeason,
                    include: [{
                        model: DoublesLeague,
                        where: { club },
                        attributes,
                    }],
                    attributes,
                },
                {
                    model: DoublesTeam,
                    as: "Seed",
                    attributes: ["player1", "player2"]
                },
                {
                    model: DoublesTeam,
                    as: "Challenger",
                    attributes: ["player1", "player2"]
                }
            ],
            attributes: [
                ...group,
                [fn("COUNT", col("DoublesMatch.id")), "numPlayed"],
            ],
        });

        const priorMatches: PriorMatches = {};
        users.forEach((user, index) => {
            priorMatches[user.id] = {};
            for (let index2 = index + 1; index2 < users.length; index2++) {
                const user2 = users[index2];
                priorMatches[user.id][user2.id] = 0;
            }

        });
        singlesMatches.forEach((match) => {
            const players = [
                match.Seed.player,
                match.Challenger.player
            ].sort().filter((p) => !!priorMatches[p]);
            players.forEach((p1, index) =>
                players.slice(index + 1).forEach((p2) =>
                    priorMatches[p1][p2] += match.getDataValue("numPlayed")
                )
            );
        });
        doublesMatches.forEach((match) => {
            const players = [
                match.Seed.player1,
                match.Seed.player2,
                match.Challenger.player1,
                match.Challenger.player2,
            ].sort().filter((p) => !!priorMatches[p]);
            players.forEach((p1, index) =>
                players.slice(index + 1).forEach((p2) =>
                    priorMatches[p1][p2] += match.getDataValue("numPlayed")
                )
            );
        });

        const overlapFinder = new OverlapFinder(users);
        const availabilityGroups = overlapFinder.getOverlaps(isDoubles);
        const graph = new AvailabilityGraph(availabilityGroups);
        return graph.getBestMatches(priorMatches, matchesPerCycle, isDoubles);
    }
}

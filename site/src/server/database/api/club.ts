import { Logger } from "@overnightjs/logger";
import { Club } from "../model";

export default class ClubAPI {
    private logger: Logger;

    constructor(logger: Logger) {
        this.logger = logger;
    }

    public async getClub(id: number): Promise<Club> {
        const club = await Club.findOne({ where: { id } });
        if (!club) {
            this.logger.err(`No club found with id: ${id}`);
        }
        return club.get({ plain: true });
    }
}

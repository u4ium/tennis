import { Logger } from "@overnightjs/logger";
import { Rule } from "../model";

export default class RuleAPI {
    private logger: Logger;

    constructor(logger: Logger) {
        this.logger = logger;
    }

    public async getAllRules(): Promise<Rule[]> {
        const rules = await Rule.findAll({
            attributes: { exclude: ["createdAt", "updatedAt"] }
        });
        if (rules.length === 0) {
            this.logger.err("No rules found");
        }
        return rules;
    }
}

import { Logger } from "@overnightjs/logger";
import { IAvatar } from "../../../common/model/avatar/Avatar";
import HTTPStatus from "../../util/HTTPStatus";
import { Avatar, User } from "../model";

export default class AvatarAPI {
    private logger: Logger;

    constructor(logger: Logger) {
        this.logger = logger;
    }

    public async getAvatar(userId: number): Promise<IAvatar> {
        const user = await User.findOne({
            where: { id: userId },
            attributes: [],
            include: [
                { model: Avatar }
            ]
        });
        if (!user) {
            this.logger.err("User not found");
            throw new HTTPStatus(404, "User not found");
        }
        if (!user.Avatar) {
            throw new HTTPStatus(204, "Avatar not found");
        }
        return user.Avatar;
    }
}

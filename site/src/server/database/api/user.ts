import { Logger } from "@overnightjs/logger";
import { Op } from "sequelize";
import escape from "validator/lib/escape";
import unescape from "validator/lib/unescape";
import { IUserP } from "../../../app/models/User";
import { AuthUserProps, extractAuthUserProps } from "../../../app/props";
import { IPatchUser } from "../../../common/model/user/PatchUser";
import { IPlayerUser } from "../../../common/model/user/PlayerUser";
import HTTPStatus from "../../util/HTTPStatus";
import { hashWithSalt, saltAndHash } from "../../util/passwords";
import {
    Avatar,
    Club,
    Invite,
    Member,
    Schedule,
    ScheduleSection,
    User,
    UserUpdateAttributes,
} from "../model/";
import { convertSectionsToSeptuple } from "./schedule";

export default class UserAPI {
    private logger: Logger;

    constructor(logger: Logger) {
        this.logger = logger;
    }

    public async getUser(club: number, userId: number): Promise<AuthUserProps> {
        const user: User = await User.findOne({
            where: { id: userId },
            plain: true,
            nest: true,
            attributes: {
                exclude: [
                    "passwordHash",
                    "passwordSalt",
                    "createdAt",
                    "updatedAt",
                ]
            },
            include: [
                {
                    required: false,
                    model: Avatar,
                },
                {
                    required: false,
                    model: Schedule,
                    include: [
                        {
                            model: ScheduleSection,
                            attributes: {
                                exclude: [
                                    "id",
                                    "schedule",
                                    "createdAt",
                                    "updatedAt",
                                ]
                            }
                        }
                    ]
                }
            ]
        });
        if (null === user) {
            throw new HTTPStatus(404, `User ${userId} not found`);
        }
        const u = user.get({ plain: true });
        if (u.bio) { u.bio = unescape(u.bio); }
        u.schedule = {
            sections: convertSectionsToSeptuple(u.schedule?.sections)
        };
        return extractAuthUserProps(u);
    }

    public async findUserByName(
        club: number,
        search: string
    ): Promise<IPlayerUser[]> {
        const [firstName, lastName] = search.split(" ");
        return (lastName
            ? await User.findAll({
                attributes: ["id", "firstName", "lastName"],
                where: {
                    [Op.and]: {
                        firstName: { [Op.like]: `%${firstName}%` },
                        lastName: { [Op.like]: `%${lastName}%` }
                    }
                },
                include: {
                    model: Club,
                    as: "memberships",
                    where: { id: club }
                }
            })
            : await User.findAll({
                attributes: ["id", "firstName", "lastName"],
                where: {
                    [Op.or]: {
                        firstName: { [Op.like]: `%${firstName}%` },
                        lastName: { [Op.like]: `%${firstName}%` }
                    }
                },
                include: {
                    model: Club,
                    as: "memberships",
                    where: { id: club }
                }
            }));
    }

    public async addUser(club: number, user: IUserP): Promise<number> {
        if (
            !(await Invite.count({
                where: { email: user.email, club, confirmed: true },
            }))
        ) {
            throw new HTTPStatus(401, `No confirmed invite for ${user.email}`);
        }
        const { passwordSalt, passwordHash } = saltAndHash(user.password);
        delete user.password;
        const newUser = {
            ...user,
            passwordSalt,
            passwordHash,
            isAdministrator: false
        };
        const userId = (await User.create(newUser)).id;
        await Member.create({ club, user: userId });
        return userId;
    }

    public async deleteUser(userId: number): Promise<void> {
        const count: number = await User.destroy({ where: { id: userId } });
        if (count === 1) {
            return;
        } else if (count === 0) {
            throw new HTTPStatus(404, `User ${userId} not found.`);
        } else {
            this.logger.err("Multiple users deleted!");
            throw new HTTPStatus(500, "Multiple users deleted!");
        }
    }

    public async modifyUser(id: number, user: IPatchUser): Promise<void> {
        if (user.password) {
            const { passwordSalt, passwordHash } = saltAndHash(user.password);
            delete user.password;
            user = {
                ...user,
                passwordSalt,
                passwordHash
            } as UserUpdateAttributes;
        }
        if (user.bio) {
            user.bio = escape(user.bio);
        }
        let newUser;
        if (user.Avatar) {
            user.Avatar.name = escape(user.Avatar.name);
            newUser = {
                ...user,
                avatar: (await Avatar.create(user.Avatar)).id
            };
            // TODO: delete old avatar
        } else if (user.Avatar === null) {
            newUser = { ...user, avatar: null };
            // TODO: delete old avatar
        } else {
            delete user.avatar;
            newUser = { ...user } as Omit<IPatchUser, "avatar">;
        }

        const [count] = await User.update(newUser, { where: { id } });
        if (count === 1) {
            return;
        } else if (count === 0) {
            throw new HTTPStatus(404, `User ${id} not found.`);
        } else {
            this.logger.err("Multiple users updated!");
            throw new HTTPStatus(500, "Multiple users updated!");
        }
    }

    public async checkUserLogin(
        club: number,
        email: string,
        password: string
    ): Promise<number> {
        const user = await User.findOne({
            where: { email },
            include: "memberships",
        });
        if (
            user &&
            user.passwordHash === hashWithSalt(password, user.passwordSalt)
        ) {
            const userClubs: number[] = user.memberships.map((c) => c.id);
            if (userClubs.includes(club)) {
                return user.id;
            } else {
                throw new HTTPStatus(401, "Not a member");
            }
        } else {
            throw new HTTPStatus(401, "Access denied");
        }
    }
}

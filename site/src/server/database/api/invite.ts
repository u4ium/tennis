import { Logger } from "@overnightjs/logger";
import crypto from "crypto";
import HTTPStatus from "../../util/HTTPStatus";
import { Invite, Member, User } from "../model";

export default class InviteAPI {
    private logger: Logger;

    constructor(logger: Logger) {
        this.logger = logger;
    }

    public async inviteUser(
        email: string,
        club: number,
        confirmed = false
    ): Promise<string> {
        const user = await User.findOne({ where: { email } });
        const userClubs: number[] = user.memberships.map((c) => c.id);
        if (user && userClubs.includes(club)) {
            throw new HTTPStatus(409, "User already a member of club");
        }
        if (await Invite.findOne({ where: { email, club } })) {
            throw new HTTPStatus(409, "Invite already sent");
        }
        return (await Invite.create({
            code: crypto.randomBytes(64).toString("hex"),
            email,
            club,
            confirmed,
        })).code;
    }

    public async deleteInviteForUser(
        email: string,
        club: number
    ): Promise<void> {
        const count = await Invite.destroy({ where: { email, club } });
        if (count === 1) {
            return;
        } else if (count === 0) {
            throw new HTTPStatus(404, `Invite not found for ${email}`);
        } else {
            this.logger.err("Multiple invites deleted!");
            throw new HTTPStatus(500, "Multiple invites deleted!");
        }
    }

    public async confirmInvite(
        email: string,
        club: number,
        code: string
    ): Promise<void> {
        const invite = await Invite.findOne({ where: { email, club, code } });
        if (null === invite) {
            throw new HTTPStatus(404, "Invite not found");
        } else {
            await invite.update({ confirmed: true });
        }
    }

    public async confirmInviteAuthenticated(
        userId: number,
        club: number
    ): Promise<void> {
        const user = await User.findOne({ where: { id: userId } });
        if (null === user) {
            throw new HTTPStatus(404, "User not found");
        }
        const invite = await Invite.findOne({
            where: { email: user.email, club }
        });
        if (null === invite) {
            throw new HTTPStatus(404, "Invite not found");
        }
        await invite.update({ confirmed: true });
        await new Member({ club, user: userId });
    }
}

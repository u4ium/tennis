import { Logger } from "@overnightjs/logger";
import { Op } from "sequelize";
import ILeague from "../../../common/model/league/LeagueModel";
import HTTPStatus from "../../util/HTTPStatus";
import {
    DoublesLeague,
    DoublesSeason,
    DoublesSeasonParticipant,
    Rule,
    SinglesLeague,
    SinglesSeason,
    SinglesSeasonParticipant,
    User
} from "../model";

export default class LeagueAPI {
    private logger: Logger;

    constructor(logger: Logger) {
        this.logger = logger;
    }

    public async getActiveLeagues(club: number): Promise<ILeague[]> {
        const singlesLeagues = (await SinglesLeague.findAll({
            where: { club },
            attributes: { exclude: ["createdAt", "updatedAt"] },
            include: [Rule, {
                model: SinglesSeason,
                required: false,
                // limit: 1,
                where: {
                    // start: { [Op.lt]: new Date() },
                    end: { [Op.gt]: new Date() }
                },
                include: [{
                    model: User,
                    as: "participants",
                    attributes: ["id"]
                }]
            }]
        })).map((l) => l.get({ plain: true }));
        const doublesLeagues = (await DoublesLeague.findAll({
            where: { club },
            attributes: { exclude: ["createdAt", "updatedAt"] },
            include: [Rule, {
                model: DoublesSeason,
                required: false,
                // limit: 1,
                where: {
                    // start: { [Op.lt]: new Date() },
                    end: { [Op.gt]: new Date() }
                },
                include: [{
                    model: User,
                    as: "participants",
                    attributes: ["id"]
                }]
            }]
        })).map((l) => l.get({ plain: true }));
        if (singlesLeagues.length === 0 && doublesLeagues.length === 0) {
            this.logger.err("No leagues found");
        }
        const ret = [...singlesLeagues, ...doublesLeagues];
        return ret;
    }

    public async addLeague(
        club: number,
        owner: number,
        league: ILeague
    ): Promise<number> {
        const newLeague = {
            ...league,
            club,
            owner,
        };
        if (league.Rule) {
            if (league.Rule.name !== league.rule) {
                throw new HTTPStatus(400, "Rule names must match");
            }
            await Rule.create(league.Rule);
        }
        if (league.isDoubles) {
            return (await DoublesLeague.create(newLeague)).id;
        } else {
            return (await SinglesLeague.create(newLeague)).id;
        }
    }

    public async joinSinglesLeague(
        club: number, id: number, user: number
    ): Promise<number> {
        const league = await SinglesLeague.findOne({
            where: { id, club },
            include: [{
                model: SinglesSeason,
                where: {
                    end: { [Op.gt]: new Date() },
                },
                order: "start"
            }]
        });
        if (!league) {
            throw new HTTPStatus(404, "League not found");
        }
        if (!league.Seasons) {
            throw new HTTPStatus(400, "No active season");
        }
        const season = league.Seasons[0].id;
        return (await SinglesSeasonParticipant.create({ season, user })).id;
    }

    public async joinDoublesLeague(
        club: number, id: number, user: number
    ): Promise<number> {
        const league = await DoublesLeague.findOne({
            where: { id, club },
            include: [{
                model: DoublesSeason,
                where: {
                    end: { [Op.gt]: new Date() },
                },
                order: "start"
            }]
        });
        if (!league) {
            throw new HTTPStatus(404, "League not found");
        }
        if (!league.Seasons) {
            throw new HTTPStatus(400, "No active season");
        }
        const season = league.Seasons[0].id;
        return (await DoublesSeasonParticipant.create({ season, user })).id;
    }
}

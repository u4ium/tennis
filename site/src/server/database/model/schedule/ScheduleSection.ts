import {
    AllowNull,
    AutoIncrement,
    Column,
    DataType,
    Default,
    ForeignKey,
    Model,
    PrimaryKey,
    Table,
    Validate,
} from "sequelize-typescript";
import {
    scheduleSectionValidators
} from "../../../../common/model/schedule/ScheduleSectionValidator";
import { Schedule } from "./Schedule";

@Table
export class ScheduleSection extends Model {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT({ unsigned: true }))
    public id: number;

    @AllowNull(false)
    @Validate(scheduleSectionValidators.dayOfWeek)
    @Column(DataType.TINYINT)
    public dayOfWeek: number;

    @Default(0)
    @AllowNull(false)
    @Validate(scheduleSectionValidators.start)
    @Column(DataType.TIME)
    public start: string;

    @Default(0)
    @AllowNull(false)
    @Validate(scheduleSectionValidators.end)
    @Column(DataType.TIME)
    public end: string;

    @AllowNull(false)
    @ForeignKey(() => Schedule)
    @Column(DataType.BIGINT({ unsigned: true }))
    public schedule: number;
}

import {
    AllowNull,
    AutoIncrement,
    Column,
    DataType,
    ForeignKey,
    HasMany,
    Model,
    PrimaryKey,
    Table,
} from "sequelize-typescript";
import { User } from "../user/User";
import { ScheduleSection } from "./ScheduleSection";

@Table
export class Schedule extends Model {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT({ unsigned: true }))
    public id: number;

    @AllowNull(false)
    @ForeignKey(() => User)
    @Column(DataType.BIGINT({ unsigned: true }))
    public user: number;

    @HasMany(() => ScheduleSection, { foreignKey: "schedule" })
    public sections: ScheduleSection[];

}

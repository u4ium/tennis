import {
    AllowNull,
    Column,
    DataType,
    HasMany,
    Model,
    PrimaryKey,
    Table,
    Validate,
} from "sequelize-typescript";
import {
    DoublesImpromptuMatch,
    DoublesLeague,
    SinglesImpromptuMatch,
    SinglesLeague,
} from "..";
import {
    RuleConstants,
    ruleValidators
} from "../../../../common/model/rule/RuleValidator";

@Table
export class Rule extends Model {
    @PrimaryKey
    @Validate(ruleValidators.name)
    @Column(DataType.STRING(RuleConstants.RULE_NAME_MAX_LENGTH))
    public name: string;

    @AllowNull(false)
    @Validate(ruleValidators.setsToWin)
    @Column(DataType.TINYINT({ unsigned: true }))
    public setsToWin: number;

    @AllowNull(false)
    @Validate(ruleValidators.setWinScore)
    @Column(DataType.TINYINT({ unsigned: true }))
    public setWinScore: number;

    @AllowNull(false)
    @Validate(ruleValidators.tiebreakWinScore)
    @Column(DataType.TINYINT({ unsigned: true }))
    public tiebreakWinScore: number;

    @HasMany(() => SinglesLeague, {
        foreignKey: "rule",
        onDelete: "RESTRICT"
    })
    public singlesLeagues: SinglesLeague[];

    @HasMany(() => DoublesLeague, {
        foreignKey: "rule",
        onDelete: "RESTRICT"
    })
    public doublesLeagues: DoublesLeague[];

    @HasMany(() => SinglesImpromptuMatch)
    public singlesImpromptuMatches: SinglesImpromptuMatch[];

    @HasMany(() => DoublesImpromptuMatch)
    public doublesImpromptuMatches: DoublesImpromptuMatch[];
}

import {
    AllowNull,
    BelongsTo,
    Column,
    DataType,
    Default,
    Model,
    PrimaryKey,
    Table,
    Validate,
} from "sequelize-typescript";
import { Club } from "../club/Club";

export interface InviteCreationAttributes {
    code: string;
    confirmed: boolean;
    email: string;
    club: number;
}

const INVITE_CODE_LENGTH = 128;
const EMAIL_MAX_LENGTH = 320;
@Table
export class Invite extends Model<Invite, InviteCreationAttributes> {

    @PrimaryKey
    @Validate({
        isEmail: { msg: `Invalid Invite email (not an email address)` },
        len: {
            args: [2, EMAIL_MAX_LENGTH],
            msg: `Invalid Invite email (must be 2-${EMAIL_MAX_LENGTH} characters)`,
        },
    })
    @Column(DataType.STRING(EMAIL_MAX_LENGTH))
    public email: string;

    @AllowNull(false)
    @Validate({
        is: {
            args: [/^[a-f0-9]*$/i],
            msg: `Invalid Invite code (must be hex)`,
        },
        len: {
            args: [INVITE_CODE_LENGTH, INVITE_CODE_LENGTH],
            msg: `Invalid Invite code (must have length ${INVITE_CODE_LENGTH})`,
        },
    })
    @Column(DataType.CHAR(INVITE_CODE_LENGTH))
    public code: string;

    @AllowNull(false)
    @Default(false)
    @Validate({ isBoolean: { msg: `Invalid Invite confirmed flag (must be true/false)` } })
    @Column(DataType.BOOLEAN)
    public confirmed: boolean;

    @BelongsTo(() => Club, { foreignKey: "club", as: "Club" })
    public club: Club;
}

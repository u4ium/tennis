import {
    AfterBulkCreate,
    AfterCreate,
    BelongsTo,
    BelongsToMany,
    Column,
    DataType,
    HasMany,
    Table
} from "sequelize-typescript";
import {
    DoublesLeague,
    DoublesMatch,
    DoublesSeasonParticipant,
    User,
} from "..";
import { hasValidEndDate } from "../../../../common/model/season/SeasonValidator";
import AbstractSeason, { scheduleSeason } from "./AbstractSeason";

@Table({ validate: { hasValidEndDate } })
export class DoublesSeason extends AbstractSeason {
    @AfterCreate
    public static addToScheduler(instance: DoublesSeason) {
        scheduleSeason(instance, true);
    }

    @AfterBulkCreate
    public static addToSchedulerBulk(instances: DoublesSeason[]) {
        instances.forEach((instance) => scheduleSeason(instance, true));
    }

    @HasMany(() => DoublesMatch, { foreignKey: "season", onDelete: "CASCADE" })
    public matches: DoublesMatch[];

    @BelongsTo(() => DoublesLeague, { foreignKey: "league", as: "League" })
    @Column(DataType.BIGINT({ unsigned: true }))
    public league: number;
    public League: DoublesLeague;

    @BelongsToMany(() => User, () => DoublesSeasonParticipant, "season", "user")
    public participants: Array<User & {
        DoublesSeasonParticipant: DoublesSeasonParticipant
    }>;
}

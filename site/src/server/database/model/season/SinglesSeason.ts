import {
    AfterBulkCreate,
    AfterCreate,
    BelongsTo,
    BelongsToMany,
    Column,
    DataType,
    HasMany,
    Table
} from "sequelize-typescript";
import { SinglesLeague, SinglesMatch } from "..";
import { hasValidEndDate } from "../../../../common/model/season/SeasonValidator";
import { SinglesSeasonParticipant } from "../member/SinglesSeasonParticipant";
import { User } from "../user/User";
import AbstractSeason, { scheduleSeason } from "./AbstractSeason";

@Table({ validate: { hasValidEndDate } })
export class SinglesSeason extends AbstractSeason {
    @AfterCreate
    public static addToScheduler(instance: SinglesSeason) {
        scheduleSeason(instance, false);
    }

    @AfterBulkCreate
    public static addToSchedulerBulk(instances: SinglesSeason[]) {
        instances.forEach((instance) => scheduleSeason(instance, false));
    }

    @HasMany(() => SinglesMatch, { foreignKey: "season", onDelete: "CASCADE" })
    public matches: SinglesMatch[];

    @BelongsTo(() => SinglesLeague, { foreignKey: "league", as: "League" })
    @Column(DataType.BIGINT({ unsigned: true }))
    public league: number;
    public League: SinglesLeague;

    @BelongsToMany(() => User, () => SinglesSeasonParticipant, "season", "user")
    public participants: Array<User & {
        SinglesSeasonParticipant: SinglesSeasonParticipant
    }>;
}

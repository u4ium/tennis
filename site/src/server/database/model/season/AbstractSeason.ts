import {
    AllowNull,
    AutoIncrement,
    Column,
    DataType,
    Default,
    Is,
    Model,
    PrimaryKey,
    Validate,
} from "sequelize-typescript";
import {
    seasonValidators
} from "../../../../common/model/season/SeasonValidator";
import { Scheduler } from "../../../jobs/Scheduler";
import { SeasonInfo } from "../../../jobs/SeasonInfo";
import { getIndex } from "../../../util/utils";
import AbstractLeague from "../league/AbstractLeague";
import { AbstractMatch } from "../match/AbstractMatch";
import { AbstractSeasonParticipant } from "../member/AbstractSeasonParticipant";
import { User } from "../user/User";

export function toSeasonInfo(
    season: AbstractSeason,
    isDoubles: boolean
): SeasonInfo {
    return {
        isDoubles,
        id: season.id,
        start: season.start,
        length: season.length,
        matchesPerCycle: season.matchesPerCycle,
        index: getIndex(season.start, season.length),
    };
}

export function scheduleSeason(instance: AbstractSeason, isDoubles: boolean) {
    Scheduler.getInstance().scheduleSeasonMatches(
        toSeasonInfo(instance, isDoubles));
}

export default abstract class AbstractSeason extends Model {
    public abstract matches: AbstractMatch[];
    public abstract league: number;
    public abstract League: AbstractLeague;
    public abstract participants: Array<User & ({
        SinglesSeasonParticipant: AbstractSeasonParticipant
    } | {
        DoublesSeasonParticipant: AbstractSeasonParticipant
    })>;

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT({ unsigned: true }))
    public id: number;

    @AllowNull(false)
    @Validate(seasonValidators.start)
    @Is("AfterToday", (date: Date) => {
        if (date < new Date()) {
            throw new Error(
                `Invalid Season start date: ${date} (must be after today)`);
        }
    })
    @Column(DataType.DATEONLY)
    public start: Date;

    @AllowNull(false)
    @Validate(seasonValidators.end)
    @Is("ValidEnd", (date: Date) => {
        if (date < new Date()) {
            throw new Error(
                `Invalid Season end date: ${date} (must be after today)`);
        }
        // TODO make sure this is x = length * n days from start, where x <= 365
    })
    @Column(DataType.DATEONLY)
    public end: Date;

    @AllowNull(false)
    @Default(14)
    @Validate(seasonValidators.length)
    @Column(DataType.SMALLINT({ unsigned: true }))
    public length: number;

    @AllowNull
    @Default(1)
    @Validate(seasonValidators.matchesPerCycle)
    @Column(DataType.TINYINT({ unsigned: true }))
    public matchesPerCycle: number;
}

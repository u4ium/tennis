import { BelongsTo, Column, DataType, Table } from "sequelize-typescript";
import { DoublesLeague, User } from "..";
import AbstractMessage from "./AbstractMessage";

@Table
export class DoublesLeagueMessage extends AbstractMessage {
    @BelongsTo(() => User, { foreignKey: "from", as: "From" })
    @Column(DataType.BIGINT({ unsigned: true }))
    public from: User;

    @BelongsTo(() => DoublesLeague, { foreignKey: "to", as: "To" })
    @Column(DataType.BIGINT({ unsigned: true }))
    public to: DoublesLeague;
}

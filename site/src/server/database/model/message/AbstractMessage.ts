import {
    AllowNull,
    AutoIncrement,
    Column,
    DataType,
    Is,
    Length,
    Model,
    PrimaryKey,
} from "sequelize-typescript";

const MESSAGE_REGEX = /^[\w\s\.,;:]*$/i;
const MESSAGE_CONTENT_CHARACTERS_ERROR = `Invalid Message content (must be words and ",", ".", ":" or ";")`;
const MESSAGE_MAX_LENGTH = 5000;
const MESSAGE_CONTENT_LENGTH_ERROR = `Invalid Message content (must be 1-${MESSAGE_MAX_LENGTH} characters long)`;

export default abstract class AbstractMessage extends Model {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT({ unsigned: true }))
    public id: number;

    @AllowNull(false) // TODO: why didn't I have this? Good reason? Meh.
    @Is({ msg: MESSAGE_CONTENT_CHARACTERS_ERROR, args: MESSAGE_REGEX })
    @Length({
        msg: MESSAGE_CONTENT_LENGTH_ERROR,
        min: 1,
        max: MESSAGE_MAX_LENGTH,
    })
    @Column(DataType.STRING(MESSAGE_MAX_LENGTH))
    public content: string;
}

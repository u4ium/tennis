import { BelongsTo, Column, DataType, Table } from "sequelize-typescript";
import { DoublesTeam, User } from "..";
import AbstractMessage from "./AbstractMessage";

@Table
export class DoublesMessage extends AbstractMessage {
    @BelongsTo(() => User, { foreignKey: "from", as: "From", onDelete: "SET NULL" })
    @Column(DataType.BIGINT({ unsigned: true }))
    public from: User;

    @BelongsTo(() => DoublesTeam, { foreignKey: "to", as: "To", onDelete: "SET NULL" })
    @Column(DataType.BIGINT({ unsigned: true }))
    public to: DoublesTeam;
}

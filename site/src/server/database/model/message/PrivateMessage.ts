import { BelongsTo, Column, DataType, Table } from "sequelize-typescript";
import { User } from "..";
import AbstractMessage from "./AbstractMessage";

@Table
export class PrivateMessage extends AbstractMessage {
    @BelongsTo(() => User, { foreignKey: "from", as: "From" })
    @Column(DataType.BIGINT({ unsigned: true }))
    public from: User;

    @BelongsTo(() => User, { foreignKey: "to", as: "To" })
    @Column(DataType.BIGINT({ unsigned: true }))
    public to: User;
}

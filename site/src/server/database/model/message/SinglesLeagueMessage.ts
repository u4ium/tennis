import { BelongsTo, Column, DataType, Table } from "sequelize-typescript";
import { SinglesLeague, User } from "..";
import AbstractMessage from "./AbstractMessage";

@Table
export class SinglesLeagueMessage extends AbstractMessage {
    @BelongsTo(() => User, { foreignKey: "from", as: "From" })
    @Column(DataType.BIGINT({ unsigned: true }))
    public from: User;

    @BelongsTo(() => SinglesLeague, { foreignKey: "to", as: "To" })
    @Column(DataType.BIGINT({ unsigned: true }))
    public to: SinglesLeague;
}

import { BelongsToMany, Column, DataType, Default, HasMany, Table } from "sequelize-typescript";
import { DoublesSeason, DoublesTeam, User } from "..";
import AbstractLeague from "./AbstractLeague";

@Table
export class DoublesLeague extends AbstractLeague {
    @HasMany(() => DoublesSeason, {
        foreignKey: "league",
        as: "Seasons",
        onDelete: "CASCADE",
    })
    public Seasons: DoublesSeason[];

    @Default(true)
    @Column(DataType.BOOLEAN)
    public isDoubles: boolean;

    @BelongsToMany(() => User, {
        through: {
            model: () => DoublesTeam,
            unique: false
        },
        foreignKey: "league",
        otherKey: "player1",
    })
    public Teams1: Array<User & { DoublesTeam: DoublesTeam }>;

    @BelongsToMany(() => User, {
        through: {
            model: () => DoublesTeam,
            unique: false
        },
        foreignKey: "league",
        otherKey: "player2",
    })
    public Teams2: Array<User & { DoublesTeam: DoublesTeam }>;

}

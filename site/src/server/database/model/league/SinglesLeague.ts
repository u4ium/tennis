import { BelongsToMany, Column, DataType, Default, HasMany, Table } from "sequelize-typescript";
import { SinglesPlayer, SinglesSeason, User } from "..";
import AbstractLeague from "./AbstractLeague";

@Table
export class SinglesLeague extends AbstractLeague {
    @HasMany(() => SinglesSeason, {
        foreignKey: "league",
        as: "Seasons",
        onDelete: "CASCADE",
    })
    public Seasons: SinglesSeason[];

    @BelongsToMany(() => User, () => SinglesPlayer, "league", "player")
    public Players: Array<User & { SinglesPlayer: SinglesPlayer }>;

    @Default(false)
    @Column(DataType.BOOLEAN)
    public isDoubles: boolean;
}

import {
    AllowNull,
    AutoIncrement,
    BelongsTo,
    Column,
    DataType,
    Default,
    Model,
    PrimaryKey,
    Unique,
    Validate,
} from "sequelize-typescript";
import { Avatar, Club, Rule, User } from "..";
import {
    LeagueConstants,
    leagueValidators
} from "../../../../common/model/league/LeagueValidator";

export default abstract class AbstractLeague extends Model {
    public abstract isDoubles: boolean;

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT({ unsigned: true }))
    public id: number;

    @AllowNull(false)
    @BelongsTo(() => Club, {
        foreignKey: "club",
        as: "Club",
        onDelete: "CASCADE",
    })
    @Unique({
        name: "unique_league_name_per_club",
        msg: "A club cannot contain multiple leagues with the same name"
    })
    @Column(DataType.BIGINT({ unsigned: true }))
    public club: number;
    public Club: Club;

    @AllowNull(false)
    @Validate(leagueValidators.name)
    @Unique({
        name: "unique_league_name_per_club",
        msg: "A club cannot contain multiple leagues with the same name"
    })
    @Column(DataType.STRING(LeagueConstants.LEAGUE_NAME_MAX_LENGTH))
    public name: string;

    @AllowNull(false)
    @Default("")
    @Validate(leagueValidators.about)
    @Column(DataType.STRING(LeagueConstants.LEAGUE_ABOUT_MAX_LENGTH))
    public about: string;

    @AllowNull(false)
    @Default(0)
    @Validate(leagueValidators.minSkill)
    @Column(DataType.TINYINT({ unsigned: true }))
    public minSkill: number;

    @AllowNull(false)
    @Default(10)
    @Validate(leagueValidators.maxSkill)
    @Column(DataType.TINYINT({ unsigned: true }))
    public maxSkill: number;

    @BelongsTo(() => Avatar, { foreignKey: "avatar", as: "Avatar" })
    @Column(DataType.BIGINT({ unsigned: true }))
    public avatar: number;
    public Avatar?: Avatar;

    @AllowNull(false)
    @BelongsTo(() => Rule, {
        foreignKey: "rule",
        as: "Rule",
        onDelete: "RESTRICT",
    })
    @Column(DataType.STRING)
    public rule: string;
    public Rule?: Rule;

    @AllowNull(false)
    @BelongsTo(() => User, {
        foreignKey: "owner",
        as: "Owner",
        onDelete: "RESTRICT"
    })
    @Column(DataType.BIGINT({ unsigned: true }))
    public owner: number;

    @AllowNull(false)
    @Default(false)
    @Column(DataType.TINYINT({ unsigned: true }))
    public isOpen: boolean;
}

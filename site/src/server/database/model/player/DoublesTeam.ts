import {
    AllowNull,
    BelongsTo,
    Column,
    DataType,
    ForeignKey,
    HasMany,
    Table,
    Unique,
} from "sequelize-typescript";
import { DoublesLeague, DoublesMessage, User } from "..";
import { DoublesMatch } from "../match/DoublesMatch";
import AbstractPlayer from "./AbstractPlayer";

@Table
export class DoublesTeam extends AbstractPlayer {

    @AllowNull(false)
    @ForeignKey(() => DoublesLeague)
    @Unique({
        name: "unique_doubles_team",
        msg: "Cannot join league twice"
    })
    @Column(DataType.BIGINT({ unsigned: true }))
    public league: number;

    @AllowNull(false)
    @ForeignKey(() => User)
    @Unique({
        name: "unique_doubles_team",
        msg: "Cannot join league twice"
    })
    @Column(DataType.BIGINT({ unsigned: true }))
    public player1: number;

    @BelongsTo(() => User, "player1")
    public Player1: User;

    @AllowNull(false)
    @ForeignKey(() => User)
    @Unique({
        name: "unique_doubles_team",
        msg: "Cannot join league twice"
    })
    @Column(DataType.BIGINT({ unsigned: true }))
    public player2: number;

    @BelongsTo(() => User, "player2")
    public Player2: User;

    @HasMany(() => DoublesMatch, {
        foreignKey: "seed",
        onDelete: "CASCADE"
    })
    public seedMatches: DoublesMatch[];

    @HasMany(() => DoublesMatch, {
        foreignKey: "challenger",
        onDelete: "CASCADE"
    })
    public challengerMatches: DoublesMatch[];

    @HasMany(() => DoublesMessage, { foreignKey: "to" })
    public messages: DoublesMessage[];
}

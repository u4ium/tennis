import {
    AutoIncrement,
    Column,
    DataType,
    Model,
    PrimaryKey,
} from "sequelize-typescript";

export default abstract class AbstractPlayer extends Model {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT({ unsigned: true }))
    public id: number;

    public abstract league: number;

}

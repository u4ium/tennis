import {
    AllowNull,
    BelongsTo,
    Column,
    DataType,
    ForeignKey,
    HasMany,
    Table,
    Unique
} from "sequelize-typescript";
import { SinglesLeague, User } from "..";
import { SinglesMatch } from "../match/SinglesMatch";
import AbstractPlayer from "./AbstractPlayer";

@Table
export class SinglesPlayer extends AbstractPlayer {
    @AllowNull(false)
    @ForeignKey(() => SinglesLeague)
    @Unique({
        name: "unique_singles_player",
        msg: "Cannot join league twice"
    })
    @Column(DataType.BIGINT({ unsigned: true }))
    public league: number;

    @AllowNull(false)
    @ForeignKey(() => User)
    @Unique({
        name: "unique_singles_player",
        msg: "Cannot join league twice"
    })
    @Column(DataType.BIGINT({ unsigned: true }))
    public player: number;

    @BelongsTo(() => User)
    public Player: User;

    @HasMany(() => SinglesMatch, {
        foreignKey: "seed",
        onDelete: "CASCADE"
    })
    public seedMatches: SinglesMatch[];

    @HasMany(() => SinglesMatch, {
        foreignKey: "challenger",
        onDelete: "CASCADE"
    })
    public challengerMatches: SinglesMatch[];
}

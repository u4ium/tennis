import {
    AllowNull,
    AutoIncrement,
    BelongsTo,
    BelongsToMany,
    Column,
    DataType,
    Default,
    HasMany,
    HasOne,
    Model,
    PrimaryKey,
    Table,
    Unique,
    Validate,
} from "sequelize-typescript";
import {
    Avatar,
    Club,
    DoublesLeague,
    DoublesSeason,
    DoublesSeasonParticipant,
    DoublesTeam,
    SinglesLeague,
    SinglesPlayer,
    SinglesSeason,
} from "..";
import { IUser } from "../../../../app/models/User";
import { INewAvatar } from "../../../../common/model/avatar/Avatar";
import {
    UserConstants,
    userValidators
} from "../../../../common/model/user/UserValidator";
import { Member } from "../member/Member";
import { SinglesSeasonParticipant } from "../member/SinglesSeasonParticipant";
import { Schedule } from "../schedule/Schedule";

export interface UserUpdateAttributes extends IUser {
    passwordSalt: string;
    passwordHash: string;
    Avatar?: INewAvatar;
}

export interface UserCreationAttributes extends UserUpdateAttributes {
    isAdministrator: boolean;
}

@Table
export class User extends Model {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT({ unsigned: true }))
    public id: number;

    @AllowNull(false)
    @Validate(userValidators.passwordHash)
    @Column(DataType.CHAR(UserConstants.PASSWORD_HASH_LENGTH))
    public passwordHash: string;

    @AllowNull(false)
    @Validate(userValidators.passwordSalt)
    @Column(DataType.CHAR(UserConstants.PASSWORD_SALT_LENGTH))
    public passwordSalt: string;

    @Unique
    @AllowNull(false)
    @Validate(userValidators.email)
    @Column(DataType.STRING(UserConstants.EMAIL_MAX_LENGTH))
    public email: string;

    @AllowNull(false)
    @Validate(userValidators.firstName)
    @Column(DataType.STRING(UserConstants.NAME_MAX_LENGTH))
    public firstName: string;

    @AllowNull(false)
    @Validate(userValidators.lastName)
    @Column(DataType.STRING(UserConstants.NAME_MAX_LENGTH))
    public lastName: string;

    @AllowNull(false)
    @Default("")
    @Validate(userValidators.bio)
    @Column(DataType.STRING(UserConstants.BIO_MAX_LENGTH))
    public bio: string;

    @AllowNull(false)
    @Default(false)
    @Validate(userValidators.isAdministrator)
    @Column(DataType.BOOLEAN)
    public isAdministrator: boolean;

    @AllowNull(true)
    @Validate(userValidators.skill)
    @Column(DataType.TINYINT({ unsigned: true }))
    public skill: number;

    @BelongsTo(() => Avatar, { foreignKey: "avatar", as: "Avatar" })
    @Column(DataType.BIGINT({ unsigned: true }))
    public avatar: number;
    public Avatar: Avatar;

    @BelongsToMany(() => SinglesLeague, () => SinglesPlayer, "player", "league")
    public singlesLeagues: Array<
        SinglesLeague & { SinglesPlayer: SinglesPlayer }
    >;

    @BelongsToMany(() => SinglesSeason, () => SinglesSeasonParticipant)
    public singlesSeasons: Array<SinglesSeason
        & { SinglesSeasonParticipant: SinglesSeasonParticipant }>;

    @BelongsToMany(() => DoublesLeague, {
        through: {
            model: () => DoublesTeam,
            unique: false
        },
        foreignKey: "player1",
        otherKey: "league",
    })
    public doublesLeagues1: Array<DoublesLeague & { DoublesTeam: DoublesTeam }>;

    @BelongsToMany(() => DoublesLeague, {
        through: {
            model: () => DoublesTeam,
            unique: false
        },
        foreignKey: "player1",
        otherKey: "league",
    })
    public doublesLeagues2: Array<DoublesLeague & { DoublesTeam: DoublesTeam }>;

    @BelongsToMany(() => DoublesSeason, () => DoublesSeasonParticipant)
    public doublesSeasons: Array<DoublesSeason
        & { DoublesSeasonParticipant: DoublesSeasonParticipant }>;

    @BelongsToMany(() => Club, () => Member, "user", "club")
    public memberships: Array<Club & { Member: Member }>;

    @HasMany(() => Club, { foreignKey: "owner", as: "Owner" })
    public ownedClubs: Club[];

    @HasOne(() => Schedule)
    public schedule: Schedule;

    // TODO: Add messages
}

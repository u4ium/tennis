import {
    AllowNull,
    Column,
    DataType,
    ForeignKey,
    Table,
    Unique,
} from "sequelize-typescript";
import AbstractSet from "./AbstractSet";
import { DoublesImpromptuMatch } from "./DoublesImpromptuMatch";

@Table
export class DoublesImpromptuSet extends AbstractSet {
    @AllowNull(false)
    @ForeignKey(() => DoublesImpromptuMatch)
    @Unique({
        name: "unique_doubles_set",
        msg: "Cannot replay a set in an existing match"
    })
    @Column(DataType.BIGINT({ unsigned: true }))
    public match: number;

    @AllowNull(false)
    @Unique({
        name: "unique_doubles_set",
        msg: "Cannot replay a set in an existing match"
    })
    @Column(DataType.TINYINT({ unsigned: true }))
    public position: number;
}

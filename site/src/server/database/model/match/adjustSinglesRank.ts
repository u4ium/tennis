import { Op } from "sequelize";
import { SinglesSeasonParticipant } from "../member/SinglesSeasonParticipant";

export async function adjustSinglesRank(
    season: number,
    seed: SinglesSeasonParticipant,
    challenger: SinglesSeasonParticipant
) {
    const seedRank = seed.rank;
    const challengerRank = challenger.rank;

    seed.rank = 0; // Set to 0 first to preserve UNIQUE(season, rank)
    await seed.save();
    challenger.rank = seedRank;
    await challenger.save();
    await SinglesSeasonParticipant.increment({
        rank: 1
    }, {
        where: {
            season, rank: {
                [Op.and]: [
                    { [Op.gt]: seedRank },
                    { [Op.lt]: challengerRank }
                ]
            }
        },
    });
    seed.rank = seedRank + 1;
    await seed.save();

}

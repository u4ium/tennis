import {
    AfterCreate,
    AllowNull,
    BeforeBulkCreate,
    BeforeCreate,
    BelongsTo,
    Column,
    DataType,
    ForeignKey,
    HasMany,
    Table
} from "sequelize-typescript";
import { SinglesPlayer, SinglesSeason } from "..";
import { AbstractScheduledMatch } from "./AbstractScheduledMatch";
import { increaseSinglesScores } from "./increaseSinglesScores";
import { SinglesSet } from "./SinglesSet";

async function increaseSinglesScoresBulk(instances: SinglesMatch[]) {
    for (const instance of instances) {
        await increaseSinglesScores(instance);
    }
}

@Table // TODO: ({ validate: { hasEqualScoreLengths } })
export class SinglesMatch extends AbstractScheduledMatch {

    @AfterCreate
    public static increaseScores(instance: SinglesMatch) {
        return increaseSinglesScores(instance);
    }

    @BeforeBulkCreate
    public static increaseScoresBulk(instances: SinglesMatch[]) {
        return increaseSinglesScoresBulk(instances);
    }

    @BeforeCreate
    public static setSetPoititions(instance: SinglesMatch) {
        if (instance.played) {
            let i = 1;
            for (const set of instance.sets) {
                set.position = i++;
            }
        }
    }

    @AllowNull(false)
    @BelongsTo(() => SinglesPlayer,
        { foreignKey: "seed", as: "Seed", })
    @Column(DataType.BIGINT({ unsigned: true }))
    public seed: number;
    public Seed: SinglesPlayer;

    @AllowNull(false)
    @BelongsTo(() => SinglesPlayer,
        { foreignKey: "challenger", as: "Challenger", })
    @Column(DataType.BIGINT({ unsigned: true }))
    public challenger: number;
    public Challenger: SinglesPlayer;

    @AllowNull(false)
    @ForeignKey(() => SinglesSeason)
    @Column(DataType.BIGINT({ unsigned: true }))
    public season: number;

    @BelongsTo(() => SinglesSeason)
    public Season: SinglesSeason;

    @HasMany(() => SinglesSet)
    public sets: SinglesSet[];
}

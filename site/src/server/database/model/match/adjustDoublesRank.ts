import { Op } from "sequelize";
import { DoublesSeasonParticipant } from "../member/DoublesSeasonParticipant";

export async function adjustDoublesRank(
    season: number,
    seed1: DoublesSeasonParticipant,
    seed2: DoublesSeasonParticipant,
    challenger1: DoublesSeasonParticipant,
    challenger2: DoublesSeasonParticipant,
) {
    const seed1Rank = seed1.rank;
    const challenger1Rank = challenger1.rank;

    seed1.rank = 0; // Set to 0 first to preserve UNIQUE(season, rank)
    await seed1.save();
    challenger1.rank = seed1Rank;
    await challenger1.save();
    await DoublesSeasonParticipant.increment({
        rank: 1
    }, {
        where: {
            season, rank: {
                [Op.and]: [
                    { [Op.gt]: seed1Rank },
                    { [Op.lt]: challenger1Rank }
                ]
            }
        },
    });
    seed1.rank = seed1Rank + 1;
    await seed1.save();

    seed2 = await seed2.reload();
    challenger2 = await challenger2.reload();
    const seed2Rank = seed2.rank;
    const challenger2Rank = challenger2.rank;
    if (challenger2Rank > seed2Rank) {
        seed2.rank = 0; // Set to 0 first to preserve UNIQUE(season, rank)
        await seed2.save();
        challenger2.rank = seed2Rank;
        await challenger2.save();
        await DoublesSeasonParticipant.increment({
            rank: 1
        }, {
            where: {
                season, rank: {
                    [Op.and]: [
                        { [Op.gt]: seed2Rank },
                        { [Op.lt]: challenger2Rank }
                    ]
                }
            },
        });
        seed2.rank = seed2Rank + 1;
        await seed2.save();
    }
}

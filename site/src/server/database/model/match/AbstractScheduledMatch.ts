import {
    AllowNull,
    Column,
    DataType,
    Default,
    Validate
} from "sequelize-typescript";
import AbstractPlayer from "../player/AbstractPlayer";
import { AbstractMatch } from "./AbstractMatch";

export abstract class AbstractScheduledMatch extends AbstractMatch {
    public abstract season: number;
    public abstract Seed: AbstractPlayer;
    public abstract Challenger: AbstractPlayer;

    @AllowNull(true)
    @Default(null)
    @Validate({
        isDate: {
            args: true,
            msg: `Invalid Match scheduled date (must be a date)`,
        },
    })
    @Column(DataType.DATE)
    public scheduled: Date;
}

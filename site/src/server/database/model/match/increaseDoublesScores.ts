import { DoublesLeague, DoublesSeason, DoublesTeam } from "..";
import { didSeedWin } from "../../../../app/utils";
import { increaseScore } from "./AbstractMatch";
import { adjustDoublesRank } from "./adjustDoublesRank";
import { DoublesMatch } from "./DoublesMatch";
import { DoublesSet } from "./DoublesSet";
import { getDoublesParticipant } from "./getDoublesParticipant";

export async function increaseDoublesScores(instance: DoublesMatch) {
    if (instance.played) {
        const season = instance.season;
        const seedData = await DoublesTeam.findOne({
            where: { id: instance.seed },
            attributes: ["player1", "player2"]
        });
        const challengerData = await DoublesTeam.findOne({
            where: { id: instance.challenger },
            attributes: ["player1", "player2"]
        });
        const seasonData = await DoublesSeason.findOne({
            where: { id: season },
            include: [{
                model: DoublesLeague,
                attributes: ["club"],
            }],
            attributes: []
        });
        const club = seasonData.League.club;
        const multiplier = instance.scheduled ? 2 : 1;
        const [
            seed1,
            seed2,
            challenger1,
            challenger2,
        ] = [
                seedData.player1,
                seedData.player2,
                challengerData.player1,
                challengerData.player2,
            ];
        if (!instance.sets) {
            const seed1Participant = await getDoublesParticipant(season, seed1);
            const challenger1Participant = await getDoublesParticipant(
                season, challenger1);
            const challenger2Participant = await getDoublesParticipant(
                season, challenger2);
            let swap = false;
            if (seed1Participant.rank > challenger1Participant.rank ||
                seed1Participant.rank > challenger2Participant.rank) {
                swap = true;
                [
                    instance.seed,
                    instance.challenger
                ] = [
                        instance.challenger,
                        instance.seed
                    ];
            }
            DoublesSet.addOnBulkInsertData(instance.id,
                {
                    swap,
                    club,
                    season,
                    seed1,
                    seed2,
                    challenger1,
                    challenger2,
                    multiplier
                });
        } else {
            const seedWin = didSeedWin(instance.sets);
            if (!seedWin) {
                await adjustDoublesRank(
                    season,
                    await getDoublesParticipant(season, seed1),
                    await getDoublesParticipant(season, seed2),
                    await getDoublesParticipant(season, challenger1),
                    await getDoublesParticipant(season, challenger2)
                );
            }

            await increaseScore(club, seed1, seedWin, multiplier * 1);
            await increaseScore(club, seed2, seedWin, multiplier * 1);
            await increaseScore(club, challenger1, !seedWin, multiplier * 2);
            await increaseScore(club, challenger2, !seedWin, multiplier * 2);
        }
    }
}

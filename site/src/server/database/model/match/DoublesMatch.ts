import {
    AfterCreate,
    AllowNull,
    BeforeBulkCreate,
    BeforeCreate,
    BelongsTo,
    Column,
    DataType,
    ForeignKey,
    HasMany,
    Table
} from "sequelize-typescript";
import { DoublesSeason, DoublesTeam } from "..";
import { AbstractScheduledMatch } from "./AbstractScheduledMatch";
import { DoublesSet } from "./DoublesSet";
import { increaseDoublesScores } from "./increaseDoublesScores";

async function increaseDoublesScoresBulk(instances: DoublesMatch[]) {
    for (const instance of instances) {
        await increaseDoublesScores(instance);
    }
}

@Table // ({ validate: { hasEqualScoreLengths } })
export class DoublesMatch extends AbstractScheduledMatch {

    @AfterCreate
    public static increaseScores(instance: DoublesMatch) {
        return increaseDoublesScores(instance);
    }

    @BeforeBulkCreate
    public static increaseScoresBulk(instances: DoublesMatch[]) {
        return increaseDoublesScoresBulk(instances);
    }

    @BeforeCreate
    public static setSetPoititions(instance: DoublesMatch) {
        if (instance.played) {
            let i = 1;
            for (const set of instance.sets) {
                set.position = i++;
            }
        }
    }

    @AllowNull(false)
    @BelongsTo(() => DoublesTeam, { foreignKey: "seed", as: "Seed" })
    @Column(DataType.BIGINT({ unsigned: true }))
    public seed: number;
    public Seed: DoublesTeam;

    @AllowNull(false)
    @BelongsTo(
        () => DoublesTeam,
        {
            foreignKey: "challenger",
            as: "Challenger"
        })
    @Column(DataType.BIGINT({ unsigned: true }))
    public challenger: number;
    public Challenger: DoublesTeam;

    @AllowNull(false)
    @ForeignKey(() => DoublesSeason)
    @Column(DataType.BIGINT({ unsigned: true }))
    public season: number;

    @BelongsTo(() => DoublesSeason)
    public Season: DoublesSeason;

    @HasMany(() => DoublesSet)
    public sets: DoublesSet[];
}

import {
    AllowNull,
    BeforeBulkCreate,
    Column,
    DataType,
    ForeignKey,
    Table,
    Unique,
} from "sequelize-typescript";
import { increaseScore } from "./AbstractMatch";
import AbstractSet from "./AbstractSet";
import { adjustDoublesRank } from "./adjustDoublesRank";
import { DoublesMatch } from "./DoublesMatch";
import { getDoublesParticipant } from "./getDoublesParticipant";

interface ToInsertRecord {
    swap: boolean;
    club: number;
    season: number;
    seed1: number;
    seed2: number;
    challenger1: number;
    challenger2: number;
    multiplier: number;
}
type ToInsertRecords = Record<number, ToInsertRecord>;

async function increaseDoublesScoresBulkSets(
    instances: DoublesSet[],
    toInsert: ToInsertRecords
) {
    const seedWins: Record<number, [boolean, number]> = {};
    for (const instance of instances) {
        const match = instance.match;
        if (toInsert[match]?.swap) {
            [
                instance.seedScore,
                instance.challengerScore
            ] = [
                    instance.challengerScore,
                    instance.seedScore
                ];
        }
        const record = seedWins[match];
        const position = instance.position;
        if (!record || position > record[1]) {
            const didWin = instance.seedScore > instance.challengerScore;
            seedWins[match] = [didWin, position];
        }
    }
    for (const matchId of Object.keys(seedWins)) {
        const match = Number(matchId);
        const seedWin = seedWins[match][0];
        const data = toInsert[match];
        if (!seedWin) {
            await adjustDoublesRank(
                data.season,
                await getDoublesParticipant(data.season, data.seed1),
                await getDoublesParticipant(data.season, data.seed2),
                await getDoublesParticipant(data.season, data.challenger1),
                await getDoublesParticipant(data.season, data.challenger2)
            );
        }

        await increaseScore(
            data.club, data.seed1, seedWin, data.multiplier * 1);
        await increaseScore(
            data.club, data.seed2, seedWin, data.multiplier * 1);
        await increaseScore(
            data.club, data.challenger1, !seedWin, data.multiplier * 2);
        await increaseScore(
            data.club, data.challenger2, !seedWin, data.multiplier * 2);
    }
}

@Table
export class DoublesSet extends AbstractSet {

    @BeforeBulkCreate
    public static increaseScoresBulk(instances: DoublesSet[]) {
        const result = increaseDoublesScoresBulkSets(
            instances,
            DoublesSet.onInsertData
        );
        result.then(() => {
            DoublesSet.onInsertData = {};
        });
        return result;
    }
    public static addOnBulkInsertData(matchId: number, data: ToInsertRecord) {
        DoublesSet.onInsertData[matchId] = data;
    }

    private static onInsertData: ToInsertRecords = {};

    @AllowNull(false)
    @ForeignKey(() => DoublesMatch)
    @Unique({
        name: "unique_doubles_set",
        msg: "Cannot replay a set in an existing match"
    })
    @Column(DataType.BIGINT({ unsigned: true }))
    public match: number;

    @AllowNull(false)
    @Unique({
        name: "unique_doubles_set",
        msg: "Cannot replay a set in an existing match"
    })
    @Column(DataType.TINYINT({ unsigned: true }))
    public position: number;
}

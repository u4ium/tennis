import { SinglesSeasonParticipant } from "../member/SinglesSeasonParticipant";

export async function getSinglesParticipant(season: number, user: number) {
    return await SinglesSeasonParticipant.findOne({ where: { season, user } });
}

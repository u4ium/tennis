import { SinglesPlayer, SinglesSeason } from "..";
import { didSeedWin } from "../../../../app/utils";
import { SinglesLeague } from "../league/SinglesLeague";
import { increaseScore } from "./AbstractMatch";
import { adjustSinglesRank } from "./adjustSinglesRank";
import { getSinglesParticipant } from "./getSinglesParticipant";
import { SinglesMatch } from "./SinglesMatch";
import { SinglesSet } from "./SinglesSet";

export async function increaseSinglesScores(instance: SinglesMatch) {
    if (instance.played) {
        const season = instance.season;
        const seedData = await SinglesPlayer.findOne({
            where: { id: instance.seed },
            attributes: ["player"]
        });
        const challengerData = await SinglesPlayer.findOne({
            where: { id: instance.challenger },
            attributes: ["player"]
        });
        const seasonData = await SinglesSeason.findOne({
            where: { id: season },
            include: [{
                model: SinglesLeague,
                attributes: ["club"],
            }],
            attributes: []
        });
        const club = seasonData.League.club;
        const multiplier = instance.scheduled ? 2 : 1;
        const [seed, challenger] = [seedData.player, challengerData.player];
        if (!instance.sets) {
            const seedParticipant = await getSinglesParticipant(season, seed);
            const challengerParticipant = await getSinglesParticipant(
                season, challenger);
            let swap = false;
            if (seedParticipant.rank > challengerParticipant.rank) {
                swap = true;
                [
                    instance.seed,
                    instance.challenger
                ] = [
                        instance.challenger,
                        instance.seed
                    ];
            }
            SinglesSet.addOnBulkInsertData(instance.id,
                { swap, club, season, seed, challenger, multiplier });
        } else {
            const seedWin = didSeedWin(instance.sets);
            if (!seedWin) {
                await adjustSinglesRank(
                    season,
                    await getSinglesParticipant(season, seed),
                    await getSinglesParticipant(season, challenger)
                );
            }

            await increaseScore(club, seed, seedWin, multiplier * 1);
            await increaseScore(club, challenger, !seedWin, multiplier * 2);
        }
    }
}

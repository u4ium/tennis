import {
    AllowNull,
    Column,
    DataType,
    ForeignKey,
    HasMany,
    Table
} from "sequelize-typescript";
import { Member } from "../member/Member";
import { AbstractImpromptuMatch } from "./AbstractImpromptuMatch";
import { DoublesImpromptuSet } from "./DoublesImpromptuSet";

@Table
export class DoublesImpromptuMatch extends AbstractImpromptuMatch {
    @AllowNull(false)
    @ForeignKey(() => Member)
    @Column(DataType.BIGINT({ unsigned: true }))
    public seed: number;

    @AllowNull(false)
    @ForeignKey(() => Member)
    @Column(DataType.BIGINT({ unsigned: true }))
    public seed2: number;

    @AllowNull(false)
    @ForeignKey(() => Member)
    @Column(DataType.BIGINT({ unsigned: true }))
    public challenger: number;

    @AllowNull(false)
    @ForeignKey(() => Member)
    @Column(DataType.BIGINT({ unsigned: true }))
    public challenger2: number;

    @HasMany(() => DoublesImpromptuSet)
    public sets: DoublesImpromptuSet[];
}

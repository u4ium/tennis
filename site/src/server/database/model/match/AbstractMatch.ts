import {
    AllowNull,
    AutoIncrement,
    Column,
    DataType,
    Default,
    Model,
    PrimaryKey,
    Validate,
} from "sequelize-typescript";
import { Member } from "..";
import AbstractSet from "./AbstractSet";

/*
const SCORE_REGEX = /^\[\s*((\d+)\s*(,\s*))*((\d+)\s*)+\]$/g;
const SCORE_MAX_LENGTH = 256;

export function hasEqualScoreLengths(match: AbstractMatch) {
    if (match.seedScore.length !== match.challengerScore.length) {
        throw new Error(stripIndent`
                        Invalid Score:
                        \tseed=${match.seedScore}
                        \tchallenger=${match.challengerScore}
                        \t(number of games not equal)`);
    }
}
*/

export async function increaseScore(
    club: number,
    user: number,
    didWin: boolean,
    multiplier: number,
) {
    const score = multiplier * (didWin ? 100 : 25);
    return Member.increment({ score }, { where: { club, user } });
}

export abstract class AbstractMatch extends Model {
    public abstract seed: number;
    public abstract challenger: number;
    public abstract sets: AbstractSet[];

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT({ unsigned: true }))
    public id: number;

    @AllowNull(false)
    @Default(false)
    @Validate({ isBoolean: { msg: `Invalid Match confirmed flag (must be true/false)` } })
    @Column(DataType.BOOLEAN)
    public confirmed: boolean;

    @Validate({
        isDate: {
            args: true,
            msg: `Invalid Match played date (must be a date)`,
        },
    })
    @Default(null)
    @Column(DataType.DATE)
    public played: Date;
}

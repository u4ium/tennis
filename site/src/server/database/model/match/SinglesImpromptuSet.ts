import {
    AllowNull,
    Column,
    DataType,
    ForeignKey,
    Table,
    Unique,
} from "sequelize-typescript";
import AbstractSet from "./AbstractSet";
import { SinglesImpromptuMatch } from "./SinglesImpromptuMatch";

@Table
export class SinglesImpromptuSet extends AbstractSet {
    @AllowNull(false)
    @ForeignKey(() => SinglesImpromptuMatch)
    @Unique({
        name: "unique_singles_impromptu_set",
        msg: "Cannot replay a set in an existing match"
    })
    @Column(DataType.BIGINT({ unsigned: true }))
    public match: number;

    @AllowNull(false)
    @Unique({
        name: "unique_singles_impromptu_set",
        msg: "Cannot replay a set in an existing match"
    })
    @Column(DataType.TINYINT({ unsigned: true }))
    public position: number;
}

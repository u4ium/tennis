import {
    AllowNull,
    Column,
    DataType,
    ForeignKey,
    HasMany,
    Table
} from "sequelize-typescript";
import { Member } from "../member/Member";
import { AbstractImpromptuMatch } from "./AbstractImpromptuMatch";
import { SinglesImpromptuSet } from "./SinglesImpromptuSet";

@Table
export class SinglesImpromptuMatch extends AbstractImpromptuMatch {
    @AllowNull(false)
    @ForeignKey(() => Member)
    @Column(DataType.BIGINT({ unsigned: true }))
    public seed: number;

    @AllowNull(false)
    @ForeignKey(() => Member)
    @Column(DataType.BIGINT({ unsigned: true }))
    public challenger: number;

    @HasMany(() => SinglesImpromptuSet)
    public sets: SinglesImpromptuSet[];
}

import { DoublesSeasonParticipant } from "..";

export async function getDoublesParticipant(season: number, user: number) {
    return await DoublesSeasonParticipant.findOne({ where: { season, user } });
}

import {
    AllowNull,
    AutoIncrement,
    Column,
    DataType,
    Model,
    PrimaryKey,
} from "sequelize-typescript";

export default abstract class AbstractSet extends Model {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT({ unsigned: true }))
    public id: number;

    @AllowNull(false)
    @Column(DataType.TINYINT({ unsigned: true }))
    public seedScore: number;

    @AllowNull(false)
    @Column(DataType.TINYINT({ unsigned: true }))
    public challengerScore: number;

    public abstract match: number;
    public abstract position: number;
}

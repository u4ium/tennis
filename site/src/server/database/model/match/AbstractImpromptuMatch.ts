import {
    AllowNull,

    Column,
    DataType,

    ForeignKey
} from "sequelize-typescript";
import { RuleConstants } from "../../../../common/model/rule/RuleValidator";
import { Rule } from "../rule/Rule";
import { AbstractMatch } from "./AbstractMatch";

export abstract class AbstractImpromptuMatch extends AbstractMatch {
    @AllowNull(false)
    @ForeignKey(() => Rule)
    @Column(DataType.STRING(RuleConstants.RULE_NAME_MAX_LENGTH))
    public rule: string;

}

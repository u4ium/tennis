import {
    AllowNull,
    BeforeBulkCreate,
    Column,
    DataType,
    ForeignKey,
    Table,
    Unique,
} from "sequelize-typescript";
import { increaseScore } from "./AbstractMatch";
import AbstractSet from "./AbstractSet";
import { adjustSinglesRank } from "./adjustSinglesRank";
import { getSinglesParticipant } from "./getSinglesParticipant";
import { SinglesMatch } from "./SinglesMatch";

interface ToInsertRecord {
    swap: boolean;
    club: number;
    season: number;
    seed: number;
    challenger: number;
    multiplier: number;
}
type ToInsertRecords = Record<number, ToInsertRecord>;

async function increaseSinglesScoresBulkSets(
    instances: SinglesSet[],
    toInsert: ToInsertRecords
) {
    const seedWins: Record<number, [boolean, number]> = {};
    for (const instance of instances) {
        const match = instance.match;
        if (toInsert[match]?.swap) {
            [
                instance.seedScore,
                instance.challengerScore
            ] = [
                    instance.challengerScore,
                    instance.seedScore
                ];
        }
        const record = seedWins[match];
        const position = instance.position;
        if (!record || position > record[1]) {
            const didWin = instance.seedScore > instance.challengerScore;
            seedWins[match] = [didWin, position];
        }
    }
    for (const matchId of Object.keys(seedWins)) {
        const match = Number(matchId);
        const seedWin = seedWins[match][0];
        const data = toInsert[match];
        if (!seedWin) {
            await adjustSinglesRank(
                data.season,
                await getSinglesParticipant(data.season, data.seed),
                await getSinglesParticipant(data.season, data.challenger)
            );
        }

        await increaseScore(data.club, data.seed, seedWin, data.multiplier * 1);
        await increaseScore(data.club, data.challenger, !seedWin,
            data.multiplier * 2);
    }
}

@Table
export class SinglesSet extends AbstractSet {

    @BeforeBulkCreate
    public static increaseScoresBulk(instances: SinglesSet[]) {
        const result = increaseSinglesScoresBulkSets(
            instances,
            SinglesSet.onInsertData
        );
        result.then(() => {
            SinglesSet.onInsertData = {};
        });
        return result;
    }

    public static addOnBulkInsertData(matchId: number, data: ToInsertRecord) {
        SinglesSet.onInsertData[matchId] = data;
    }
    private static onInsertData: ToInsertRecords = {};

    @AllowNull(false)
    @ForeignKey(() => SinglesMatch)
    @Unique({
        name: "unique_singles_set",
        msg: "Cannot replay a set in an existing match"
    })
    @Column(DataType.BIGINT({ unsigned: true }))
    public match: number;

    @AllowNull(false)
    @Unique({
        name: "unique_singles_set",
        msg: "Cannot replay a set in an existing match"
    })
    @Column(DataType.TINYINT({ unsigned: true }))
    public position: number;
}

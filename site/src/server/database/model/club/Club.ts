import {
    AllowNull,
    AutoIncrement,
    BelongsTo,
    BelongsToMany,
    Column,
    DataType,
    HasMany,
    Model,
    PrimaryKey,
    Table,
    Unique,
    Validate,
} from "sequelize-typescript";
import { DoublesLeague, Member, SinglesLeague, User } from "..";
import { Invite } from "../invite/Invite";

const CLUB_NAME_MAX_LENGTH = 256;
const CLUB_SHORT_NAME_MAX_LENGTH = 8;
const URL_MAX_LENGTH = 2048;

@Table
export class Club extends Model {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT({ unsigned: true }))
    public id: number;

    @Unique
    @AllowNull(false)
    @Validate({
        len: {
            args: [1, CLUB_NAME_MAX_LENGTH],
            msg: `Invalid Club name (must be between 1 and 20 characters)`,
        },
        is: {
            args: [/^[\w\s\.,;:!]*$/i],
            msg: `Invalid Club name (must be words and ",", ".", ":", "!", or ";")`,
        },
    })
    @Column(DataType.STRING(CLUB_NAME_MAX_LENGTH))
    public name: string;

    @Unique
    @AllowNull(false)
    @Validate({
        len: {
            args: [1, CLUB_SHORT_NAME_MAX_LENGTH],
            msg: `Invalid Club short name (must be between 1 and`
                + ` ${CLUB_SHORT_NAME_MAX_LENGTH} characters)`,
        },
        is: {
            args: [/^[a-z]*$/],
            msg: `Invalid Club short name (must be lowercase letters only)`,
        },
    })
    @Column(DataType.STRING(CLUB_SHORT_NAME_MAX_LENGTH))
    public shortName: string;

    @Validate({
        isUrl: { msg: `Invalid Club website (must be valid URL)` },
        len: {
            args: [1, URL_MAX_LENGTH],
            msg: `Invalid Club website (must be 1-${URL_MAX_LENGTH} characters long)`
        }
    })
    @AllowNull(false)
    @Column(DataType.STRING(URL_MAX_LENGTH))
    public website: string;

    @AllowNull(false)
    @Column(DataType.BIGINT({ unsigned: true }))
    public owner: number;

    @BelongsTo(() => User, { foreignKey: "owner", as: "Owner" })
    public Owner: User;

    @HasMany(() => SinglesLeague, { foreignKey: "club" })
    public singlesLeagues: SinglesLeague[];

    @HasMany(() => DoublesLeague, { foreignKey: "club" })
    public doublesLeagues: DoublesLeague[];

    @BelongsToMany(() => User, () => Member, "club", "user")
    public members: User[];

    @HasMany(() => Invite, { foreignKey: "club", as: "Invites" })
    public invites: Invite[];
}

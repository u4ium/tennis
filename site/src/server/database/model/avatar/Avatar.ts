import {
    AllowNull,
    AutoIncrement,
    Column,
    DataType,
    HasMany,
    Model,
    PrimaryKey,
    Table,
    Validate,
} from "sequelize-typescript";
import { DoublesLeague, SinglesLeague, User } from "..";
import {
    AvatarConstants
} from "../../../../common/model/avatar/AvatarValidator";

@Table
export class Avatar extends Model {

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT({ unsigned: true }))
    public id: number;

    @AllowNull(false)
    @Validate({
        len: {
            args: [
                AvatarConstants.AVATAR_NAME_MIN_LENGTH,
                AvatarConstants.AVATAR_NAME_MAX_LENGTH
            ],
            msg: `Invalid Avatar name ` +
                `(must be ${AvatarConstants.AVATAR_NAME_MIN_LENGTH}-` +
                `${AvatarConstants.AVATAR_NAME_MAX_LENGTH} characters)`,
        },
    })
    @Column(DataType.STRING(AvatarConstants.AVATAR_NAME_MAX_LENGTH))
    public name: string;

    @AllowNull(false)
    @Validate({
        isDataURI: {
            msg: "Invalid Avatar image (must be DataURI)",
        }
    })
    @Column(DataType.STRING(16000)) // Max needed size for b64 jpeg 64*64
    public image: string;

    @HasMany(() => User, { foreignKey: "avatar", onDelete: "SET NULL" })
    public users: User[];

    @HasMany(() => SinglesLeague, {
        foreignKey: "avatar",
        onDelete: "SET NULL"
    })
    public singlesLeagues: SinglesLeague[];

    @HasMany(() => DoublesLeague, {
        foreignKey: "avatar",
        onDelete: "SET NULL"
    })
    public doublesLeagues: DoublesLeague[];
}

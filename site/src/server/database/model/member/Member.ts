import {
    AllowNull,
    AutoIncrement,
    Column,
    DataType,
    Default,
    ForeignKey,
    HasMany,
    Model,
    PrimaryKey,
    Table,
    Validate,
} from "sequelize-typescript";
import { Club, DoublesImpromptuMatch, User } from "..";
import { SinglesImpromptuMatch } from "../match/SinglesImpromptuMatch";

@Table
export class Member extends Model {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT({ unsigned: true }))
    public id: number;

    @AllowNull(false)
    @ForeignKey(() => Club)
    @Column(DataType.BIGINT({ unsigned: true }))
    public club: number;

    @AllowNull(false)
    @ForeignKey(() => User)
    @Column(DataType.BIGINT({ unsigned: true }))
    public user: number;

    @Default(0)
    @AllowNull(false)
    @Validate({
        isInt: {
            msg: `Invalid Member score (must be an integer)`,
        },
        min: {
            args: [0],
            msg: `Invalid Member score (must be non-negative)`
        },
    })
    @Column(DataType.BIGINT({ unsigned: true }))
    public score: number;

    @Default(0)
    @AllowNull(false)
    @Validate({
        isInt: {
            msg: `Invalid Member privelage (must be an integer)`,
        },
        min: {
            args: [0],
            msg: `Invalid Member privelage (must be non-negative)`
        },
        max: {
            args: [2],
            msg: `Invalid Member privelage (must be less than or equal to 2)`
        },
    })
    @Column(DataType.TINYINT({ unsigned: true }))
    public privilege: number;

    @HasMany(() => SinglesImpromptuMatch, "seed")
    public singlesSeedMatches: SinglesImpromptuMatch[];

    @HasMany(() => SinglesImpromptuMatch, "challenger")
    public singlesChallengerMatches: SinglesImpromptuMatch[];

    @HasMany(() => DoublesImpromptuMatch, "seed")
    public doublesSeedMatches: DoublesImpromptuMatch[];

    @HasMany(() => DoublesImpromptuMatch, "seed2")
    public doublesSeed2Matches: DoublesImpromptuMatch[];

    @HasMany(() => DoublesImpromptuMatch, "challenger")
    public doublesChallengerMatches: DoublesImpromptuMatch[];

    @HasMany(() => DoublesImpromptuMatch, "challenger2")
    public doublesChallenger2Matches: DoublesImpromptuMatch[];
}

import {
    AllowNull,
    BeforeBulkCreate,
    BeforeCreate,
    BelongsTo,
    Column,
    DataType,
    ForeignKey,
    Table,
    Unique,
} from "sequelize-typescript";
import { DoublesSeason } from "../season/DoublesSeason";
import { User } from "../user/User";
import {
    AbstractSeasonParticipant,
    setRank,
    setRanks
} from "./AbstractSeasonParticipant";

@Table
export class DoublesSeasonParticipant extends AbstractSeasonParticipant {

    @BeforeCreate
    public static setRankBeforeCreate(instance: DoublesSeasonParticipant) {
        return setRank(instance,
            DoublesSeasonParticipant.maxRanks,
            DoublesSeasonParticipant.getMaxRank);
    }

    @BeforeBulkCreate
    public static setRankBeforeBulkCreate(
        instances: DoublesSeasonParticipant[]
    ) {
        return setRanks(instances,
            DoublesSeasonParticipant.maxRanks,
            DoublesSeasonParticipant.getMaxRank);
    }

    private static maxRanks: Record<number, number> = [];

    private static getMaxRank(season: number): Promise<number> {
        return DoublesSeasonParticipant.max("rank", { where: { season } });
    }

    @AllowNull(false)
    @ForeignKey(() => DoublesSeason)
    @Unique({
        name: "unique_doubles_participant",
        msg: "already a participant"
    })
    @Column(DataType.BIGINT({ unsigned: true }))
    public season: number;

    @BelongsTo(() => DoublesSeason, "season")
    public Season: DoublesSeason;

    @AllowNull(false)
    @ForeignKey(() => User)
    @Unique({
        name: "unique_doubles_participant",
        msg: "already a participant"
    })
    @Column(DataType.BIGINT({ unsigned: true }))
    public user: number;

    @BelongsTo(() => User, "user")
    public User: User;
}

import {
    AllowNull,
    AutoIncrement,
    Column,
    DataType,
    Model,
    PrimaryKey,
} from "sequelize-typescript";

export async function setRank(
    instance: AbstractSeasonParticipant,
    maxRanks: Record<number, number>,
    getMaxRank: (seasonId: number) => Promise<number>
) {
    const season = instance.season;
    if (maxRanks[season]) {
        ++maxRanks[season];
    } else {
        maxRanks[season] = (await getMaxRank(season)) || 1;
    }
    instance.rank = maxRanks[season];
}

export async function setRanks(
    instances: AbstractSeasonParticipant[],
    maxRanks: Record<number, number>,
    getMaxRank: (seasonId: number) => Promise<number>
) {
    for (const instance of instances) {
        await setRank(instance, maxRanks, getMaxRank);
    }
}

export abstract class AbstractSeasonParticipant extends Model {

    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT({ unsigned: true }))
    public id: number;

    @AllowNull(false)
    @Column(DataType.BIGINT({ unsigned: true }))
    public rank: number;

    public abstract season: number;
    public abstract user: number;
}

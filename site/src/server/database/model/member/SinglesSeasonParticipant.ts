import {
    AllowNull,
    BeforeBulkCreate,
    BeforeCreate,
    BelongsTo,
    Column,
    DataType,
    ForeignKey,
    Table,
    Unique,
} from "sequelize-typescript";
import { SinglesSeason } from "../season/SinglesSeason";
import { User } from "../user/User";
import { AbstractSeasonParticipant, setRank, setRanks } from "./AbstractSeasonParticipant";

@Table
export class SinglesSeasonParticipant extends AbstractSeasonParticipant {

    @BeforeCreate
    public static setRankBeforeCreate(instance: SinglesSeasonParticipant) {
        return setRank(instance,
            SinglesSeasonParticipant.maxRanks,
            SinglesSeasonParticipant.getMaxRank);
    }

    @BeforeBulkCreate
    public static setRankBeforeBulkCreate(
        instances: SinglesSeasonParticipant[]
    ) {
        return setRanks(instances,
            SinglesSeasonParticipant.maxRanks,
            SinglesSeasonParticipant.getMaxRank);
    }

    private static maxRanks: Record<number, number> = [];

    private static getMaxRank(season: number): Promise<number> {
        return SinglesSeasonParticipant.max("rank", { where: { season } });
    }

    @AllowNull(false)
    @Unique({
        name: "unique_singles_participant",
        msg: "already a participant"
    })
    @ForeignKey(() => SinglesSeason)
    @Column(DataType.BIGINT({ unsigned: true }))
    public season: number;

    @BelongsTo(() => SinglesSeason, "season")
    public Season: SinglesSeason;

    @AllowNull(false)
    @Unique({
        name: "unique_singles_participant",
        msg: "already a participant"
    })
    @ForeignKey(() => User)
    @Column(DataType.BIGINT({ unsigned: true }))
    public user: number;

    @BelongsTo(() => User, "user")
    public User: User;
}

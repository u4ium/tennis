import {
    AllowNull,
    AutoIncrement,
    Column,
    DataType,
    Model,
    PrimaryKey,
    Table,
} from "sequelize-typescript";

@Table({ timestamps: false })
export class Job extends Model {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT({ unsigned: true }))
    public id: number;

    @AllowNull(false)
    @Column(DataType.STRING(255))
    public name: string;

    @AllowNull(false)
    @Column(DataType.STRING(255))
    public channel: string;

    @AllowNull(false)
    @Column(DataType.BOOLEAN)
    public active: boolean;

    @Column(DataType.STRING(255))
    public cronexp: string;

    @Column(DataType.DATE)
    public nextRunAt: Date;

    @Column(DataType.INTEGER)
    public intervalSeconds: Date;

    @AllowNull(false)
    @Column(DataType.BIGINT)
    public lastRunTime: number;

    @Column(DataType.STRING(4096))
    public params: string;

    @Column(DataType.DATE)
    public startDate: Date;

    @Column(DataType.DATE)
    public endDate: Date;
}

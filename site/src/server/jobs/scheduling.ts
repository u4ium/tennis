import { eqSet } from "../../app/utils";

enum DayOfWeek {
    Monday = 0,
    Tuesday = 1,
    Wednesday = 2,
    Thursday = 3,
    Friday = 4,
    Saturday = 5,
    Sunday = 6
}

interface IScheduleSection {
    dayOfWeek: DayOfWeek;
    start: string;
    end: string;
}

interface IRawSchedule {
    id: number;
    sections: IScheduleSection[];
}

function toTimeIndex(time: string) {
    const [hours, minutes, ] = time.split(":");
    return 4 * Number(hours) + Math.floor(Number(minutes) / 15);
}

/*
function pad0(n: number) {
    return n.toLocaleString(undefined, { minimumIntegerDigits: 2 });
}

function toTimeString(timeIndex: number) {
    if (timeIndex === 0) { return "Midnight"; }
    if (timeIndex === 48) { return "Noon"; }
    const hours = Math.floor(timeIndex / 4) % 12;
    const minutes = timeIndex % 4;
    return `${hours || 12}:${pad0(minutes)} ${timeIndex < 48
        ? "AM"
        : "PM"}`;
}
*/

export function toAvailablity(sections: IScheduleSection[]) {
    const availability = [
        new Array<boolean>(24 * 4).fill(false),
        new Array<boolean>(24 * 4).fill(false),
        new Array<boolean>(24 * 4).fill(false),
        new Array<boolean>(24 * 4).fill(false),
        new Array<boolean>(24 * 4).fill(false),
        new Array<boolean>(24 * 4).fill(false),
        new Array<boolean>(24 * 4).fill(false),
    ];
    sections.forEach((section) => {
        availability[section.dayOfWeek].fill(true,
            toTimeIndex(section.start),
            toTimeIndex(section.end));

    });
    return availability;
}

interface IUserSchedule {
    id: number;
    schedule: IRawSchedule;
}

export type AvailabilityGroups = [
    AvailabilityGroup[],
    AvailabilityGroup[],
    AvailabilityGroup[],
    AvailabilityGroup[],
    AvailabilityGroup[],
    AvailabilityGroup[],
    AvailabilityGroup[],
];

export type PriorMatches = Record<number, Record<number, number>>;

export class OverlapFinder {
    // TODO: memoize overlaps of schedule-id pairs

    private schedules: Record<number, IRawSchedule>;
    private availability: Array<Array<Set<number>>>;

    constructor(schedules: IUserSchedule[]) {
        this.schedules = {};
        this.availability = [
            new Array(24 * 4).fill(null).map(() => new Set()),
            new Array(24 * 4).fill(null).map(() => new Set()),
            new Array(24 * 4).fill(null).map(() => new Set()),
            new Array(24 * 4).fill(null).map(() => new Set()),
            new Array(24 * 4).fill(null).map(() => new Set()),
            new Array(24 * 4).fill(null).map(() => new Set()),
            new Array(24 * 4).fill(null).map(() => new Set()),
        ];
        schedules.forEach((s) => this.addSchedule(s));
    }

    public addSchedule({ id, schedule }: IUserSchedule) {
        const existingSchedule = this.schedules[id];
        if (existingSchedule) {
            if (existingSchedule.id === schedule.id) { return; }
            this.alterAvailabilities((availableUsers) =>
                availableUsers.delete(id) // remove previous schedule
            );
        } else {
            this.schedules[id] = schedule;
            const availability = toAvailablity(schedule.sections);
            this.alterAvailabilities((availableUsers, dayIndex, index) => {
                if (availability[dayIndex][index]) {
                    availableUsers.add(id);
                }
            });
        }
    }

    public getOverlaps(isDoubles: boolean): AvailabilityGroups {
        return this.availability.map((a) =>
            toContiguousSections(a, isDoubles ? 4 : 2)) as AvailabilityGroups;
    }

    private alterAvailabilities(
        alter: (
            availableUsers: Set<number>,
            dayIndex: number,
            user: number
        ) => void
    ) {
        this.availability.forEach(
            (dayAvailability, dayIndex) =>
                dayAvailability.forEach(
                    (availableUsers, index) =>
                        alter(availableUsers, dayIndex, index)
                )
        );
    }
}

export interface AvailabilityGroup {
    start: number;
    end: number;
    players: number[];
}

function toContiguousSections(
    dayAvailability: Array<Set<number>>,
    minSize: number
): AvailabilityGroup[] {
    const result: AvailabilityGroup[] = [];
    let currentAvailability: Set<number> = new Set();
    let lastStart = 0;
    for (let index = 0; index < dayAvailability.length; index++) {
        const availability = dayAvailability[index];
        if (!eqSet(currentAvailability, availability)) {
            if (currentAvailability.size >= minSize) {
                result.push({
                    players: Array.from(currentAvailability),
                    start: lastStart,
                    end: index,
                });
            }
            lastStart = index;
            currentAvailability = availability;
        }
    }

    return result;
}

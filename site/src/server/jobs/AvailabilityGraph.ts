import { BinaryHeap } from "./Heap";
import {
    AvailabilityGroup,
    AvailabilityGroups,
    PriorMatches
} from "./scheduling";

export type FoundMatch = Array<AvailabilityGroup & { dayOfWeek: number }>;
type Accumulated = Record<number, number>;

export interface FoundMatches {
    accumulated: Record<number, number>;
    complete: boolean;
    result: FoundMatch[];
}

export class AvailabilityGraph {
    private nodes: Record<number, Record<number, Array<[number, number]>>> = {};
    private availabilities: AvailabilityGroups;

    constructor(availabilities: AvailabilityGroups) {
        this.availabilities = availabilities;
        availabilities.forEach((dayAvailability, dayIndex) => {
            dayAvailability.forEach((availability, index) =>
                this.add(availability, dayIndex, index));
        });
    }

    // DEPRECATED
    public getAvailability(player: number) {
        return Object.entries(this.nodes[player])?.map(
            ([opponent, opportunities]) =>
                opportunities.map(([dayOfWeek, index]) => {
                    const availability = this.availabilities[dayOfWeek][index];
                    return {
                        player: opponent,
                        start: availability.start,
                        end: availability.end,
                    };
                })
        ) || [];
    }

    // NOTE: priorMatches - ALL rowIndexes > colIndexes (symmetric, not dupli)
    public getBestMatches( // TODO: Doubles
        priorMatches: PriorMatches,
        numMatches: number = 1,
        isDoubles: boolean = false,
    ): FoundMatches {
        const hq = new BinaryHeap<[number, number]>(
            ([a, b]) => 10 * priorMatches[a][b]
                + (this.getNumOps(a) + this.getNumOps(b)) + Math.random()
        );
        let players = Object.keys(priorMatches).map(Number);
        for (let p = 0; p < players.length; p++) {
            const player = players[p];
            for (let o = p + 1; o < players.length; o++) {
                const opponent = players[o];
                if (this.nodes[player]?.[opponent] === undefined) { continue; }
                hq.push([player, opponent]);
            }
        }
        const done = new Set<string>();
        const result: FoundMatch[] = [];
        const accumulated: Accumulated = {};
        players.forEach((player) => accumulated[player] = 0);

        const isComplete = (playerId: number) =>
            accumulated[playerId] === numMatches;
        while (hq.size() && players.reduce(
            (p, c) => p || !isComplete(c),
            false
        )) {
            const [a, b] = hq.pop();
            if (isComplete(a)) {
                players = players.filter((p) => p !== a);
                continue;
            }
            if (isComplete(b)) {
                players = players.filter((p) => p !== b);
                continue;
            }

            const ops = this.nodes[a][b];

            let c: number;
            let d: number;
            if (isDoubles) {
                const randomOp = ops[
                    Math.round((ops.length - 1) * Math.random())
                ];
                const [dayOfWeek, index] = randomOp;
                const availablility = this.availabilities[dayOfWeek][index];
                const otherPlayers = availablility.players.filter(
                    (p) => p !== a && p !== b && players.includes(p)
                );
                const randomStartIdx = Math.round(
                    (otherPlayers.length - 2) * Math.random()
                );
                [c, d] = otherPlayers.splice(randomStartIdx, 2);
                if (!c || !d) { continue; }

            }
            const matchPlayers = isDoubles ? [a, b, c, d].sort() : [a, b];
            const matchPlayersUID = matchPlayers.join(",");
            if (done.has(matchPlayersUID)) { continue; }
            done.add(matchPlayersUID);

            matchPlayers.forEach((p, i) => {
                accumulated[p] += 1;
                matchPlayers.slice(i + 1).forEach((p2) => {
                    priorMatches[p][p2] += 1; // This could break the heap order
                });
            });

            result.push(ops.filter(
                ([dayOfWeek, index]) => {
                    if (!isDoubles) { return true; }
                    const availablility = this.availabilities[dayOfWeek][index];
                    return matchPlayers.reduce(
                        (p, x) => p && availablility.players.includes(x),
                        true
                    );
                }).map(([dayOfWeek, index]) => {
                    const availablility = this.availabilities[dayOfWeek][index];
                    return {
                        dayOfWeek,
                        start: availablility.start,
                        end: availablility.end,
                        players: matchPlayers,
                    };
                }));
        }

        const complete = players.reduce((p, c) => p && isComplete(c), true);

        return { result, accumulated, complete };
    }

    private getNumOps(playerId: number) {
        return Object.entries(this.nodes[playerId]).length;
    }

    private add(
        availability: AvailabilityGroup,
        dayOfWeek: number,
        index: number
    ) {
        const value = [dayOfWeek, index] as [number, number];
        availability.players.forEach((player) => {
            const existingNode = this.nodes[player];
            if (existingNode) {
                availability.players.forEach((opponent) => {
                    if (opponent !== player) {
                        const ops = existingNode[opponent];
                        if (ops === undefined) {
                            existingNode[opponent] = [value];
                        } else {
                            ops.push(value);
                        }

                    }
                });
            } else {
                this.nodes[player] = {};
                availability.players.forEach((opponent) => {
                    this.nodes[player][opponent] = [value];
                });
            }
        });
    }
}

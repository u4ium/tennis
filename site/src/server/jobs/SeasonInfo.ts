
export interface SeasonInfo {
    id: number;
    isDoubles: boolean;
    matchesPerCycle: number;
    length: number;
    start: Date;
    index: number;
}

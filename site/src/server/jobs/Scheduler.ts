import { Logger } from "@overnightjs/logger";
import { SimpleEventScheduler } from "simple-event-scheduler";
import { IDoublesMatch, IMatch, ISinglesMatch } from "../../common/model/match/MatchModel";
import { CLUB_ID } from "../../config";
import DB from "../database/db";
import { DoublesLeague, DoublesSeason, SinglesLeague, SinglesSeason, User } from "../database/model";
import AbstractSeason from "../database/model/season/AbstractSeason";
import EmailServer from "../email/EmailServer";
import {
    ScheduledMatchEmail,
    toMatchInfo
} from "../email/templates/ScheduledMatchEmail";
import { addDays } from "../util/utils";
import { FoundMatch, FoundMatches } from "./AvailabilityGraph";
import { SeasonInfo } from "./SeasonInfo";

interface UserContact {
    firstName: string;
    lastName: string;
    email: string;
    rank: number;
}

type PlayerData = Record<number, UserContact>;

export class Scheduler {

    public static getInstance(props?: {
        logger: Logger;
        database: DB;
        emailServer: EmailServer;
    }) {
        if (!Scheduler.instance && props) {
            Scheduler.instance = new Scheduler(
                props.logger,
                props.database,
                props.emailServer,
            );
        }
        return Scheduler.instance;
    }

    // Singleton
    private static instance: Scheduler = null;
    public scheduler: SimpleEventScheduler;
    private club: number;
    private logger: Logger;
    private db: DB;
    private channel: string;
    private emailServer: EmailServer;
    private constructor(
        logger: Logger,
        database: DB,
        emailServer: EmailServer
    ) {
        this.club = Number(CLUB_ID);
        this.logger = logger;
        this.db = database;
        this.emailServer = emailServer;
        this.channel = `${this.club}-ScheduledMatches`;
        this.scheduler = this.db.getScheduler();
    }

    public start() {
        this.scheduler.on(this.channel, ({ params }: { params: string; }) => {
            const season: SeasonInfo = JSON.parse(params);
            season.index += 1;
            this.db.api.schedule.scheduleMatches(
                this.club,
                season.isDoubles,
                season.id,
                season.matchesPerCycle
            ).then((matches) => {
                this.logger.warn(JSON.stringify(matches, null, 2));
                this.emailAndScheduleMatches(
                    season.id,
                    season.isDoubles,
                    matches
                );
            });
            this.scheduleSeasonMatches(season);
        });
        this.scheduler.start();
    }

    public async scheduleSeasonMatches(season: SeasonInfo) {
        const type = season.isDoubles ? "D" : "S";
        const jobId = `${this.club}-S${type}M-${season.id}-${season.index}`;
        const runAt = addDays(season.start, season.length * season.index);
        try {
            await this.scheduler.createOnetimeJob(jobId, runAt, {
                params: JSON.stringify(season)
            });
        } catch (e) {
            this.logger.warn(`${e} - (${jobId})`);
        }
    }

    private emailAndScheduleMatches(
        seasonId: number,
        isDoubles: boolean,
        matches: FoundMatches
    ) {
        const players = Object.keys(matches.accumulated).map(Number);
        const scheduledMatches: Record<number, IMatch[]> = {};
        players.forEach((player) => {
            if (!matches.accumulated[player]) {
                this.logger.warn(`No matches scheduled for ${player}`);
            }
            scheduledMatches[player] = [];
        });
        const playerData: PlayerData = {};
        User.findAll({
            where: { id: players },
            include: [{
                where: { id: seasonId },
                model: isDoubles ? DoublesSeason : SinglesSeason,
                through: {
                    attributes: ["rank"],
                },
                attributes: []
            }],
            attributes: ["id", "firstName", "lastName", "email"],
        }).then((users) =>
            users.forEach((user) => {
                playerData[user.id] = {
                    ...user,
                    rank: isDoubles
                        ? user.doublesSeasons[0].DoublesSeasonParticipant.rank
                        : user.singlesSeasons[0].SinglesSeasonParticipant.rank
                };
            })
        );

        const getRank = (playerId: number) => playerData[playerId].rank;
        const byRank = (a: number, b: number) => getRank(a) - getRank(b);

        matches.result.forEach(async (matchUp) => {
            const scheduled = getPlayDates(matchUp);
            if (isDoubles) {
                const matchPlayers = matchUp[0].players.sort(byRank);
                const [seed1, challenger1, challenger2, seed2] = matchPlayers;
                const match: IDoublesMatch = {
                    seed: toDoublesPlayer(seed1, seed2, playerData),
                    challenger: toDoublesPlayer(
                        challenger1, challenger2, playerData),
                };
                const id = await this.db.api.match.addDoublesMatch(
                    this.club,
                    0,
                    match,
                    scheduled,
                );
                matchPlayers.forEach((p) =>
                    scheduledMatches[p].push({ id, ...match, scheduled })
                );
            } else {
                const matchPlayers = matchUp[0].players.sort(byRank);
                const [seed, challenger] = matchPlayers;
                const match: ISinglesMatch = {
                    seed: toSinglesPlayer(seed, playerData),
                    challenger: toSinglesPlayer(challenger, playerData),
                };
                const id = await this.db.api.match.addSinglesMatch(
                    this.club,
                    0,
                    match,
                    scheduled,
                );
                matchPlayers.forEach((p) =>
                    scheduledMatches[p].push({ id, ...match, scheduled })
                );
            }
        });

        const find = {
            where: { id: seasonId },
            include: [{
                model: isDoubles ? DoublesLeague : SinglesLeague,
                attributes: ["name"],
            }]
        };
        if (isDoubles) {
            SinglesSeason.findOne(find).then((s) =>
                Object.keys(scheduledMatches).forEach((p) => {
                    const playerId = Number(p);
                    const playerMatches = scheduledMatches[playerId];
                    this.emailScheduledMatch(
                        s,
                        playerMatches,
                        playerData,
                        playerId
                    );
                })
            );
        } else {
            DoublesSeason.findOne(find).then((s) =>
                Object.keys(scheduledMatches).forEach((p) => {
                    const playerId = Number(p);
                    const playerMatches = scheduledMatches[playerId];
                    this.emailScheduledMatch(
                        s,
                        playerMatches,
                        playerData,
                        playerId
                    );
                })
            );
        }

    }

    private emailScheduledMatch(
        season: AbstractSeason,
        matches: IMatch[],
        playerData: PlayerData,
        playerId: number,
    ) {
        const email = playerData[playerId].email;
        this.emailServer.sendEmail(
            new ScheduledMatchEmail(
                email,
                season.League.name,
                matches.map(toMatchInfo),
            )
        );
    }
}

function toSinglesPlayer(id: number, playerData: PlayerData) {
    return {
        player: id,
        Player: {
            firstName: playerData[id].firstName,
            lastName: playerData[id].lastName,
        }
    };
}

function toDoublesPlayer(id1: number, id2: number, playerData: PlayerData) {
    return {
        player1: id1,
        Player1: {
            firstName: playerData[id1].firstName,
            lastName: playerData[id1].lastName,
        },
        player2: id2,
        Player2: {
            firstName: playerData[id2].firstName,
            lastName: playerData[id2].lastName,
        }
    };
}

function getPlayDates(matchUp: FoundMatch): Date[] {
    return matchUp.map((m) => toDateFromFoundMatchIndex(m.dayOfWeek, m.start));
}

function toDateFromFoundMatchIndex(dayOfWeek: number, timeIndex: number): Date {
    const today = new Date();
    const todayParams: [number, number, number] = [
        today.getFullYear(),
        today.getMonth(),
        today.getDate()
    ];
    const todaysDayOfWeek = today.getDay();
    const startOfToday = new Date(...todayParams);
    let daysInFuture = dayOfWeek - todaysDayOfWeek;
    if (daysInFuture < 0) { daysInFuture += 7; }
    const matchDate = addDays(startOfToday, daysInFuture);
    matchDate.setHours(...fromIndexToHoursAndMinutes(timeIndex));
    return matchDate;
}

function fromIndexToHoursAndMinutes(timeIndex: number): [number, number] {
    return [
        Math.floor(timeIndex / 4),
        (timeIndex % 4) * 15
    ];
}

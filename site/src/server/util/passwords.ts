import crypto from "crypto";

const SALT_LENGTH = 16;

export function saltAndHash(password: string) {
    const salt = crypto.randomBytes(SALT_LENGTH).toString("base64");
    return { passwordSalt: salt, passwordHash: hashWithSalt(password, salt) };
}

export function hashWithSalt(password: string, salt: string) {
    const hash = crypto.createHmac("sha512", salt);
    hash.update(password);
    return hash.digest("base64");
}

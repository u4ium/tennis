export default class HTTPStatus extends Error {
    public code: number;

    constructor(code: number, message: string) {
        super();
        this.code = code;
        this.message = message;
    }
}

export const DB_FAIL = new HTTPStatus(500, "Database Failure");

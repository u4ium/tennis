// import { Logger } from "@overnightjs/logger";
// import DB from "../database/db";
// import ConnectedController from "../route/AbstractController";

export function addDays(date: Date | string, days: number) {
    const result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

const ONE_DAY_IN_MILLISECONDS = 24 * 60 * 60 * 1000;
export function getIndex(start: Date | string, length: number) {
    const s = new Date(start).valueOf();
    const n = new Date().valueOf();
    const r = Math.ceil((n - s) / (ONE_DAY_IN_MILLISECONDS * length));
    return r > 0 ? r : 0;
}

export const ONE_WEEK_IN_MILLISECONDS = 7 * ONE_DAY_IN_MILLISECONDS;
export const isProduction: boolean = process.env.NODE_ENV === "production";

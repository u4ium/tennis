import { Server } from "@overnightjs/core";
import { Logger } from "@overnightjs/logger";
import { json, urlencoded } from "body-parser";
import { exec } from "child_process";
import { randomBytes } from "crypto";
import express, { Request, Response, static as serveStatic } from "express";
import Router from "express-promise-router";
import expressSession from "express-session";
import { readFileSync } from "fs";
import helmet from "helmet";
import http from "http";
import https from "https";
import {
    CLUB_ID,
    SERVER_ADDRESS,
    SERVER_HTTP_PORT,
    SERVER_HTTPS_PORT,
    SERVER_PROTOCOL,
    SESSION_SECRET,
    SSL_CERTIFICATE_PATH,
} from "../config";
import DB from "./database/db";
import EmailServer from "./email/EmailServer";
import { Scheduler } from "./jobs/Scheduler";
import Root from "./route/view/Root";
import { isProduction } from "./util/utils";

declare module "express-session" {
    interface SessionData {
        userID: number;
        isAdministrator: boolean;
        inviteEmail?: string;
    }
}

declare module "express" {
    interface Response {
        locals: {
            cspNonce: string, // added by Helmet middleware
            isProduction: boolean,
            clubId: number,
        };
    }
}

class WebServer extends Server {
    private logger: Logger;

    constructor(
        databaseConnection: DB,
        sessionStore: expressSession.Store,
        emailServer: EmailServer,
        logger: Logger
    ) {
        super(!isProduction);

        this.logger = logger;

        this.app.use((req, res, next) => {
            res.locals.cspNonce = randomBytes(16).toString("hex");
            res.locals.isProduction = isProduction;
            res.locals.clubId = Number(CLUB_ID);
            next();
        });

        this.app.use(helmet({
            contentSecurityPolicy: {
                directives: {
                    defaultSrc: ["'self'"],
                    scriptSrc: [
                        "'self'",
                        (req, res: Response) => `'nonce-${res.locals.cspNonce}'`
                    ],
                    connectSrc: isProduction ? [] : [
                        "'self'",
                        "http://localhost:3000",
                        "ws://localhost:3000"
                    ],
                    // TODO: remove data:, blob:
                    imgSrc: ["'self'", "data:", "blob:"]
                },
            }
        }));

        this.app.use(json({ limit: "32kb" }));
        this.app.use(urlencoded({ extended: true }));

        this.app.use(
            expressSession({
                secret: SESSION_SECRET,
                store: sessionStore,
                resave: false,
                saveUninitialized: false,
                cookie: {
                    secure: isProduction,
                    sameSite: true,
                }
            })
        );

        this.app.use(serveStatic("dist/public"));

        this.app.set("views", "dist/public");
        this.app.set("view engine", "ejs");

        const root = new Root(databaseConnection, emailServer, logger);
        super.addControllers(root, Router);
    }

    public start() {
        if (SERVER_PROTOCOL === "https") {
            const certificatePath = `${SSL_CERTIFICATE_PATH}/${SERVER_ADDRESS}`;

            https
                .createServer(
                    {
                        cert: readFileSync(
                            `${certificatePath}/cert.pem`,
                            "utf8"
                        ),
                        key: readFileSync(
                            `${certificatePath}/privkey.pem`,
                            "utf8"
                        ),
                        ca: readFileSync(
                            `${certificatePath}/chain.pem`,
                            "utf8"
                        ),
                    },
                    this.app
                )
                .listen(SERVER_HTTPS_PORT, () =>
                    http
                        .createServer(() => express().use(this.redirectToHTTPS))
                        .listen(SERVER_HTTP_PORT, this.logConnection)
                );
        } else {
            http.createServer(this.app).listen(
                SERVER_HTTP_PORT,
                this.logConnection
            );
        }
    }

    private logConnection = () => {
        if (SERVER_PROTOCOL === "https") {
            this.logger.info(
                `Server: https://${SERVER_ADDRESS}:${SERVER_HTTPS_PORT}`
            );
        }
        this.logger.info(
            `Server: http://${SERVER_ADDRESS}:${SERVER_HTTP_PORT}`
        );
    }

    private redirectToHTTPS() {
        return (req: Request, res: Response) =>
            res.redirect(
                `https://${req.headers.host}:${SERVER_HTTPS_PORT}${req.url}`
            );
    }
}

(async () => {
    const logger = new Logger();
    try {
        const database = await DB.getInstance(logger);
        const sessionStore = await database.getSessionStore();
        const emailServer = await EmailServer.getInstance(logger);
        new WebServer(
            database,
            sessionStore,
            emailServer,
            logger,
        ).start();
        const scheduler = Scheduler.getInstance({
            logger,
            database,
            emailServer
        });
        scheduler.start();

        if (!isProduction) {
            exec("yarn notifyClient");
        }
    } catch (err) {
        logger.err("Server failed to start");
        logger.err(err.message);
    }
})();

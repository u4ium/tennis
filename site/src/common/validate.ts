import validator from "validator";
import { IUserP, IUserPP } from "../app/models/User";

export const validateUser = (
    user: IUserP | IUserPP,
    requirePasswordConfirmation: boolean = true
): user is IUserP | IUserPP => {
    if (!user || Object.keys(user).length === 0) {
        throw new Error("User may not be undefined or empty");
    } else if (!validator.isEmail(user.email.trim())) {
        throw new Error("Username must be a valid email address.");
    } else if (validator.escape(user.password) !== user.password) {
        throw new Error("Password cannot contain: <, >, &, ', \" or /");
    } else if (user.password && user.password.length < 5) {
        throw new Error("Password must have at least 5 characters");
    } else if (
        requirePasswordConfirmation &&
        (!("password2" in user) ||
            user.password !== (user as IUserPP).password2)
    ) {
        throw new Error("Passwords must match");
    } else if (user.firstName.length < 1) {
        throw new Error("Your first name must be provided");
    } else if (!validator.isAlpha(user.firstName)) {
        throw new Error("First name may only contain alphabetic characters");
    } else if (!validator.isAlpha(user.lastName)) {
        throw new Error("Last name may only contain alphabetic characters");
    }
    return true;
};

export const validatePartialUser = (
    user: Partial<IUserPP>,
    requirePasswordConfirmation: boolean = false
): user is Partial<IUserP> => {
    if (!user || Object.keys(user).length === 0) {
        throw new Error("User may not be undefined or empty");
    } else if (user.email && !validator.isEmail(user.email.trim())) {
        throw new Error("Username must be a valid email address.");
    } else if (
        user.password &&
        validator.escape(user.password) !== user.password
    ) {
        throw new Error("Password cannot contain: <, >, &, ', \" or /");
    } else if (user.password && user.password.length < 5) {
        throw new Error("Password must have at least 5 characters");
    } else if (
        requirePasswordConfirmation &&
        user.password !== user.password2
    ) {
        throw new Error("Passwords must match");
    } else if (user.firstName && user.firstName.length < 1) {
        throw new Error("Your first name must be provided");
    } else if (user.firstName && !validator.isAlpha(user.firstName)) {
        throw new Error("First name may only contain alphabetic characters");
    } else if (user.lastName && !validator.isAlpha(user.lastName)) {
        throw new Error("Last name may only contain alphabetic characters");
    } else if (
        !user.email &&
        !user.password &&
        !user.lastName &&
        !user.firstName
    ) {
        throw new Error("User must be updated in some way");
    }
    return true;
};


export interface INewAvatar {
    name: string;
    image: string;
}

export interface IAvatar {
    id: number;
    name: string;
    image: string;
}
export class AvatarConstants {
    public static AVATAR_NAME_MIN_LENGTH = 2;
    public static AVATAR_NAME_MAX_LENGTH = 32;
}

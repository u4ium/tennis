import { Rules } from "../../validation/rules";
import { IScheduleSectionAttributes } from "./Schedule";

class ScheduleConstants {
    public static TIME_REGEX = /(?:[01][0-9]|2[0-3])\:(?:00|15|30|45)(?:\:00)?/;
}

type ValidatedSectionAttributes = Omit<Omit<
    IScheduleSectionAttributes, "id">, "schedule">;

export const scheduleSectionValidators: Rules<ValidatedSectionAttributes> = {
    dayOfWeek: {
        isInt: {
            msg: `Invalid Schedule section day of week" +
                " (must be an integer)`,
        },
        min: {
            args: [0],
            msg: `Invalid Schedule section day of week" +
                " (must be non-negative)`
        },
        max: {
            args: [6],
            msg: `Invalid Schedule section day of week" +
                " (must be less than or equal to 6/Sunday)`
        },
    },
    start: {
        is: {
            args: [ScheduleConstants.TIME_REGEX.source],
            msg: `Invalid Schedule Section start` +
                ` (must be formatted HH:mm, 24-hour)`
        },
    },
    end: {
        is: {
            args: [ScheduleConstants.TIME_REGEX.source],
            msg: `Invalid Schedule Section end` +
                ` (must be formatted HH:mm, 24-hour)`
        },
    },
};

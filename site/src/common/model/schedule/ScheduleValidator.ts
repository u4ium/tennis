import { Rules } from "../../validation/rules";
import { ISchedule } from "./Schedule";

export class ScheduleConstants {
}

export const scheduleValidators: Rules<ISchedule> = {
    /** NOT FOR USE IN DB VALIDATION */
    sections: {
        // isJSON: {
        //    msg: "Invalid Schedule (must be JSON)"
        // },
        // is: {
        //    args: [/\[(?:\[.*\],){6}\[.*\]\]/.source],
        //    msg: "Invalid Schedule (must be array of arrays)"
        // }
    }
};

import { extract } from "../../../app/utils";
import HTTPStatus from "../../../server/util/HTTPStatus";
import makeValidator from "../../validation/validate";

export interface IScheduleSection {
    id?: number;
    start: string;
    end: string;
    schedule?: number;
}

export interface IScheduleSectionAttributes extends IScheduleSection {
    dayOfWeek: 0 | 1 | 2 | 3 | 4 | 5 | 6;
}

export type IScheduleSections = [
    IScheduleSection[],
    IScheduleSection[],
    IScheduleSection[],
    IScheduleSection[],
    IScheduleSection[],
    IScheduleSection[],
    IScheduleSection[]
];

export interface ISchedule {
    sections: IScheduleSections;
}

export interface NewSchedule {
    sections: IScheduleSectionAttributes[];
    user: number;
}

export const isScheduleSection = makeValidator<IScheduleSection>(
    "ScheduleSection",
    ["start", "end"]
);

export function isSchedule(schedule: unknown): schedule is ISchedule {
    if (!schedule || Object.entries(schedule).length === 0) {
        throw new HTTPStatus(400, "Schedule must be provided");
    }
    if (typeof (schedule) !== "object") {
        throw new HTTPStatus(400, "Schedule must be a valid JSON");
    }
    if (!("sections" in schedule)) {
        throw new HTTPStatus(400, "Schedule must have sections");
    }
    const sections = (schedule as { sections: unknown }).sections;
    if (typeof (sections) !== "object") {
        throw new HTTPStatus(400, "Schedule sections must be a list");
    }
    if ((sections as unknown[]).length !== 7) {
        throw new HTTPStatus(400, "Schedule sections must have length 7");
    }
    let index = 0;
    for (const day of (sections as unknown[])) {
        if (typeof day !== "object" || !("forEach" in day)) {
            throw new HTTPStatus(400, `Schedule Day ${index} must be list`);
        }
        (day as unknown[]).forEach(isScheduleSection);
        index++;
    }
    return true;
}

export const extractSchedule = extract<ISchedule>({
    sections: true,
});

export const extractSection = extract<IScheduleSectionAttributes>({
    id: false,
    start: true,
    end: true,
    dayOfWeek: true,
    schedule: false,
});


export interface ISet {
    position?: number;
    seedScore: number;
    challengerScore: number;
}

export type IMatchPlayer = ISinglesPlayer | IDoublesPlayer;

export interface IMatch<T = ISinglesPlayer | IDoublesPlayer> {
    id?: number;
    rule?: string;
    played?: Date;
    scheduled?: Date[];
    season?: number;
    seed: T;
    challenger: T;
    sets?: ISet[];
}

export interface INamed {
    firstName: string;
    lastName: string;
}


export interface ISinglesPlayer {
    player: number;
    Player?: INamed;
}

export interface IDoublesPlayer {
    player1: number;
    Player1?: INamed;
    player2: number;
    Player2?: INamed;
}

export type ISinglesMatch = IMatch<ISinglesPlayer>;
export type IDoublesMatch = IMatch<IDoublesPlayer>;
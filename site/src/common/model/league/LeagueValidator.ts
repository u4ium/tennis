import {
    allowedPunctuation,
    allowedPunctuationString,
    charRegex,
    Rules
} from "../../validation/rules";
import { ruleValidators } from "../rule/RuleValidator";
import ILeague from "./LeagueModel";

export class LeagueConstants {
    public static LEAGUE_NAME_MAX_LENGTH = 32;
    public static LEAGUE_ABOUT_MAX_LENGTH = 64;
    public static NAME_MATCHER = `[ ${charRegex}0-9]+`;
    public static ABOUT_MATCHER = `[\\s${charRegex}${allowedPunctuation}0-9]+`;
}

type ValidatedLeague = Omit<Omit<ILeague, "id">, "club">;

export const leagueValidators: Rules<ValidatedLeague> = {
    name: {
        len: {
            args: [1, LeagueConstants.LEAGUE_NAME_MAX_LENGTH],
            msg: `Invalid League name (must be between 1 and` +
                ` ${LeagueConstants.LEAGUE_NAME_MAX_LENGTH} characters)`,
        },
        is: {
            args: [LeagueConstants.NAME_MATCHER],
            msg: `Invalid League name (must be words, numbers and spaces only)`
        }
    },
    about: {
        len: {
            args: [1, LeagueConstants.LEAGUE_ABOUT_MAX_LENGTH],
            msg: `Invalid League about (must be between 0 and` +
                ` ${LeagueConstants.LEAGUE_ABOUT_MAX_LENGTH} characters)`,
        },
        is: {
            args: [LeagueConstants.ABOUT_MATCHER],
            msg: `Invalid League about (must be words, ` +
                `${allowedPunctuationString}) or whitespace.`,
        },
    },
    isDoubles: {},
    rule: ruleValidators.name,
    Rule: {},
    minSkill: {
        isInt: {
            msg: `Invalid League min skill (must be an integer)`,
        },
        min: {
            args: [0],
            msg: `Invalid League min skill (must be non-negative {1.0 or more})`
        },
        max: {
            args: [10],
            msg: `Invalid League min skill (must be less than or equal to 10 {6.0})`
        },
    },
    maxSkill: {
        isInt: {
            msg: `Invalid League max skill (must be an integer)`,
        },
        min: {
            args: [0],
            msg: `Invalid League max skill (must be non-negative {1.0 or more})`
        },
        max: {
            args: [10],
            msg: `Invalid League max skill (must be less than or equal to 10 {6.0})`
        },
    },
    owner: {

    },
    Seasons: {}
    // TODO
    /*
    RECURSE: { Rule: ruleValidator }
    */
};

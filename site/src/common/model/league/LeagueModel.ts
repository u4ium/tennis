import { extract } from "../../../app/utils";
import makeValidator from "../../validation/validate";
import IRule from "../rule/RuleModel";
import { ISeason } from "../season/Season";

export default interface ILeague {
    id?: number;
    club?: number;
    name: string;
    about: string;
    owner?: number;
    isDoubles: boolean;
    minSkill: number;
    maxSkill: number;
    rule?: string;
    Rule: IRule;
    Seasons?: ISeason[];
}

export const isNewLeague = makeValidator<ILeague>(
    "League",
    [
        "name",
        "about",
        "isDoubles",
        "minSkill",
        "maxSkill",
        "Rule",
        "rule",
    ],
    true
);

export const extractLeague = extract<ILeague>({
    id: false,
    club: false,
    name: true,
    about: true,
    owner: false,
    isDoubles: true,
    rule: true,
    Rule: true,
    minSkill: true,
    maxSkill: true,
    Seasons: false,
});

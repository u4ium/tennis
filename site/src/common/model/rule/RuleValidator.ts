import {
    charRegex,
    Rules
} from "../../validation/rules";
import IRule from "./RuleModel";

export class RuleConstants {
    public static RULE_NAME_MIN_LENGTH = 3;
    public static RULE_NAME_MAX_LENGTH = 64;
    public static RULE_NAME_MATCHER = `[ ${charRegex}0-9]+`;
    public static MIN_SETS_TO_WIN = 1;
    public static MAX_SETS_TO_WIN = 5;
    public static MIN_SET_WIN_SCORE = 1;
    public static MAX_SET_WIN_SCORE = 12;
    public static MIN_TIEBREAK_WIN_SCORE = 0;
    public static MAX_TIEBREAK_WIN_SCORE = 20;
}

export const ruleValidators: Rules<IRule> = {
    name: {
        len: {
            args: [
                RuleConstants.RULE_NAME_MIN_LENGTH,
                RuleConstants.RULE_NAME_MAX_LENGTH
            ],
            msg: `Invalid Rule name (must be ${RuleConstants.RULE_NAME_MIN_LENGTH}` +
                `-${RuleConstants.RULE_NAME_MAX_LENGTH} characters)`,
        },
        is: {
            args: [RuleConstants.RULE_NAME_MATCHER],
            msg: `Invalid Rule name (must be alphanumeric)`,
        },
    },
    setsToWin: {
        min: {
            args: [RuleConstants.MIN_SETS_TO_WIN],
            msg: `Invalid Rule sets to win ` +
                `(minimum ${RuleConstants.MIN_SETS_TO_WIN})`,
        },
        max: {
            args: [RuleConstants.MAX_SETS_TO_WIN],
            msg: `Invalid Rule sets to win ` +
                `(maximum ${RuleConstants.MAX_SETS_TO_WIN})`,
        },
    },
    setWinScore: {
        min: {
            args: [RuleConstants.MIN_SET_WIN_SCORE],
            msg: `Invalid Rule set win score ` +
                `(minimum ${RuleConstants.MIN_SET_WIN_SCORE})`,
        },
        max: {
            args: [RuleConstants.MAX_SET_WIN_SCORE],
            msg: `Invalid Rule set win score ` +
                `(maximum ${RuleConstants.MAX_SET_WIN_SCORE})`,
        },
    },
    tiebreakWinScore: {
        min: {
            args: [RuleConstants.MIN_TIEBREAK_WIN_SCORE],
            msg: `Invalid Rule tiebreak win score ` +
                `(minimum ${RuleConstants.MIN_TIEBREAK_WIN_SCORE})`,
        },
        max: {
            args: [RuleConstants.MAX_TIEBREAK_WIN_SCORE],
            msg: `Invalid Rule tiebreak win score ` +
                `(maximum ${RuleConstants.MAX_TIEBREAK_WIN_SCORE})`,
        },
    },

};

export default interface IRule {
    name: string;
    setsToWin: number;
    setWinScore: number;
    tiebreakWinScore: number;
}
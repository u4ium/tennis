export interface ISeason {
    id?: number;
    participants?: Array<{ id: number; }>;
    start: string;
    end: string;
    length: number;
    matchesPerCycle: number;
}


import { Rules } from "../../validation/rules";
import { ISeason } from "./Season";

export class SeasonConstants {
    public static SEASON_MAX_LENGTH = 365;
    public static MATCHES_PER_CYCLE_MAX = 100;
}

export function hasValidEndDate() {
    const minSession = new Date(this.start);
    minSession.setDate(minSession.getDate() + this.length);
    if (minSession > this.end) {
        throw new Error(
            `Invalid Season end date: ${this.end} (must be at least "length" days after start)`
        );
    }
}

type ValidatedSeason = Omit<Omit<ISeason, "id">, "participants">;

export const seasonValidators: Rules<ValidatedSeason> = {
    start: {
        isDate: {
            msg: "Start must be a valid date."
        }
    },
    end: {
        isDate: {
            msg: "Start must be a valid date."
        }
    },
    length: {
        min: {
            args: [1],
            msg: `Invalid Season length (must be between` +
                ` 1 and ${SeasonConstants.SEASON_MAX_LENGTH})`,
        },
        max: {
            args: [SeasonConstants.SEASON_MAX_LENGTH],
            msg: `Invalid Season length (must be between` +
                ` 1 and ${SeasonConstants.SEASON_MAX_LENGTH})`,
        }
    },
    matchesPerCycle: {
        min: {
            args: [1],
            msg: `Invalid Season matches per cycle (must` +
                ` be between 1 and ${SeasonConstants.MATCHES_PER_CYCLE_MAX})`,
        },
        max: {
            args: [SeasonConstants.MATCHES_PER_CYCLE_MAX],
            msg: `Invalid Season matches per cycle (must` +
                ` be between 1 and ${SeasonConstants.MATCHES_PER_CYCLE_MAX})`,
        }
    },
};

import { extract } from "../../../app/utils";
import makeValidator from "../../validation/validate";
import { IAvatar, INewAvatar } from "../avatar/Avatar";

export interface IPatchUser {
    email?: string;
    password?: string;
    firstName?: string;
    lastName?: string;
    bio?: string;
    skill?: number;
    avatar?: number;
    Avatar?: IAvatar | INewAvatar;
}

export const isPatchUser = makeValidator<IPatchUser>(
    "User",
    [
        "email",
        "password",
        "firstName",
        "lastName",
        "skill",
        "bio"
    ],
    true
);

export const extractPatchUser = extract<IPatchUser>({
    email: true,
    password: true,
    firstName: true,
    lastName: true,
    bio: true,
    skill: true,
    avatar: true,
    Avatar: true,
});

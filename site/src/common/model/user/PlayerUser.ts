import { IAvatar } from "../avatar/Avatar";

export interface IPlayerUser {
    id: number;
    firstName: string;
    lastName: string;
    Avatar?: IAvatar;
}
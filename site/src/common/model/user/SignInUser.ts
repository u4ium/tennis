import { extract } from "../../../app/utils";
import makeValidator from "../../validation/validate";

export interface ISignInUser {
    email: string;
    password: string;
}

const signInUserValidator = makeValidator<ISignInUser>(
    "User", ["email", "password"]);

export function isSignInUser(user: unknown) {
    return signInUserValidator(user);
}

export const extractSignInUser = extract<ISignInUser>({
    email: true,
    password: true,
});

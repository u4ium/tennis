import { extract } from "../../../app/utils";
import makeValidator from "../../validation/validate";

export interface ISignUpUser {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
}

export const isSignUpUser = makeValidator<ISignUpUser>(
    "User", ["email", "password", "firstName", "lastName"]);

export const extractSignUpUser = extract<ISignUpUser>({
    email: true,
    password: true,
    firstName: true,
    lastName: true,
});

import {
    allowedPunctuation,
    allowedPunctuationString,
    charRegex,
    Rules
} from "../../validation/rules";
import { IPatchUser } from "./PatchUser";

export class UserConstants {
    public static PASSWORD_MIN_LENGTH = 5;
    public static PASSWORD_MAX_LENGTH = 512;
    public static PASSWORD_HASH_LENGTH = 88;
    public static PASSWORD_SALT_LENGTH = 24;
    public static NAME_MAX_LENGTH = 256;
    public static BIO_MAX_LENGTH = 5000;
    public static EMAIL_MAX_LENGTH = 320;
    public static NAME_MATCHER = `[${charRegex}]+`;
    public static BIO_MATCHER = `[\\s${charRegex}${allowedPunctuation}0-9]*`;
}

interface AllUserProperties extends Omit<IPatchUser, "Avatar"> {
    isAdministrator: boolean;
    passwordHash: string;
    passwordSalt: string;
}

export const userValidators: Rules<AllUserProperties> = {
    password: {
        len: {
            args: [
                UserConstants.PASSWORD_MIN_LENGTH,
                UserConstants.PASSWORD_MAX_LENGTH
            ],
            msg: `Invalid User password: (must be at` +
                ` least ${UserConstants.PASSWORD_MIN_LENGTH} characters)`,
        }
    },
    passwordHash: {
        len: {
            args: [
                UserConstants.PASSWORD_HASH_LENGTH,
                UserConstants.PASSWORD_HASH_LENGTH
            ],
            msg: `Invalid User password hash:` +
                `(must have length ${UserConstants.PASSWORD_HASH_LENGTH})`,
        },
        isBase64: {
            msg: `Invalid User password hash: not base64-encoded sha512 hash`,
        },
    },
    passwordSalt: {
        len: {
            args: [
                UserConstants.PASSWORD_SALT_LENGTH,
                UserConstants.PASSWORD_SALT_LENGTH
            ],
            msg: `Invalid User password salt ` +
                `(must have length ${UserConstants.PASSWORD_SALT_LENGTH})`,
        },
        isBase64: {
            msg: `Invalid User password salt (must be base64 encoded)`,
        },
    },
    email: {
        isEmail: { msg: `Invalid User email (not an email address)` },
        len: {
            args: [2, UserConstants.EMAIL_MAX_LENGTH],
            msg: `Invalid User email ` +
                `(must be 2-${UserConstants.EMAIL_MAX_LENGTH} characters)`,
        },
    },
    firstName: {
        is: {
            args: [UserConstants.NAME_MATCHER],
            msg: `Invalid User first name (not alphabetic)`,
        },
        len: {
            args: [1, UserConstants.NAME_MAX_LENGTH],
            msg: `Invalid User first name (too short/long)`,
        },
    },
    lastName: {
        is: {
            args: [UserConstants.NAME_MATCHER],
            msg: `Invalid User last name (not alphabetic)`,
        },
        len: {
            args: [1, UserConstants.NAME_MAX_LENGTH],
            msg: `Invalid User last name (too short/long)`,
        },
    },
    bio: {
        len: {
            args: [0, UserConstants.BIO_MAX_LENGTH],
            msg: `Invalid User bio (must be between 0 and 5000 characters)`,
        },
        is: {
            args: [UserConstants.BIO_MATCHER],
            msg: `Invalid User bio (must be words, numbers, ` +
                `${allowedPunctuationString}) or whitespace.`,
        },
    },
    skill: {
        isInt: {
            msg: `Invalid User skill (must be an integer)`,
        },
        min: {
            args: [0],
            msg: `Invalid User skill (must be non-negative {1.0 or more})`
        },
        max: {
            args: [10],
            msg: `Invalid User skill (must be less than or equal to 10 {6.0})`
        },
    },
    isAdministrator: {
        isBoolean: {
            msg: "Invalid User is administrator flag (must be true/false)",
        },
    },
    avatar: {
        isInt: {
            msg: "Invalid avatar ID",
        }
    }
};

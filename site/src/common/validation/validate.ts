import validator from "validator";
import { leagueValidators } from "../model/league/LeagueValidator";
import { scheduleSectionValidators } from "../model/schedule/ScheduleSectionValidator";
import { userValidators } from "../model/user/UserValidator";
import { Rule, Rules } from "./rules";

type ValidatedObjects = "User" | "Invite" | "League" | "ScheduleSection";

function getValidator(name: string) {
    const v = validator;
    switch (name) {
        case "is": return v.matches;
        case "len": return v.isLength;
        case "min": return (n: string, a: number) => v.isInt(n, { min: a });
        case "max": return (n: string, a: number) => v.isInt(n, { max: a });
        default: return (v as any)[name];
    }
}

function getObjectValidator<T>(objectName: ValidatedObjects): Rules<T> {
    return {
        User: userValidators,
        Invite: userValidators, // TODO
        League: leagueValidators,
        ScheduleSection: scheduleSectionValidators, // TODO
    }[objectName] as Rules<T>;
}

function validate(field: any, rules: Record<string, Rule>) {
    for (const [ruleName, rule] of Object.entries(rules)) {
        const validationFunction = getValidator(ruleName);
        const args = rule.args || [];
        const msg = rule.msg || `Invalid: ${field} (${ruleName} failed)`;
        if (!validationFunction(`${field}`, ...args)) {
            throw new Error(msg);
        }
    }
}

export default function makeValidator<T>(
    objectName: ValidatedObjects,
    fields: Array<keyof T>,
    optional: boolean = false
) {
    const validators = getObjectValidator<T>(objectName);
    return (object: unknown): object is T => {
        if (typeof object !== "object" || Object.keys(object).length === 0) {
            throw new Error(`${objectName} may not be undefined or empty`);
        } else {
            for (const field of fields) {
                if (field in object) {
                    validate((object as any)[field], validators[field]);
                } else if (!optional) {
                    throw new Error(`${field} required`);
                }
            }
        }
        return true;
    };
}

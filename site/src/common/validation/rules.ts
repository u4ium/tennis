export const charRegex = "a-zA-ZÀ-ÖØ-öø-ÿ(?:\\p{L}\\p{M}?)";
export const allowedPunctuation = ",.!?:;";
export const allowedPunctuationString = allowedPunctuation.split("").map(
    (p) => "'" + p + "'").join(", ");

export interface Rule {
    args?: Array<string | number>;
    msg: string;
}

export type Rules<T> = Record<keyof T,
    Partial<Record<
        string,
        // keyof SequelizeValidator |
        //          "isJSON" |
        //          "isBoolean" |
        //          "isBase64" |
        //          "isEmail",
        Rule
    >>
>;

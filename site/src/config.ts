// TODO: make sure that server-side environment variables are not sent to client
const {
    NODE_ENV = "development",
    ADMIN_EMAIL = "admin@tennisladder.ca",
    CLUB_ID = 1,

    SSL_CERTIFICATE_PATH = "../ssl",
    SERVER_HTTP_PORT = "8000",
    SERVER_HTTPS_PORT = "8001",
    SERVER_ADDRESS = "localhost",
    SERVER_PROTOCOL = "http",
    SESSION_SECRET = "K33P IT S3CR3T, K33P IT S4FE",

    DATABASE_NAME = "tennis",
    DATABASE_USER = "tennis",
    DATABASE_PASSWORD = "tennis",
    DATABASE_URL = "localhost",
    DATABASE_PORT = "3306",
} = process.env;

export {
    NODE_ENV,
    ADMIN_EMAIL,
    CLUB_ID,

    SSL_CERTIFICATE_PATH,
    SERVER_ADDRESS,
    SERVER_HTTP_PORT,
    SERVER_HTTPS_PORT,
    SERVER_PROTOCOL,
    SESSION_SECRET,

    DATABASE_NAME,
    DATABASE_USER,
    DATABASE_PASSWORD,
    DATABASE_URL,
    DATABASE_PORT,
};

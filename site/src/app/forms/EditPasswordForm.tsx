import React, { useContext } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import { UserConstants } from "../../common/model/user/UserValidator";
import { ErrorContext } from "../contexts/ErrorContext";
import { UserContext } from "../contexts/UserContext";
import useFormData from "../effects/useFormData";
import useFormValidateOnSubmit from "../effects/useFormValidateOnSubmit";
import { editUser } from "../requests/user";
import { escapeRegex } from "../utils";

const blankPasswords = {
    password: "",
    password2: "",
};
export function EditPasswordForm({ close }: { close: VoidFunction }) {
    const {
        formData,
        setFormData,
        updateFormData
    } = useFormData(blankPasswords);

    const { onError } = useContext(ErrorContext);
    const { userState } = useContext(UserContext);
    const { validated, handleSubmit } = useFormValidateOnSubmit(async () => {
        await editUser({
            id: userState.id,
            body: { password: formData.password }
        });
        setFormData(blankPasswords);
        close();
        return true;
    }, onError);

    return (<>
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
            <Form.Control
                hidden
                readOnly
                type="email"
                autoComplete="username"
                value={userState.email}
            />
            <Form.Group as={Form.Row} controlId="formLoginPassword">
                <Form.Label column md={4}>New Password</Form.Label>
                <Col>
                    <Form.Control
                        required
                        value={formData.password}
                        onChange={updateFormData}
                        name="password"
                        type="password"
                        minLength={UserConstants.PASSWORD_MIN_LENGTH}
                        maxLength={UserConstants.PASSWORD_MAX_LENGTH}
                        autoComplete={"new-password"}
                        placeholder="New Password"
                    />
                    <Form.Control.Feedback type="invalid" tooltip>
                        Please set a new password.
                    </Form.Control.Feedback>
                </Col>
            </Form.Group>
            <Form.Group as={Form.Row} controlId="formEditLoginPassword2">
                <Form.Label column md={4}>
                    Confirm Password
                </Form.Label>
                <Col>
                    <Form.Control
                        required
                        value={formData.password2}
                        onChange={updateFormData}
                        name="password2"
                        type="password"
                        pattern={escapeRegex(formData.password)}
                        autoComplete="new-password"
                        placeholder="Repeat Password"
                    />
                    <Form.Control.Feedback type="invalid" tooltip>
                        Passwords must match.
                    </Form.Control.Feedback>
                </Col>
            </Form.Group>
            <br />
            <Form.Row className="justify-content-between">
                <Col xs="auto">
                    <Button variant="secondary" onClick={close}>Cancel</Button>
                </Col>
                <Col xs="auto">
                    <Button variant="primary" type="submit">Submit</Button>
                </Col>
            </Form.Row>
        </Form>
    </>);
}

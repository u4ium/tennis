import React, { useContext, useState } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import ILeague from "../../common/model/league/LeagueModel";
import { ISeason } from "../../common/model/season/Season";
import IconButton from "../components/IconButton";
import { SeasonInfo } from "../components/SeasonInfo";
import { ErrorContext } from "../contexts/ErrorContext";
import { LeaguesContext } from "../contexts/LeaguesContext";
import useFormData from "../effects/useFormData";
import useFormValidateOnSubmit from "../effects/useFormValidateOnSubmit";
import { addDoublesSeason, addSinglesSeason } from "../requests/season";
import { SeasonSubForm } from "./SeasonSubForm";

export function ManageLeagueForm({ league }: { league: ILeague; }) {
    const { leaguesState, setLeaguesState } = useContext(LeaguesContext);
    const currentSeason = league.Seasons?.[0];
    const [isNewSeasonFormOpen, setIsNewSeasonFormOpen] = useState(false);
    const leagueIndex = leaguesState.findIndex(
        (l) => (l.id === league.id && l.isDoubles === league.isDoubles));

    const { formData, updateFormData } = useFormData<ISeason>({
        start: "",
        end: "",
        length: 7,
        matchesPerCycle: 1,
    });

    const { onError } = useContext(ErrorContext);
    const { validated, handleSubmit } = useFormValidateOnSubmit(async () => {
        const newLeague = {
            id: league.id,
            body: formData,
        };
        const id = (league.isDoubles
            ? await addDoublesSeason(newLeague)
            : await addSinglesSeason(newLeague));
        setLeaguesState((p) => {
            p[leagueIndex].Seasons.push({ ...formData, ...id });
            return [...p];
        });
        setIsNewSeasonFormOpen(false);
        return false;
    }, onError);

    return (
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
            <h4>
                {league.name} ({league.isDoubles ? "Doubles" : "Singles"})
            </h4>
            <Form.Group as={Form.Row} >
                <Form.Label column md={3}> Seasons </Form.Label>
                {currentSeason
                    ? <>
                        <Col>
                            <SeasonInfo {...currentSeason} />
                        </Col>
                        <Col xs="auto">
                            <IconButton iconName="icono-rename" variant="warning" />
                        </Col>
                    </>
                    : (isNewSeasonFormOpen
                        ? <SeasonSubForm
                            {...formData}
                            updateFormData={updateFormData}
                            close={() => setIsNewSeasonFormOpen(false)} />
                        : <Col>
                            <Form.Group as={Form.Row}>
                                <Col xs={2} className="d-md-none" />
                                <Form.Label column>
                                    None
                                </Form.Label>
                                <Button onClick={() =>
                                    setIsNewSeasonFormOpen(true)}
                                >
                                    New
                                </Button>
                            </Form.Group>
                        </Col>
                    )}
            </Form.Group>
        </Form>
    );

}

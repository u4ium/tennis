export type FormElement = HTMLInputElement & HTMLTextAreaElement & HTMLSelectElement;
export type FormChangeEvent = React.ChangeEvent<FormElement>;
export type FormActiveData<T> = Record<keyof T, boolean>;
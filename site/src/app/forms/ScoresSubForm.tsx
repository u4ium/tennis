import React from "react";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import { ScoreInputBox } from "../components/ScoreInputBox";

export function ScoresSubForm(
    { scores, setScore, setsToWin, setWinScore, isDoubles }: {
        scores: Array<[number, number]>;
        setScore: (index: number, which: 0 | 1, value: number) => void;
        setsToWin: number;
        setWinScore: number;
        isDoubles: boolean;
    }
) {
    const iMF = (i: number) => isMatchFinishedBefore(i,
        scores, setsToWin, setWinScore);

    return <>
        {[...Array(setsToWin * 2 - 1)].map(
            (_, i) => i < setsToWin || !iMF(i)
                ? <InputGroup key={i} hasValidation>
                    <InputGroup.Prepend>
                        <InputGroup.Text>Set #{i + 1}</InputGroup.Text>
                    </InputGroup.Prepend>
                    <ScoreInputBox
                        score={scores[i]?.[0]}
                        other={scores[i]?.[1]}
                        placeholder="Your Score"
                        setScore={(v) => setScore(i, 0, v)}
                        winScore={setWinScore} />
                    <ScoreInputBox
                        score={scores[i]?.[1]}
                        other={scores[i]?.[0]}
                        placeholder={`Opponent${isDoubles ? "s'" : "'s"} Score`}
                        setScore={(v) => setScore(i, 1, v)}
                        winScore={setWinScore} />
                    <Form.Control.Feedback type="invalid" tooltip>
                        <small>
                            Invalid set score.
                            Must win {setWinScore} games by margin of 2.
                        </small>
                    </Form.Control.Feedback>
                </InputGroup>
                : null
        )}
    </>;

}

export function isMatchFinishedBefore(
    i: number,
    scores: Array<[number, number]>,
    setsToWin: number,
    setWinScore: number
) {
    return scores.slice(0, i).reduce(
        ([pa, pb], [a, b]) => {
            if (isNaN(a) || isNaN(b)) {
                return [setsToWin, setsToWin]; // value not entered
            } else if (pa === setsToWin || pb === setsToWin) {
                return [pa, pb]; // match already finished
            } else if (a < setWinScore && b < setWinScore) {
                return [pa, pb]; // at least one score too low
            } else if (a > b) {
                return [pa + 1, pb]; // a wins
            } else if (b > a) {
                return [pa, pb + 1]; // b wins
            } else {
                return [pa, pb]; // tie
            }
        }, [0, 0]).includes(setsToWin);
}

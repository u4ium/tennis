import React from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import { ISeason } from "../../common/model/season/Season";
import { SeasonConstants } from "../../common/model/season/SeasonValidator";
import { nextYear, toValidationDate } from "../utils";

export function SeasonSubForm({
    length,
    matchesPerCycle,
    start,
    end,
    updateFormData,
    close
}:
    ISeason & {
        updateFormData: (event: React.ChangeEvent<HTMLInputElement>) => void;
        close: VoidFunction;
    }) {
    const today = new Date();

    return (<>
        <Col className="d-md-none" xs={2} />
        <Col xs={10} md={9}>
            <Form.Group as={Form.Row} >
                <Form.Label column> Days Per Cycle </Form.Label>
                <Col xs={12} sm={6}>
                    <Form.Control
                        required
                        type="number"
                        name="length"
                        min={1}
                        max={SeasonConstants.SEASON_MAX_LENGTH}
                        value={length}
                        onChange={updateFormData} />
                    <Form.Control.Feedback type="invalid" tooltip>
                        <small>
                            Must be from 1 to {
                                SeasonConstants.SEASON_MAX_LENGTH
                            } days
                        </small>
                    </Form.Control.Feedback>
                </Col>
            </Form.Group>
            <Form.Group as={Form.Row} >
                <Form.Label column> Matches Per Cycle </Form.Label>
                <Col xs={12} sm={6}>
                    <Form.Control
                        required
                        type="number"
                        name="matchesPerCycle"
                        min={1}
                        max={SeasonConstants.MATCHES_PER_CYCLE_MAX}
                        value={matchesPerCycle}
                        onChange={updateFormData} />
                    <Form.Control.Feedback type="invalid" tooltip>
                        <small>
                            Must be from 1 to {
                                SeasonConstants.MATCHES_PER_CYCLE_MAX
                            } matches
                        </small>
                    </Form.Control.Feedback>
                </Col>
            </Form.Group>
            <Form.Group as={Form.Row} >
                <Form.Label column> Start Date </Form.Label>
                <Col xs={12} sm={6}>
                    <Form.Control
                        required
                        type="date"
                        name="start"
                        min={toValidationDate(today)}
                        max={toValidationDate(nextYear(today))}
                        value={start}
                        onChange={updateFormData} />
                    <Form.Control.Feedback type="invalid" tooltip>
                        <small>
                            Must be after today
                        </small>
                    </Form.Control.Feedback>
                </Col>
            </Form.Group>
            <Form.Group as={Form.Row} >
                <Form.Label column> End Date </Form.Label>
                <Col xs={12} sm={6}>
                    <Form.Control
                        required
                        type="date"
                        name="end"
                        min={start
                            ? toValidationDate(new Date(start))
                            : toValidationDate(today)}
                        max={start
                            ? toValidationDate(nextYear(new Date(start)))
                            : toValidationDate(nextYear(today))}
                        step={length}
                        value={end}
                        onChange={updateFormData} />
                    <Form.Control.Feedback type="invalid" tooltip>
                        <small>
                            Must be some multiple of "days per cycle"
                            days after start date
                        </small>
                    </Form.Control.Feedback>
                </Col>
            </Form.Group>
            <Form.Row className="justify-content-between">
                <Col xs="auto">
                    <Button variant="secondary" onClick={close}>
                        Cancel
                    </Button>
                </Col>
                <Col xs="auto">
                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </Col>
            </Form.Row>
        </Col>

    </>);
}

import React, { useContext } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import ILeague from "../../common/model/league/LeagueModel";
import { LeagueConstants } from "../../common/model/league/LeagueValidator";
import IRule from "../../common/model/rule/RuleModel";
import { RuleConstants } from "../../common/model/rule/RuleValidator";
import RuleSelector from "../components/RuleSelector";
import { SkillSelector } from "../components/SkillSelector";
import { ErrorContext } from "../contexts/ErrorContext";
import { LeaguesContext } from "../contexts/LeaguesContext";
import { RulesContext } from "../contexts/RulesContext";
import { UserContext } from "../contexts/UserContext";
import useFormData from "../effects/useFormData";
import useFormValidateOnSubmit from "../effects/useFormValidateOnSubmit";
import { addLeague } from "../requests/league";

// tslint:disable:max-line-length

export function NewLeagueForm({ close }: { close: VoidFunction; }) {
    const { userState } = useContext(UserContext);
    const { rulesState } = useContext(RulesContext);
    const { onError } = useContext(ErrorContext);
    const { setLeaguesState } = useContext(LeaguesContext);

    const { formData, setFormData, updateFormData } = useFormData<ILeague>(({
        name: "",
        about: "",
        rule: rulesState?.[0]?.name || "",
        Rule: null,
        isDoubles: false,
        minSkill: 0,
        maxSkill: 10,
    }));

    const {
        formData: newRule,
        setFormData: setNewRule,
        updateFormData: updateNewRule,
    } = useFormData<IRule>(null);
    const currentRuleIndex = rulesState.findIndex(
        (r) => r.name === formData.rule);

    const {
        validated,
        handleSubmit
    } = useFormValidateOnSubmit(
        async () => {
            const newLeague = newRule
                ? { ...formData, Rule: newRule, rule: newRule.name }
                : formData;
            const { id } = await addLeague({ body: newLeague });
            setLeaguesState((p) => [
                ...p,
                {
                    ...newLeague,
                    owner: userState.id,
                    id,
                    Rule: (newRule || rulesState[currentRuleIndex])
                }
            ]);
            close();
            return false;
        },
        onError
    );

    return (
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
            <Form.Group as={Form.Row}>
                <Form.Text>
                    <h3>
                        Create New {formData.isDoubles ?
                            "Doubles" : "Singles"} League
                    </h3>
                </Form.Text>
            </Form.Group>
            <Form.Group as={Form.Row} controlId="formName">
                <Form.Label column md={3}>Name</Form.Label>
                <Col>
                    <InputGroup hasValidation className="align-items-center">
                        <Form.Control
                            required
                            name="name"
                            value={formData.name}
                            onChange={updateFormData}
                            maxLength={LeagueConstants.LEAGUE_NAME_MAX_LENGTH}
                            pattern={LeagueConstants.NAME_MATCHER}
                            placeholder="Enter League Name" />
                        <Form.Control.Feedback type="invalid" tooltip>
                            <small>
                                Please enter a valid league name (max
                                {` ${LeagueConstants.LEAGUE_ABOUT_MAX_LENGTH} `}
                                characters: letters, numbers and spaces only)
                            </small>
                        </Form.Control.Feedback>
                        <InputGroup.Append>
                            <Form.Control
                                as="select"
                                name="type"
                                onChange={(e) => setFormData((p) => ({
                                    ...p,
                                    isDoubles: e.target.value === "Doubles"
                                }))}
                                value={formData.isDoubles ? "Doubles" : "Singles"}
                            >
                                <option>Singles</option>
                                <option>Doubles</option>
                            </Form.Control>
                        </InputGroup.Append>
                    </InputGroup>
                </Col>
            </Form.Group>
            <Form.Group as={Form.Row} controlId="formAbout">
                <Form.Label column md={3}>About</Form.Label>
                <Col>
                    <Form.Control
                        required
                        name="about"
                        value={formData.about}
                        onChange={updateFormData}
                        maxLength={LeagueConstants.LEAGUE_ABOUT_MAX_LENGTH}
                        pattern={LeagueConstants.ABOUT_MATCHER}
                        placeholder="Describe this League" />
                    <Form.Control.Feedback type="invalid" tooltip>
                        <small>
                            Please enter a valid league description (max
                            {` ${LeagueConstants.LEAGUE_ABOUT_MAX_LENGTH} `}
                            characters: letters, numbers, punctuation and
                            spaces only)
                        </small>
                    </Form.Control.Feedback>
                </Col>
            </Form.Group>
            <Form.Group as={Form.Row}>
                <Form.Label column md={3}>Skill</Form.Label>
                <Col sm={12} md={4}>
                    <InputGroup hasValidation >
                        <InputGroup.Prepend >
                            <InputGroup.Text>
                                <pre className="mb-0">minimum</pre>
                            </InputGroup.Text>
                        </InputGroup.Prepend>
                        <SkillSelector
                            name="minSkill"
                            value={formData.minSkill}
                            setValue={(value) => setFormData((p) => ({
                                ...p,
                                minSkill: value
                            }))}
                            max={formData.maxSkill}
                        />
                    </InputGroup>
                </Col>
                <Col className="d-none d-md-flex" xs={1} />
                <Col sm={12} md={4}>
                    <InputGroup hasValidation >
                        <InputGroup.Prepend >
                            <InputGroup.Text>
                                <pre className="mb-0">maximum</pre>
                            </InputGroup.Text>
                        </InputGroup.Prepend>
                        <SkillSelector
                            name="maxSkill"
                            value={formData.maxSkill}
                            setValue={(value) => setFormData((p) => ({
                                ...p,
                                maxSkill: value
                            }))}
                            min={formData.minSkill}
                        />
                    </InputGroup>
                </Col>
            </Form.Group>
            <Form.Row>
                <Form.Label column md={3}>
                    {newRule
                        ? "Define a New Rule"
                        : "Select Rules"}
                </Form.Label>
                <Col xs={12} md={9}>
                    <Form.Group controlId="formRuleName">
                        <InputGroup hasValidation>
                            {newRule
                                ? <>
                                    <Form.Control
                                        required
                                        name="name"
                                        value={newRule.name}
                                        onChange={updateNewRule}
                                        minLength={RuleConstants.RULE_NAME_MIN_LENGTH}
                                        maxLength={RuleConstants.RULE_NAME_MAX_LENGTH}
                                        pattern={RuleConstants.RULE_NAME_MATCHER}
                                        placeholder="Name your new Rule (e.g. Fast4)"
                                        className="text-truncate" />
                                    <Form.Control.Feedback type="invalid" tooltip>
                                        <small>
                                            Please enter a valid rule name (
                                            {`${RuleConstants.RULE_NAME_MIN_LENGTH
                                                }-${RuleConstants.RULE_NAME_MAX_LENGTH
                                                } `} characters: letters, numbers, and
                                            spaces only)
                                        </small>
                                    </Form.Control.Feedback>
                                </>
                                : <RuleSelector
                                    rule={formData.rule}
                                    updateFormData={updateFormData} />}
                            <Button
                                active={newRule !== null}
                                variant={newRule ? "secondary" : "success"}
                                onClick={() => newRule ?
                                    setNewRule(null) :
                                    setNewRule({
                                        name: "",
                                        setsToWin: 1,
                                        setWinScore: 6,
                                        tiebreakWinScore: 7
                                    })}
                            >
                                {newRule ? "Use Existing Rule" : "New Rule"}
                            </Button>

                        </InputGroup>
                    </Form.Group>
                    <Form.Row className="justify-content-between">

                        <Col xs={12} sm={4}>
                            <Form.Group
                                as={Form.Row}
                                controlId="formSetsToWin"
                                className="justify-content-start"
                            >
                                <Col xs={2} className="d-sm-none"></Col>
                                <Form.Label column xs={7} sm="auto">Sets: </Form.Label>
                                {newRule
                                    ? <Col xs={3} sm={6}>
                                        <Form.Control
                                            name="setsToWin"
                                            value={newRule.setsToWin}
                                            onChange={newRule ? updateNewRule : null}
                                            type="number"
                                            min={RuleConstants.MIN_SETS_TO_WIN}
                                            max={RuleConstants.MAX_SETS_TO_WIN} />
                                        <Form.Control.Feedback type="invalid" tooltip>
                                            <small>
                                                Must be
                                                between {RuleConstants.MIN_SETS_TO_WIN}
                                                -{RuleConstants.MAX_SETS_TO_WIN} sets.
                                            </small>
                                        </Form.Control.Feedback>
                                    </Col>
                                    : <Form.Label column>
                                        {rulesState[currentRuleIndex]?.setsToWin}
                                        {rulesState[currentRuleIndex]?.setsToWin > 1 ?
                                            `-${2 * rulesState[currentRuleIndex]?.setsToWin - 1} sets`
                                            : " set"}
                                    </Form.Label>}
                                <Col xs="auto" className="p-0" />
                            </Form.Group>
                        </Col>
                        <Col xs={12} sm={4}>
                            <Form.Group
                                as={Form.Row}
                                controlId="formSetWinScore"
                                className="justify-content-center"
                            >
                                <Col xs={2} className="d-sm-none"></Col>
                                <Form.Label column xs={7} sm="auto">Games: </Form.Label>
                                {newRule
                                    ? <Col xs={3} sm={6}>
                                        <Form.Control
                                            name="setWinScore"
                                            value={newRule.setWinScore}
                                            onChange={newRule ? updateNewRule : null}
                                            type="number"
                                            min={RuleConstants.MIN_SET_WIN_SCORE}
                                            max={RuleConstants.MAX_SET_WIN_SCORE} />
                                        <Form.Control.Feedback type="invalid" tooltip>
                                            <small>
                                                Must be between {RuleConstants.MIN_SET_WIN_SCORE}
                                                -{RuleConstants.MAX_SET_WIN_SCORE} games.
                                            </small>
                                        </Form.Control.Feedback>
                                    </Col>
                                    : <Form.Label column>
                                        first to {rulesState[currentRuleIndex]?.setWinScore}
                                    </Form.Label>}
                                <Col xs="auto" className="p-0" />
                            </Form.Group>
                        </Col>
                        <Col xs={12} sm={4}>
                            <Form.Group
                                as={Form.Row}
                                controlId="formTiebreakWinScore"
                                className="justify-content-end"
                            >
                                <Col xs={2} className="d-sm-none"></Col>
                                <Form.Label column xs={7} sm="auto">Tiebreak: </Form.Label>
                                {newRule
                                    ? <Col xs={3} sm={6}>
                                        <Form.Control
                                            name="tiebreakWinScore"
                                            value={newRule.tiebreakWinScore}
                                            onChange={newRule ? updateNewRule : null}
                                            type="number"
                                            min={RuleConstants.MIN_TIEBREAK_WIN_SCORE}
                                            max={RuleConstants.MAX_TIEBREAK_WIN_SCORE} />
                                        <Form.Control.Feedback type="invalid" tooltip>
                                            <small>
                                                Must be between {RuleConstants.MIN_TIEBREAK_WIN_SCORE}
                                                -{RuleConstants.MAX_TIEBREAK_WIN_SCORE} points.
                                            </small>
                                        </Form.Control.Feedback>
                                    </Col>
                                    : <Form.Label column>
                                        {rulesState[currentRuleIndex]?.tiebreakWinScore} points
                                    </Form.Label>}
                                <Col xs="auto" className="p-0" />
                            </Form.Group>
                        </Col>
                    </Form.Row>
                </Col>
            </Form.Row>
            <br />
            <Form.Row className="justify-content-between">
                <Col xs="auto">
                    <Button variant="secondary" onClick={close}>Cancel</Button>
                </Col>
                <Col xs="auto">
                    <Button variant="primary" type="submit">Submit</Button>
                </Col>
            </Form.Row>
        </Form>
    );
}

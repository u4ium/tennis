import React, { useContext, useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import { IPatchUser } from "../../common/model/user/PatchUser";
import { UserConstants } from "../../common/model/user/UserValidator";
import IconButton from "../components/IconButton";
import NameInput from "../components/NameInput";
import { ProfileCard } from "../components/ProfileCard";
import { SkillSelector } from "../components/SkillSelector";
import { ErrorContext } from "../contexts/ErrorContext";
import { UserContext } from "../contexts/UserContext";
import useFocusEditable from "../effects/useFocusEditable";
import useFormData from "../effects/useFormData";
import useFormValidateOnSubmit from "../effects/useFormValidateOnSubmit";
import { AuthUserProps } from "../props";
import { editUser } from "../requests/user";
import { extract } from "../utils";
import { resizeImage } from "../utils/resizeImage";
import {
    FormActiveData,
    FormChangeEvent
} from "./types/FormElements";

export type ProfileFormActiveData = FormActiveData<IPatchUser>;

function getIsUnset<T>(object: Partial<T>): FormActiveData<T> {
    return Object.entries(object).reduce(
        (p, [key, value]) => ({ ...p, [key]: !value }),
        {} as FormActiveData<T>);
}

export function ProfileForm({ close }: { close: VoidFunction; }) {
    const { userState, setUserState } = useContext(UserContext);
    const { onError } = useContext(ErrorContext);

    const {
        formData,
        setFormData,
        updateFormData,
    } = useFormData<IPatchUser>(userState);

    const [formActiveData, setFormActiveData] = useState<ProfileFormActiveData>(
        getIsUnset<IPatchUser>({ ...userState, password: "set" }));

    const [profileImage, setProfileImage] = useState<File>(null);
    useEffect(() => {
        if (profileImage) {
            resizeImage({ file: profileImage, maxSize: 64 })
                .then((dataUrl) => {
                    setFormData((p) => ({
                        ...p,
                        Avatar: { name: profileImage.name, image: dataUrl }
                    }));
                });
        } else {
            setFormData((p) => ({ ...p, Avatar: null }));
        }
    }, [profileImage, setFormData]);

    const { validated, setValidated, handleSubmit } = useFormValidateOnSubmit(
        async () => {
            const editedData = extract<Partial<IPatchUser>>(
                formActiveData)(formData);
            await editUser({
                body: editedData,
                id: userState.id,
            });
            setUserState((p) => ({ ...p, ...editedData }));
            close();
            return true;
        }, onError);

    useEffect(
        () => {
            setFormActiveData(getIsUnset<IPatchUser>(
                { ...userState, password: "set" }));
            setValidated(false);
        },
        [userState]);

    const inputRefs = useFocusEditable<IPatchUser>(formActiveData);

    const activeData: IPatchUser = Object.entries(formActiveData).reduce(
        (p, [k, v]: [keyof IPatchUser, boolean]) => ({
            ...p, [k]: v ? formData[k] : (
                k in userState ? userState[k as keyof AuthUserProps] : false)
        }),
        {}) as IPatchUser;

    // TODO: factor out components
    return (<>
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
            <Form.Group
                as={Form.Row}
                controlId="formProfileFirstName"
                className="align-items-stretch"
            >
                <Form.Label column md={4}>First Name</Form.Label>
                <Col>
                    <InputGroup hasValidation>
                        <NameInput
                            reference={inputRefs.firstName}
                            disabled={!formActiveData.firstName}
                            formState={activeData}
                            updateFormState={updateFormData}
                            name="firstName" />
                        <Form.Control.Feedback type="invalid" tooltip>
                            Please enter your new given name (letters only).
                        </Form.Control.Feedback>
                        <InputGroup.Append>
                            <IconButton
                                variant={formActiveData.firstName
                                    ? "outline-danger"
                                    : "outline-warning"}
                                onClick={() => setFormActiveData((p) =>
                                    ({ ...p, firstName: !p.firstName }))}
                                iconName={formActiveData.firstName
                                    ? "icono-cross"
                                    : "icono-rename"} />
                        </InputGroup.Append>
                    </InputGroup>
                </Col>
            </Form.Group>
            <Form.Group as={Form.Row} controlId="formProfileLastName">
                <Form.Label column md={4}>
                    Last Name
                </Form.Label>
                <Col>
                    <InputGroup hasValidation>
                        <NameInput
                            reference={inputRefs.lastName}
                            disabled={!formActiveData.lastName}
                            formState={activeData}
                            updateFormState={updateFormData}
                            name="lastName" />
                        <Form.Control.Feedback type="invalid" tooltip>
                            Please enter your new family name (letters only).
                        </Form.Control.Feedback>
                        <InputGroup.Append>
                            <IconButton
                                variant={formActiveData.lastName
                                    ? "outline-danger"
                                    : "outline-warning"}
                                onClick={() => setFormActiveData((p) =>
                                    ({ ...p, lastName: !p.lastName }))}
                                iconName={formActiveData.lastName
                                    ? "icono-cross"
                                    : "icono-rename"} />
                        </InputGroup.Append>
                    </InputGroup>
                </Col>
            </Form.Group>
            <Form.Group as={Form.Row} controlId="formProfileBio">
                <Form.Label column md={4}>
                    About You
                </Form.Label>
                <Col>
                    <InputGroup hasValidation>
                        <Form.Control
                            ref={inputRefs.bio}
                            disabled={!formActiveData.bio}
                            as="textarea"
                            rows={4}
                            value={activeData.bio}
                            onChange={updateFormData}
                            name="bio"
                            maxLength={UserConstants.BIO_MAX_LENGTH}
                            placeholder={`Tell others about yourself:\n`
                                + ` - Describe your play style`
                                + ` and preferences.\n`
                                + ` - 👍 Include emoji, if you like 🤷‍♀️\n`
                                + ` - Enjoy playing 🎾!`} />
                        <Form.Control.Feedback type="invalid" tooltip>
                            Please enter your new family name (letters only).
                        </Form.Control.Feedback>
                        <InputGroup.Append>
                            <IconButton
                                variant={formActiveData.bio
                                    ? "outline-danger"
                                    : "outline-warning"}
                                onClick={() => setFormActiveData((p) =>
                                    ({ ...p, bio: !p.bio })
                                )}
                                iconName={formActiveData.bio
                                    ? "icono-cross"
                                    : "icono-rename"} />
                        </InputGroup.Append>
                    </InputGroup>
                </Col>
            </Form.Group>
            <Form.Group as={Form.Row} controlId="formProfileSkill">
                <Form.Label column md={4}>
                    Skill
                </Form.Label>
                <Col>
                    <InputGroup hasValidation>
                        <SkillSelector
                            reference={inputRefs.skill}
                            disabled={!formActiveData.skill}
                            name={"skill"}
                            value={activeData.skill}
                            setValue={(v) => setFormData((p) => ({
                                ...p,
                                skill: v
                            }))}
                        />
                        <Form.Control.Feedback type="invalid" tooltip>
                            Invalid skill: must be between 1.0 and 6.0.
                        </Form.Control.Feedback>
                        <InputGroup.Append>
                            <IconButton
                                variant={formActiveData.skill
                                    ? "outline-danger"
                                    : "outline-warning"}
                                onClick={() => setFormActiveData((p) =>
                                    ({ ...p, skill: !p.skill })
                                )}
                                iconName={formActiveData.skill
                                    ? "icono-cross"
                                    : "icono-rename"} />
                        </InputGroup.Append>
                    </InputGroup>
                </Col>
            </Form.Group>
            <Form.Group as={Form.Row} controlId="formPictureUpload">
                <Form.Label column md={4}>Profile Picture</Form.Label>
                <Col>
                    <InputGroup hasValidation>
                        <Form.File
                            disabled={!formActiveData.Avatar}
                            data-browse="Find Photo"
                            label={activeData.Avatar?.name || "None"}
                            onChange={(e: FormChangeEvent) =>
                                setProfileImage(e.target?.files?.[0])}
                            custom />
                        <InputGroup.Append>
                            <IconButton
                                variant={formActiveData.Avatar
                                    ? "outline-danger"
                                    : "outline-warning"}
                                onClick={() => setFormActiveData((p) =>
                                    ({ ...p, Avatar: !p.Avatar }))}
                                iconName={formActiveData.Avatar
                                    ? "icono-cross"
                                    : "icono-rename"} />
                        </InputGroup.Append>
                    </InputGroup>
                </Col>
            </Form.Group>
            {Object.values(formActiveData).reduce((p, v) => p || v, false)
                ? <Form.Row>
                    <Form.Label column md={4}>Preview</Form.Label>
                    <Col>
                        <ProfileCard {...activeData} />
                    </Col>
                </Form.Row>
                : null}
            <br />
            <Form.Row className="justify-content-between">
                <Col xs="auto">
                    <Button variant="secondary" onClick={close}>Cancel</Button>
                </Col>
                <Col xs="auto">
                    <Button variant="primary" type="submit">Submit</Button>
                </Col>
            </Form.Row>
        </Form>
    </>);
}

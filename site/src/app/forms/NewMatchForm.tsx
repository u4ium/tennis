import React, { useContext, useEffect, useRef, useState } from "react";
import { AsyncTypeahead, TypeaheadModel } from "react-bootstrap-typeahead";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import {
    IDoublesMatch,
    IMatch,
    ISinglesMatch
} from "../../common/model/match/MatchModel";
import { IPlayerUser } from "../../common/model/user/PlayerUser";
import LeagueSelector from "../components/LeagueSelector";
import RuleSelector from "../components/RuleSelector";
import { ErrorContext } from "../contexts/ErrorContext";
import { LeaguesContext } from "../contexts/LeaguesContext";
import { MatchesContext } from "../contexts/MatchesContext";
import { RulesContext } from "../contexts/RulesContext";
import { UserContext } from "../contexts/UserContext";
import useFormData from "../effects/useFormData";
import useFormValidateOnSubmit from "../effects/useFormValidateOnSubmit";
import { IPlayer } from "../models/User";
import { addDoublesMatch, addSinglesMatch } from "../requests/match";
import { search } from "../requests/player";
import { isMatchFinishedBefore, ScoresSubForm } from "./ScoresSubForm";

interface IAsyncTypeahead {
    getInput: () => HTMLInputElement;
}

type ATypeahead<T extends TypeaheadModel> = AsyncTypeahead<T> & IAsyncTypeahead;

interface NewMatchFormData {
    type: string;
    rule: string;
    league: string;
    teammate: [IPlayer] | [];
    opponents: IPlayer[];
}

export function NewMatchForm({ close }: { close: VoidFunction; }) {
    const { onError } = useContext(ErrorContext);
    const { userState } = useContext(UserContext);
    const { leaguesState } = useContext(LeaguesContext);
    const { rulesState } = useContext(RulesContext);
    const { setMatchesState } = useContext(MatchesContext);

    const {
        formData,
        setFormData,
        updateFormData
    } = useFormData<NewMatchFormData>({
        type: "Singles",
        rule: rulesState?.[0]?.name,
        league: "",
        teammate: [],
        opponents: [],
    });
    const [players, setPlayers] = useState<IPlayerUser[]>([]);
    const [isLoadingOpponents, setIsLoadingOpponents] = useState(false);
    const [isLoadingTeammates, setIsLoadingTeammates] = useState(false);

    const activeLeague = formData.league && leaguesState.find(
        (league) => {
            const [id, isDbls] = formData.league.split(" ").map(Number);
            return id === league.id && !!isDbls === league.isDoubles;
        });

    const isDoubles = activeLeague ? activeLeague.isDoubles
        : formData.type === "Doubles";

    const activeRule = formData.league
        ? activeLeague?.Rule
        : rulesState.find((rule) => rule.name === formData.rule);

    const [scores, setScores] = useState<Array<[number, number]>>([[NaN, NaN]]);
    useEffect(() => {
        setScores((previousScores) => [
            ...previousScores,
            ...(Array.from(Array(activeRule.setsToWin * 2 - 1),
                () => [NaN, NaN]) as Array<[number, number]>)
        ].slice(0, activeRule.setsToWin * 2 - 1));
    }, [formData.rule, formData.league]);

    const setScore = (index: number, which: 0 | 1, score: number) => {
        setScores((previousScores) => {
            previousScores[index][which] = score;
            return [...previousScores];
        });
    };

    const updateMatches = (match: IMatch) =>
        setMatchesState((p) => {
            match.played = new Date();

            if (match.season) {
                const leagueMatches = p.leagueMatches;
                const league = leagueMatches.find(
                    (l) => l.id === activeLeague.id &&
                        l.isDoubles === activeLeague.isDoubles);
                if (!league) {
                    leagueMatches.push({
                        id: activeLeague.id,
                        isDoubles: activeLeague.isDoubles,
                        Seasons: [{
                            id: match.season,
                            matches: [match]
                        }]
                    });
                } else {
                    const season = league.Seasons.find(
                        (s) => s.id === match.season
                    );
                    if (season) {
                        season.matches.push(match);
                    } else {
                        league.Seasons.push({
                            id: match.season,
                            matches: [match]
                        });
                    }
                }
            } else if (isDoubles) {
                p.doublesMatches.push(match as IDoublesMatch);
            } else {
                p.singlesMatches.push(match as ISinglesMatch);
            }

            return { ...p };
        });

    const {
        validated,
        handleSubmit
    } = useFormValidateOnSubmit(
        async () => {
            const sets = scores.filter((v, i) =>
                !isMatchFinishedBefore(i, // TODO: run only once
                    scores, activeRule.setsToWin, activeRule.setWinScore)
            ).map((s, i) => ({
                position: i,
                seedScore: s[0],
                challengerScore: s[1]
            }));
            const match = {
                rule: activeRule?.name,
                season: activeLeague?.Seasons?.[0]?.id,
                sets
            };
            if (isDoubles) {
                const doublesMatch = {
                    ...match,
                    seed: {
                        player1: userState.id,
                        player2: formData.teammate?.[0]?.id
                    },
                    challenger: {
                        player1: formData.opponents?.[0]?.id,
                        player2: formData.opponents?.[1]?.id
                    },
                };
                await addDoublesMatch({ body: doublesMatch });
                updateMatches(doublesMatch);
            } else {
                const singlesMatch = {
                    ...match,
                    seed: { player: userState.id },
                    challenger: { player: formData.opponents?.[0].id },
                };
                await addSinglesMatch({ body: singlesMatch });
                updateMatches(singlesMatch);
            }
            close();
            return false;
        },
        onError
    );

    const opponentInputRef = useRef<ATypeahead<IPlayer>>(null);
    const teammateInputRef = useRef<ATypeahead<IPlayer>>(null);
    const invalidOpponents = isDoubles
        ? formData.opponents.length !== 2
        : formData.opponents.length !== 1;
    const invalidTeammate = formData.teammate.length !== 1 ||
        formData.teammate[0]?.id === formData.opponents[0]?.id ||
        formData.teammate[0]?.id === formData.opponents[1]?.id;
    useEffect(() => {
        if (validated) {
            const opponentTypeahead = opponentInputRef?.current?.getInput();
            if (opponentTypeahead) {
                opponentTypeahead.setCustomValidity(
                    invalidOpponents ? "invalid" : ""
                );
            }
            if (isDoubles) {
                const teammateTypeahead = teammateInputRef?.current?.getInput();
                if (teammateTypeahead) {
                    teammateTypeahead.setCustomValidity(
                        invalidTeammate ? "invalid" : ""
                    );
                }
            }
        }
    }, [
        invalidOpponents,
        invalidTeammate,
        validated,
        opponentInputRef,
        teammateInputRef
    ]);

    return (
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
            <Form.Group as={Form.Row} controlId="formLeagueAndRule">
                <Form.Label column md={4}>League and Rule</Form.Label>
                <Col>
                    <InputGroup>
                        <LeagueSelector
                            league={formData.league}
                            updateFormData={updateFormData} />
                        <RuleSelector
                            disabled={formData.league && !!activeLeague?.Rule}
                            rule={activeRule?.name}
                            updateFormData={updateFormData} />
                    </InputGroup>
                </Col>
            </Form.Group>

            <Form.Group
                as={Form.Row}
                className="align-items-stretch justify-content-between"
            >
                <Form.Label column md={4}>
                    Opponent{isDoubles ? "s" : ""}
                </Form.Label>
                <Col>
                    <InputGroup hasValidation>
                        <AsyncTypeahead
                            id="formOpponentSelect"
                            ref={opponentInputRef}
                            isLoading={isLoadingOpponents}
                            className={validated
                                ? (invalidOpponents ? "is-invalid" : "is-valid")
                                : ""}
                            isValid={validated ? !invalidOpponents : undefined}
                            isInvalid={validated ? invalidOpponents : undefined}
                            placeholder={"Select Opponent" +
                                (isDoubles ? "s" : "")}
                            labelKey={(option) =>
                                `${option.firstName} ${option.lastName}`}
                            options={players}
                            selected={isDoubles
                                ? formData.opponents
                                : formData.opponents.slice(0, 1)}
                            onChange={(selected) => setFormData((prev) =>
                                ({ ...prev, opponents: selected.slice(0, 2) })
                            )}
                            onSearch={(name) => {
                                setIsLoadingOpponents(true);
                                search({ name }).then((results) => {
                                    setPlayers(results);
                                    setIsLoadingOpponents(false);
                                });
                            }}
                            minLength={2}
                            open={formData.opponents.length < (
                                Number(isDoubles) + 1) && undefined}
                            highlightOnlyResult
                            clearButton
                            multiple={isDoubles}
                            flip />
                        <Form.Control.Feedback type="invalid" tooltip>
                            <small>
                                {isDoubles
                                    ? "Must select two opponents"
                                    : "Opponent not selected"}
                            </small>
                        </Form.Control.Feedback>
                        <InputGroup.Append>
                            <Form.Control
                                required
                                className="d-flex align-self-center h-100"
                                id="formIsDoubles"
                                as="select"
                                name="type"
                                disabled={!!activeLeague}
                                onChange={updateFormData}
                                value={isDoubles ? "Doubles" : "Singles"}
                            >
                                <option>Singles</option>
                                <option>Doubles</option>
                            </Form.Control>
                            <Form.Control.Feedback type="invalid" tooltip>
                                <small>
                                    {isDoubles
                                        ? "Must select two opponents"
                                        : "Opponent not selected"}
                                </small>
                            </Form.Control.Feedback>
                        </InputGroup.Append>
                    </InputGroup>
                </Col>
            </Form.Group>

            {isDoubles
                ? <Form.Group
                    as={Form.Row}
                    className="align-items-stretch justify-content-between"
                >
                    <Form.Label column md={4}>Teammate</Form.Label>
                    <Col>
                        <AsyncTypeahead
                            id="formTeammateSelect"
                            className={validated
                                ? (invalidTeammate ? "is-invalid" : "is-valid")
                                : ""}
                            ref={teammateInputRef}
                            isLoading={isLoadingTeammates}
                            placeholder={"Select Teammate"}
                            labelKey={(option) =>
                                `${option.firstName} ${option.lastName}`}
                            options={players}
                            selected={formData.teammate}
                            onChange={(selected) => setFormData((prev) =>
                                ({ ...prev, teammate: (selected as [IPlayer]) })
                            )}
                            onSearch={(name) => {
                                setIsLoadingTeammates(true);
                                search({ name }).then((results) => {
                                    setPlayers(results);
                                    setIsLoadingTeammates(false);
                                });
                            }}
                            minLength={2}
                            highlightOnlyResult
                            clearButton
                            flip />
                        <Form.Control.Feedback type="invalid" tooltip>
                            <small>
                                Must select teammate from list,
                                cannot be one of the opponents.
                            </small>
                        </Form.Control.Feedback>
                    </Col>
                </Form.Group>
                : null}

            <Form.Group as={Form.Row}>
                <Form.Label column md={4}>Score</Form.Label>
                <Col>
                    <ScoresSubForm
                        scores={scores}
                        setScore={setScore}
                        setsToWin={activeRule.setsToWin}
                        setWinScore={activeRule.setWinScore}
                        isDoubles={isDoubles} />
                </Col>
            </Form.Group>

            <br />
            <Form.Row className="justify-content-between">
                <Col xs="auto">
                    <Button variant="secondary" onClick={close}>Cancel</Button>
                </Col>
                <Col xs="auto">
                    <Button variant="primary" type="submit">Submit</Button>
                </Col>
            </Form.Row>
        </Form>
    );
}

import React, { useContext, useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import { ISchedule, IScheduleSection } from "../../common/model/schedule/Schedule";
import { AvailabiltySelector } from "../components/AvailabiltySelector";
import { ErrorContext } from "../contexts/ErrorContext";
import { UserContext } from "../contexts/UserContext";
import useFormValidateOnSubmit from "../effects/useFormValidateOnSubmit";
import { replaceSchedule } from "../requests/schedule";

type IGranularity = 0 | 1 | 2;
enum Granularity {
    Weekly = 0,
    Workweek = 1,
    Daily = 2,
}

const emptySchedule: ISchedule = {
    sections: [
        [/*{ start: "16:00:00", end: "21:00:00" }*/],
        [/*{ start: "16:00:00", end: "21:00:00" }*/],
        [/*{ start: "16:00:00", end: "21:00:00" }*/],
        [/*{ start: "16:00:00", end: "21:00:00" }*/],
        [/*{ start: "16:00:00", end: "21:00:00" }*/],
        [/*{ start: "12:00:00", end: "17:00:00" }*/],
        [/*{ start: "12:00:00", end: "17:00:00" }*/],
    ]
};

function isEqualDays(d1: IScheduleSection[], d2: IScheduleSection[]): boolean {
    return d1.reduce(
        (p, s, i) => p && s.start === d2[i]?.start && s.end === d2[i]?.end,
        true
    ) && d2.reduce(
        (p, s, i) => p && s.start === d1[i]?.start && s.end === d1[i]?.end,
        true
    );
}

function getGranularity(schedule: ISchedule): IGranularity {
    const monday = schedule.sections[0];
    const saturday = schedule.sections[5];
    const sunday = schedule.sections[6];
    const isWorkweek = schedule.sections.slice(1, 5).reduce(
        (p, s) => p && isEqualDays(s, monday), true) &&
        isEqualDays(saturday, sunday);
    const isWeekly = isWorkweek && isEqualDays(monday, saturday);
    return isWeekly ? Granularity.Weekly :
        (isWorkweek ? Granularity.Workweek : Granularity.Daily);
}

function toGranularity(value: string): IGranularity {
    const n = Number(value);
    return ((isNaN(n) ? 2 : (n % 3)) as IGranularity);
}

// Use for CLUB, too (refactor)
export function ScheduleSelectorForm() {
    const { userState, setUserState } = useContext(UserContext);
    const { onError } = useContext(ErrorContext);

    // TODO: compute
    const [
        schedule,
        setSchedule
    ] = useState<ISchedule>(userState.schedule || emptySchedule);
    //  || clubState.schedule;
    const [
        fieldState,
        setFieldState
    ] = useState<IGranularity>(getGranularity(schedule));

    const { validated, handleSubmit } = useFormValidateOnSubmit(
        async () => {
            await replaceSchedule({
                id: userState.id,
                body: schedule,
            });
            setUserState((p) => ({ ...p, schedule }));
            return true;
        }, onError);

    const setScheduleByDays = (day: IScheduleSection[], ds: number[]) => {
        setSchedule((previousSchedule) => {
            for (const d of ds) {
                previousSchedule.sections[d] = day.map(
                    (s) => ({ start: s.start, end: s.end }));
            }
            return { ...previousSchedule };
        });
    };

    useEffect(() => setFieldState(getGranularity(schedule)),
        [userState.schedule]);

    useEffect(() => {
        switch (fieldState) {
            case Granularity.Weekly:
                setScheduleByDays(schedule.sections[0], [1, 2, 3, 4, 5, 6]);
                break;
            case Granularity.Workweek:
                setScheduleByDays(schedule.sections[0], [1, 2, 3, 4]);
                setScheduleByDays(schedule.sections[5], [6]);
                break;
            case Granularity.Daily: break;
            default: break;
        }
    }, [fieldState]);

    return (
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
            <Form.Group as={Form.Row} className="justify-content-between">
                <Col xs={12} md={4}>
                    <Form.Label>
                        Set schedule by
                    </Form.Label>
                </Col>
                <Col xs={2} className="d-md-none" />
                <Col xs={10} md={8}>
                    <Form.Row className="justify-content-md-end">
                        <div className="inline-radio col-12 col-sm-auto">
                            <Form.Check
                                inline
                                checked={fieldState === Granularity.Weekly}
                                id="formScheduleGranularity0"
                                name="formScheduleGranularity"
                                value={Granularity.Weekly}
                                type="radio"
                                label="Weekly"
                                onChange={(e) => setFieldState(
                                    toGranularity(e.target.value))}
                            />
                        </div>
                        <div className="inline-radio col-12 col-sm-auto">
                            <Form.Check
                                inline
                                checked={fieldState === Granularity.Workweek}
                                id="formScheduleGranularity1"
                                name="formScheduleGranularity"
                                value={Granularity.Workweek}
                                type="radio"
                                label="Weekdays/Weekends"
                                onChange={(e) => setFieldState(
                                    toGranularity(e.target.value))}
                            />
                        </div>
                        <div className="inline-radio col-12 col-sm-auto">
                            <Form.Check
                                inline
                                checked={fieldState === Granularity.Daily}
                                id="formScheduleGranularity2"
                                name="formScheduleGranularity"
                                value={Granularity.Daily}
                                type="radio"
                                label="Daily"
                                onChange={(e) => setFieldState(
                                    toGranularity(e.target.value)
                                )}
                                className="mr-0" />
                        </div>
                    </Form.Row>
                    <Form.Row className="justify-content-md-end">
                        <Form.Text>
                            {[
                                "Your availabilty is consitent Monday-Sunday.",
                                "Your availabilty is consitent Monday-Friday, "
                                + " but different on the weekend.",
                                "Your availabilty is different each day of the"
                                + " week."
                            ][fieldState]}
                        </Form.Text>
                    </Form.Row>
                </Col>
            </Form.Group>
            {[
                <>
                    <AvailabiltySelector
                        type="All Week"
                        values={schedule?.sections?.[0]}
                        setValues={(day) =>
                            setScheduleByDays(day, [0, 1, 2, 3, 4, 5, 6])}
                    />
                </>,
                <>
                    <AvailabiltySelector
                        type="Weekdays"
                        values={schedule?.sections?.[0]}
                        setValues={(day) =>
                            setScheduleByDays(day, [0, 1, 2, 3, 4])}
                    />
                    <AvailabiltySelector
                        type="Weekends"
                        values={schedule?.sections?.[5]}
                        setValues={(day) => setScheduleByDays(day, [5, 6])}
                    />
                </>,
                <>
                    <AvailabiltySelector
                        type="Monday"
                        values={schedule?.sections?.[0]}
                        setValues={(vs) => setScheduleByDays(vs, [0])} />
                    <AvailabiltySelector
                        type="Tuesday"
                        values={schedule?.sections?.[1]}
                        setValues={(vs) => setScheduleByDays(vs, [1])} />
                    <AvailabiltySelector
                        type="Wednesday"
                        values={schedule?.sections?.[2]}
                        setValues={(vs) => setScheduleByDays(vs, [2])} />
                    <AvailabiltySelector
                        type="Thursday"
                        values={schedule?.sections?.[3]}
                        setValues={(vs) => setScheduleByDays(vs, [3])} />
                    <AvailabiltySelector
                        type="Friday"
                        values={schedule?.sections?.[4]}
                        setValues={(vs) => setScheduleByDays(vs, [4])} />
                    <AvailabiltySelector
                        type="Saturday"
                        values={schedule?.sections?.[5]}
                        setValues={(vs) => setScheduleByDays(vs, [5])} />
                    <AvailabiltySelector
                        type="Sunday"
                        values={schedule?.sections?.[6]}
                        setValues={(vs) => setScheduleByDays(vs, [6])} />
                </>
            ][fieldState]}
            <br />
            <Form.Row className="justify-content-between">
                <Col xs="auto">
                    <Button variant="secondary">Cancel</Button>
                </Col>
                <Col xs="auto">
                    <Button variant="primary" type="submit">Submit</Button>
                </Col>
            </Form.Row>
        </Form>
    );
}

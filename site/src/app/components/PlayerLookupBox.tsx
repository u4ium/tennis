import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import { toTitleCase } from "../utils";

export default function PlayerLookupBox({
    name,
    player,
    updateForm,
}: {
    name: string,
    player: string,
    updateForm: (event: React.ChangeEvent) => void,
}) {
    const [search, setSearch] = useState("");
    const [players, setPlayers] = useState([
        {
            id: 1,
            firstName: "Rafael",
            lastName: "Nadal",
        },
        {
            id: 2,
            firstName: "Novak",
            lastName: "Djokovic",
        },
        {
            id: 3,
            firstName: "Rogel",
            lastName: "Federer",
        },
    ]);

    const updateList = () => {
        setSearch((p) => p);
        setPlayers((ps) => ps.filter((p) => p?.firstName.match(search)));
        // fetch(`/api/search/user/${search}`)
        //      .then(response => response.status === 200 ?
        //      response.json().then(players => this.setState({ players: players })) :
        //      this.setState({ players: [] }))
        //      .catch(err =>  this.props.onError(new Error("Invalid Search")));
    };

    return (
        <>
            <Form.Control
                required
                name={name}
                list={name + "List"}
                type="search"
                value={player}
                placeholder={toTitleCase(name)}
                onChange={(e) => {
                    updateForm(e);
                    updateList();
                }}
            />
            <datalist id={name + "List"}>
                {players.map((p) => (
                    <option
                        key={p.id}
                    // value={p.id}
                    >{`${p.firstName} ${p.lastName}`}</option>
                ))}
            </datalist>
        </>
    );
}

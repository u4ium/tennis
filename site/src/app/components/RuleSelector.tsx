import React, { useContext } from "react";
import Form from "react-bootstrap/Form";
import IRule from "../../common/model/rule/RuleModel";
import { RulesContext } from "../contexts/RulesContext";

export default function RuleSelector(
    { rule, updateFormData, disabled = false }: {
        rule: string,
        updateFormData: (event: React.ChangeEvent<HTMLInputElement>) => void,
        disabled?: boolean
    }) {
    const { rulesState } = useContext(RulesContext);
    return (
        <Form.Control
            as="select"
            name="rule"
            value={rule}
            onChange={updateFormData}
            disabled={disabled}
            className="text-truncate"
        >
            {rulesState.map((r: IRule, index: number) =>
                <option key={index}>{r.name}</option>)}
        </Form.Control>
    );
}

import React, { useContext } from "react";
import Form from "react-bootstrap/Form";
import ILeague from "../../common/model/league/LeagueModel";
import { LeaguesContext } from "../contexts/LeaguesContext";
import { UserContext } from "../contexts/UserContext";

export default function LeagueSelector({ league, updateFormData }: {
    league: string,
    updateFormData: (event: React.ChangeEvent<HTMLInputElement>) => void,
}) {
    const { leaguesState } = useContext(LeaguesContext);
    const { userState } = useContext(UserContext);
    const myLeagues = leaguesState.filter(
        (l) => !!l?.Seasons?.find(
            (s) => !!s?.participants?.find(
                (p) => p.id === userState.id)));

    const today = new Date();
    const myActiveLeagues = myLeagues.filter(
        (l) => !!l.Seasons?.find((s) => new Date(s.start) < today));
    return (
        <Form.Control
            as="select"
            name="league"
            value={league}
            onChange={updateFormData}
            className="text-truncate"
        >
            <option value="">
                Free Play (No League)
            </option>
            {myActiveLeagues.map((lg: ILeague, index: number) =>
                <option
                    key={index}
                    value={`${lg.id} ${Number(lg.isDoubles)}`}
                >
                    {lg.name} ({lg.isDoubles ? "Doubles" : "Singles"})
                </option>)}
        </Form.Control>
    );
}

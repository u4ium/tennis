import React from "react";
import Form from "react-bootstrap/Form";
import { FormElement } from "../forms/types/FormElements";

export function SkillSelector({
    name,
    value,
    setValue,
    reference,
    disabled,
    min = 0,
    max = 10
}: {
    name: string;
    value: number;
    setValue: (value: number) => void;
    reference?: React.MutableRefObject<FormElement>;
    disabled?: boolean;
    min?: number;
    max?: number;
}) {
    return (
        <Form.Control
            required
            ref={reference}
            as="select"
            name={name}
            disabled={disabled}
            value={value === null ? "" : value}
            onChange={(e) => setValue(Number(e.target.value))}
        >
            <option disabled hidden value={""}>
                Estimate Your NTRP Rating
            </option>
            {Array.from(new Array(max - min + 1)).map((u, i) =>
                <option
                    key={i}
                    value={min + i}
                >
                    {(1 + (min + i) / 2).toFixed(1)}
                </option>
            )}
        </Form.Control>
    );
}

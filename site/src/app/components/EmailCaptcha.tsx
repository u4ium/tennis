import React, { Component } from "react";
import { ClubProps } from "../props";

/* interface IEmailCaptchaState {
    value?: boolean;
    email: string;
} */

class EmailCaptcha extends Component<ClubProps> {
    public render = () => (
        <form>
            <a href={`mailto:${this.props.owner}`}>{this.props.owner}</a>
        </form>
    )
}

export default EmailCaptcha;

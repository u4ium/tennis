import React from "react";
import Form from "react-bootstrap/Form";

export function ScoreInputBox({
    score,
    other,
    winScore,
    placeholder,
    setScore
}: {
    score: number;
    other: number;
    winScore: number;
    placeholder: string;
    setScore: (value: number) => void;
}) {
    return <Form.Control
        required
        className="text-truncate"
        type="number"
        value={isNaN(score) ? "" : score}
        placeholder={placeholder}
        onChange={(e) => setScore(Number(e.target.value))}
        min={isNaN(other) || other >= winScore ? 0
            : (other === winScore - 1 ? winScore + 1 : winScore)}
        max={(isNaN(other) || other === winScore - 1 || other === winScore)
            ? winScore + 1 : winScore} />;
}

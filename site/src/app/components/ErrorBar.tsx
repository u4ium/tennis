import React from "react";
import Alert from "react-bootstrap/Alert";

export default function ErrorBar({ errors }: { errors: Error[] }) {
    return <div className="w-100">
        {errors.map((error: Error, index: number) =>
            <Alert variant="danger" key={index}>
                {error.message}
            </Alert>
        )}
    </div>;
}

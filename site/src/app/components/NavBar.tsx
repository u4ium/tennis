import React, { useContext, useEffect, useState } from "react";
import Dropdown from "react-bootstrap/Dropdown";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavItem from "react-bootstrap/NavItem";
import { NavLink, useLocation } from "react-router-dom";
import { ErrorContext } from "../contexts/ErrorContext";
import { UserContext } from "../contexts/UserContext";
import { allViews, isUnauthorizedView, } from "../props";
import { logout } from "../requests/user";
import { toTitleCase } from "../utils";
import AvatarImage from "./AvatarImage";

const breakpoint = "md";

interface INavProps {
    setShowLogin: React.Dispatch<React.SetStateAction<boolean>>;
}

export default function NavBar({ setShowLogin }: INavProps) {
    const location = useLocation();
    const [isExpanded, setExpanded] = useState(false);
    const { userState, setUserState } = useContext(UserContext);
    const { onError } = useContext(ErrorContext);

    const [isNavBarHidden, setIsNavBarHidden] = useState(false);
    const [scrollPos, setScrollPos] = useState(0);
    const onScroll = () => {
        const newPos = window?.pageYOffset;
        setIsNavBarHidden(newPos > scrollPos);
        setScrollPos(newPos);
    };

    useEffect(() => {
        window?.addEventListener("scroll", onScroll);
        return () => window?.removeEventListener("scroll", onScroll);
    }, [scrollPos, onScroll, setScrollPos, setIsNavBarHidden]);

    const title = toTitleCase(location?.pathname?.split("/")?.[1]);

    return (
        <Navbar
            collapseOnSelect
            expand={breakpoint}
            sticky="top"
            bg="white"
            onToggle={setExpanded}
            className={!isExpanded && isNavBarHidden
                ? "top-selector-hide"
                : "top-selector"}
        >
            <Navbar.Toggle aria-controls="navbar-nav">
                <div
                    className={isExpanded ?
                        "icono-cross" :
                        "icono-hamburger"} />
            </Navbar.Toggle>
            <Navbar.Brand className={`d-${breakpoint}-none ml-3`}>
                {title}
            </Navbar.Brand>

            <Dropdown
                as={NavItem}
                id="account-nav"
                className={`order-${breakpoint}-last`}
            >
                <Dropdown.Toggle
                    as={Nav.Link}
                    className="pr-2 pl-2"
                    aria-label="Profile Menu Dropdown Button"
                    aria-controls="profile-menu"
                >
                    <AvatarImage avatar={userState?.Avatar} size={32} />
                </Dropdown.Toggle>
                <Dropdown.Menu id="profile-menu" align="right">
                    {userState?.id ?
                        <>
                            <Dropdown.Item>Settings</Dropdown.Item>
                            <Dropdown.Item>Profile</Dropdown.Item>
                            <Dropdown.Divider />
                            <Dropdown.Item onClick={async () => {
                                try {
                                    setUserState(await logout({}));
                                } catch (err) {
                                    onError(err);
                                }
                            }}>
                                Log Out
                            </Dropdown.Item>
                        </> :
                        <Dropdown.Item onClick={() => setShowLogin(true)}>
                            Log In
                        </Dropdown.Item>
                    }
                </Dropdown.Menu>
            </Dropdown>
            <Navbar.Collapse id="navbar-nav">
                <Nav
                    variant={isExpanded ? "pills" : "tabs"}
                    className="w-100"
                    justify
                    fill
                    defaultActiveKey="/about"
                >
                    {allViews
                        .filter((view: string) =>
                            userState?.id || isUnauthorizedView(view))
                        .map((view: string, index: number) =>
                            <Tab key={index} name={view} />
                        )}
                </Nav>
            </Navbar.Collapse>
        </Navbar >
    );
}

function Tab({ name }: { name: string }) {
    return (
        <NavItem>
            <Nav.Link eventKey={name} as={NavLink} to={`/${name}`}>
                {toTitleCase(name)}
            </Nav.Link>
        </NavItem>
    );
}

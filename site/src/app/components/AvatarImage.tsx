import React from "react";
import { PersonCircle } from "react-bootstrap-icons";
import Image from "react-bootstrap/Image";
import { IAvatar, INewAvatar } from "../../common/model/avatar/Avatar";

export default function AvatarImage({ size, avatar }:
    { avatar?: IAvatar | INewAvatar, size?: number }
) {
    return (
        avatar
            ? <Image
                src={avatar.image}
                alt={avatar.name}
                width={size || 64}
                height={size || 64}
                roundedCircle
            />
            : <PersonCircle size={size} />
    );
}

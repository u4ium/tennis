import React from "react";
import Button, { ButtonProps } from "react-bootstrap/Button";

interface IconButtonProps extends ButtonProps {
    iconName: string;
}

export default function IconButton({ iconName, ...props }: IconButtonProps) {
    return <Button {...props} className={`p-0 ${props.className}`} >
        <span className={iconName} />
    </Button>;
}

import React, { useContext, useEffect } from "react";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Popover from "react-bootstrap/Popover";
import Row from "react-bootstrap/Row";
import {
    IDoublesPlayer,
    IMatch,
    IMatchPlayer,
    INamed,
    ISet,
    ISinglesPlayer
} from "../../common/model/match/MatchModel";
import { AvatarsContext } from "../contexts/AvatarsContext";
import { UserContext } from "../contexts/UserContext";
import { toShortName, toShortNames } from "../utils";
import AvatarImage from "./AvatarImage";

export function PlayedMatch({ match, isDoubles }:
    { match: IMatch; isDoubles: boolean; }
) {
    return (
        <Card>
            <Card.Body>
                <ScoreRow
                    seed={match.seed}
                    challenger={match.challenger}
                    sets={match.sets}
                    isDoubles={isDoubles}
                />
            </Card.Body>
        </Card>
    );
}

function ScoreRow({ seed, challenger, sets, isDoubles }:
    {
        seed: IMatchPlayer,
        challenger: IMatchPlayer,
        sets: ISet[],
        isDoubles: boolean,
    }
) {
    const extraSpace = 5 - sets.length;
    return (<Row className="align-items-stretch justify-content-end">
        <Col xs={2 + extraSpace} sm={4 + extraSpace} className="mr-auto">
            <MiniPlayerCard player={seed} isDoubles={isDoubles} />
            <MiniPlayerCard player={challenger} isDoubles={isDoubles} />
        </Col>
        {sets.map((s, i) =>
            <ScoreColumn
                seedScore={s.seedScore}
                challengerScore={s.challengerScore}
                key={i}
            />
        )}
    </Row>);
}

function ScoreColumn({ seedScore, challengerScore }:
    { seedScore: number, challengerScore: number }
) {
    const seedWin = seedScore > challengerScore;
    const className = "flex-grow-1 text-light pl-1 pr-1 mr-0";

    return (<Col xs="auto" className="align-self-center">
        <Row className={"rounded-top " + className + (seedWin
            ? " bg-success"
            : " bg-secondary")}>
            {seedScore}
        </Row>
        <Row className={"rounded-bottom " + className + (seedWin
            ? " bg-secondary"
            : " bg-success")}>
            {challengerScore}
        </Row>
    </Col>);
}

function MiniPlayerCard({ player, isDoubles }:
    { player: IMatchPlayer, isDoubles: boolean }
) {

    return (<Row className="align-items-center overflow-hidden">
        <Col xs={12} sm={3}>
            {isDoubles
                ? <Row>
                    <MiniProfileAvatar
                        userId={(player as IDoublesPlayer).player1}
                        player={(player as IDoublesPlayer).Player1}
                    />
                    <MiniProfileAvatar
                        userId={(player as IDoublesPlayer).player2}
                        player={(player as IDoublesPlayer).Player2}
                    />
                </Row>
                : <MiniProfileAvatar
                    userId={(player as ISinglesPlayer).player}
                    player={(player as ISinglesPlayer).Player}
                />
            }
        </Col>
        <Col sm={9} className="text-truncate d-none d-sm-block">
            {toShortNames(player)}
        </Col>
    </Row>);
}

function MiniProfileAvatar({ player, userId }:
    { player: INamed, userId: number }
) {
    const { avatarsState, scheduleAvatarFetch } = useContext(AvatarsContext);
    const { userState } = useContext(UserContext);
    const isSelf = userState.id === userId;
    useEffect(
        () => {
            if (!isSelf) {
                scheduleAvatarFetch(userId);
            }
        },
        [scheduleAvatarFetch, isSelf, userId]
    );
    const avatar = isSelf ? userState.Avatar : avatarsState[userId];

    return (
        <OverlayTrigger
            placement="right"
            overlay={
                <Popover id="profile-popover" className="d-sm-none">
                    <Popover.Title as="h3">
                        {toShortName(player)}
                    </Popover.Title>
                </Popover>
            }
        >
            <span>
                <AvatarImage avatar={avatar} size={24} />
            </span>
        </OverlayTrigger>
    );
}

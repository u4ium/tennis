import React from "react";
import Accordion from "react-bootstrap/Accordion";
import { useLocation } from "react-router-dom";
import { IMatch } from "../../common/model/match/MatchModel";
import { toPlayerName, toValidationDate } from "../utils";
import { ContextAwareToggle } from "./ContextAwareToggle";
import { PlayedMatch } from "./PlayedSinglesMatch";

export function MatchCard(
    { rootUrl, match, isDoubles, didWin, wasSeed }: {
        rootUrl: string;
        match: IMatch;
        isDoubles: boolean;
        didWin: boolean;
        wasSeed: boolean;
    }
) {
    const eventKey = `${rootUrl}/${match.id}`;
    const location = useLocation();

    return (
        <div key={match.id}>
            <ContextAwareToggle eventKey={eventKey}>
                You {didWin ? "Won" : "Lost"} against {toPlayerName(wasSeed
                ? match.challenger
                : match.seed)} on {toValidationDate(match.played)}
            </ContextAwareToggle>
            <Accordion.Collapse eventKey={eventKey}>
                <PlayedMatch
                    key={location.pathname}
                    match={match}
                    isDoubles={isDoubles}
                />
            </Accordion.Collapse>
        </div>
    );
}

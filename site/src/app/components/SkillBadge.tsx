import React from "react";
import Badge from "react-bootstrap/Badge";

export function SkillBadge({ skill }: { skill: number; }) {
    return (
        <Badge variant="success">
            {(skill / 2 + 1).toFixed(1)}
        </Badge>
    );
}

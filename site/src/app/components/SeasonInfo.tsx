import React from "react";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import { ISeason } from "../../common/model/season/Season";

export function SeasonInfo({ start, end, length, matchesPerCycle }: ISeason) {
    return (
        <Form.Group as={Form.Row} className="justify-content-between">
            <Col className="d-md-none" xs={2} />
            <Col xs={10} sm={5} md={6}>
                <InputGroup className="align-items-stretch">
                    <InputGroup.Prepend>
                        <InputGroup.Text>
                            <pre className="mb-0">From:</pre>
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <InputGroup.Text>
                        {start}
                    </InputGroup.Text>
                </InputGroup>
            </Col>
            <Col className="d-sm-none" xs={2} />
            <Col xs={10} sm={5} md={6}>
                <InputGroup className="align-items-stretch">
                    <InputGroup.Prepend>
                        <InputGroup.Text>
                            <pre className="mb-0">Ends:</pre>
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <InputGroup.Text>
                        {end}
                    </InputGroup.Text>
                </InputGroup>
            </Col>
            <Col className="d-md-none" xs={2} />
            <Col xs={10}>
                <Form.Text>
                    {matchesPerCycle} matches every {length} days
                </Form.Text>
            </Col>
        </Form.Group>
    );
}

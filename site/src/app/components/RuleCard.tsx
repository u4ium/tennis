import React from "react";
import IRule from "../../common/model/rule/RuleModel";

// No idea why passing props directly (from context of parent) breaks SSR...

export function RuleCard({
    name,
    setsToWin,
    setWinScore,
    tiebreakWinScore,
    isDoubles
}: IRule & { isDoubles: boolean; }) {
    return (
        <div>
            Rules: {name || "Any"}
            {name && <ul className="d-none d-lg-block">
                <li>
                    {setsToWin > 1
                        ? `Best of ${setsToWin * 2 - 1} sets.`
                        : "Play one set."}
                </li>
                <li>
                    First {isDoubles ? "team" : "player"} to
                    win {setWinScore} games wins each set
                    (must win by at least 2 games).
                </li>
                {tiebreakWinScore
                    ? <>
                        <li>
                            If set score is tied at {setWinScore},
                            then play a tiebreak.
                        </li>
                        <li>
                            A tiebreak is won at {tiebreakWinScore} points,
                            (must win by at least 2 points).
                        </li>
                    </>
                    :
                    <li>
                        No tiebreaks (Advantage Sets); keep playing until
                        someone wins by 2 games.
                    </li>}
            </ul>}
        </div>
    );
}

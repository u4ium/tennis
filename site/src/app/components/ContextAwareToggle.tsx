import React, { ReactNode, useContext } from "react";
import { PlusSquare, XSquare } from "react-bootstrap-icons";
import AccordionContext from "react-bootstrap/AccordionContext";
import { useAccordionToggle } from "react-bootstrap/AccordionToggle";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { NavLink } from "react-router-dom";

export function ContextAwareToggle({ children, eventKey, callback }: {
    children: ReactNode;
    eventKey: string;
    callback?: (eventKey: string) => void;
}) {
    const currentEventKey = useContext(AccordionContext);

    const decoratedOnClick = useAccordionToggle(
        eventKey,
        () => callback && callback(eventKey)
    );

    const isCurrentEventKey = currentEventKey === eventKey;
    const parentUrl = eventKey.split("/").slice(0, -1).join("/");

    return (
        <NavLink
            to={`${isCurrentEventKey ? parentUrl : eventKey}`}
            className="link-unstyled"
        >
            <Card.Header
                onClick={decoratedOnClick}
                className={isCurrentEventKey ? "bg-white" : "bg-light"}
            >
                <Row>
                    <Col>
                        {children}
                    </Col>
                    <Col xs="auto">
                        {isCurrentEventKey
                            ? <XSquare size={24} />
                            : <PlusSquare size={24} />}
                    </Col>
                </Row>
            </Card.Header>
        </NavLink>
    );
}

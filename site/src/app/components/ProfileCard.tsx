import React from "react";
import Card from "react-bootstrap/Card";
import Media from "react-bootstrap/Media";
import { IPatchUser } from "../../common/model/user/PatchUser";
import AvatarImage from "./AvatarImage";
import { SkillBadge } from "./SkillBadge";

export function ProfileCard(
    { firstName, lastName, bio, skill, Avatar }: IPatchUser
) {
    return (
        <Card>
            <Card.Body>
                <Media>
                    <AvatarImage avatar={Avatar} />
                    <Media.Body className="ml-3 overflow-hidden">
                        <h5 className="d-flex justify-content-between overflow-hidden">
                            <span className="text-truncate">
                                {firstName}
                                <br />
                                {lastName}
                            </span>
                            <span className="col-auto">
                                <SkillBadge skill={skill} />
                            </span>
                        </h5>
                        <div className="overflow-hidden">
                            <p className="text-truncate wrap-text">{bio}</p>
                        </div>
                    </Media.Body>
                </Media>
            </Card.Body>
        </Card>
    );
}

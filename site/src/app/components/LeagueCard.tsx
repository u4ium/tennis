import React, { useContext } from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import ILeague from "../../common/model/league/LeagueModel";
import { ErrorContext } from "../contexts/ErrorContext";
import { LeaguesContext } from "../contexts/LeaguesContext";
import { UserContext } from "../contexts/UserContext";
import { joinDoublesLeague, joinSinglesLeague } from "../requests/league";
import { RuleCard } from "./RuleCard";
import { SkillBadge } from "./SkillBadge";

export function LeagueCard({ league }: { league: ILeague; }) {
    const { leaguesState, setLeaguesState } = useContext(LeaguesContext);
    const { userState } = useContext(UserContext);
    const { onError } = useContext(ErrorContext);

    const leagueIndex = leaguesState.findIndex(
        (l) => (l.id === league.id && l.isDoubles === league.isDoubles));

    const activeSeason = league.Seasons?.[0];
    const isParticipant = !!activeSeason?.participants?.find(
        (p) => p.id === userState.id);

    const joinLeague = (league.isDoubles
        ? () => {
            joinDoublesLeague({ id: league.id })
                .then(() => setLeaguesState((p) => {
                    activeSeason.participants = [
                        ...(activeSeason.participants || []),
                        { id: userState.id }
                    ];
                    p[leagueIndex].Seasons[0] = activeSeason;
                    return [...p];
                }))
                .catch(onError);
        }
        : () => {
            joinSinglesLeague({ id: league.id })
                .then(() => setLeaguesState((p) => {
                    activeSeason.participants = [
                        ...(activeSeason.participants || []),
                        { id: userState.id }
                    ];
                    p[leagueIndex].Seasons[0] = activeSeason;
                    return p;
                }))
                .catch(onError);
        }
    );

    return (
        <Card className="w-md-75">
            <Card.Header>
                <Row className="justify-content-between">
                    <span className="col-12 col-md-7">
                        <h4>
                            {`${league.name} ${league.isDoubles
                                ? "(Doubles)"
                                : "(Singles)"}`}
                        </h4>
                    </span>
                    <span className="col-12 col-md-5 text-right">
                        <h4>
                            <SkillBadge skill={league.minSkill} />
                            -
                            <SkillBadge skill={league.maxSkill} />
                        </h4>
                    </span>
                </Row>
            </Card.Header>
            <Card.Body>
                <p>{league.about}</p>
                <RuleCard {...league.Rule} isDoubles={league.isDoubles} />
            </Card.Body>
            <Card.Footer>
                {activeSeason
                    ? <Row className="justify-content-end">
                        <Button
                            disabled={isParticipant}
                            className="d-flex mr-2"
                            onClick={joinLeague}
                        >
                            {isParticipant ? "Play" : "Join"}
                        </Button>
                    </Row>
                    : <p>Sorry, the season is over for now.</p>
                }
            </Card.Footer>
        </Card>
    );
}

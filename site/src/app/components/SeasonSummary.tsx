import React, { useContext } from "react";
import Accordion from "react-bootstrap/Accordion";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { useLocation } from "react-router-dom";
import { IMatch } from "../../common/model/match/MatchModel";
import { ISeason } from "../../common/model/season/Season";
import { UserContext } from "../contexts/UserContext";
import { SeasonMatches } from "../props";
import { didWinMatch, wasMatchSeed } from "../utils";
import { ContextAwareToggle } from "./ContextAwareToggle";
import { MatchCard } from "./MatchCard";

function getMatchesData(userId: number, matches: IMatch[], isDoubles: boolean) {
    const wasSeedOfMatches = matches?.map((m) =>
        wasMatchSeed(userId, isDoubles, m)) || [];
    const didWinMatches = matches?.map((m, i) =>
        didWinMatch(wasSeedOfMatches[i], m)) || [];
    const numWonMatches = didWinMatches.reduce((p, w) => p + Number(w), 0) || 0;
    const numLostMatches = matches?.length - numWonMatches || 0;
    return {
        wasSeedOfMatches,
        didWinMatches,
        numWonMatches,
        numLostMatches,
    };
}

export function SeasonSummary({ rootUrl, season, seasonDetails, isDoubles }:
    {
        rootUrl: string,
        season: SeasonMatches;
        seasonDetails?: ISeason;
        isDoubles: boolean;
    }
) {
    const location = useLocation();
    const { userState } = useContext(UserContext);
    const eventKey = `${rootUrl}/${season.id}`;

    // TODO: memoize
    const {
        wasSeedOfMatches,
        didWinMatches,
        numWonMatches,
        numLostMatches,
    } = getMatchesData(userState.id, season.matches, isDoubles);
    const seasonTypeString = seasonType(
        seasonDetails?.start,
        seasonDetails?.end
    );

    return (<>
        <ContextAwareToggle eventKey={eventKey}>
            <Row >
                <Col>
                    <h6>{seasonTypeString} Season</h6>
                </Col>
                <Col>
                    <h6>{seasonDetails?.start} to {seasonDetails?.end}</h6>
                </Col>
            </Row>
        </ContextAwareToggle>
        <Accordion.Collapse eventKey={eventKey}>
            <Card>
                <Card.Header>
                    <Row className="justify-content-between">
                        <Col>
                            Won: {numWonMatches}
                        </Col>
                        <Col>
                            Lost: {numLostMatches}
                        </Col>
                    </Row>
                </Card.Header>
                <Card.Body>
                    <Accordion activeKey={location.pathname}>
                        {season.matches.length
                            ? season.matches.map((match, matchIndex) =>
                                <MatchCard
                                    key={matchIndex}
                                    rootUrl={`${rootUrl}/${season.id}`}
                                    match={match}
                                    isDoubles={isDoubles}
                                    didWin={didWinMatches[matchIndex]}
                                    wasSeed={wasSeedOfMatches[matchIndex]}
                                />
                            )
                            : "No matches played this season..."
                        }
                    </Accordion>
                </Card.Body>
            </Card>
        </Accordion.Collapse>
    </>);
}

function seasonType(start: Date | string, end: Date | string) {
    const today = new Date();
    const startDate = new Date(start);
    const endDate = new Date(end);
    if (startDate < today && today < endDate) {
        return "Current";
    } else if (startDate > today) {
        return "Upcoming";
    } else {
        return "Past";
    }
}

import React from "react";
import Form from "react-bootstrap/Form";
import { UserConstants } from "../../common/model/user/UserValidator";

export default function NameInput({
    reference,
    formState,
    updateFormState,
    name,
    disabled = false }: {
        reference?: React.MutableRefObject<HTMLInputElement>,
        formState: { firstName?: string, lastName?: string },
        updateFormState: React.ChangeEventHandler<HTMLInputElement>,
        name: "firstName" | "lastName",
        disabled?: boolean
    }) {
    return (<>
        <Form.Control
            required
            disabled={disabled}
            ref={reference}
            value={formState[name]}
            onChange={updateFormState}
            name={name}
            maxLength={UserConstants.NAME_MAX_LENGTH}
            pattern={UserConstants.NAME_MATCHER}
            autoComplete={name === "firstName" ? "given-name" : "family-name"}
            placeholder={`Enter ${name === "firstName"
                ? "First"
                : "Last"} Name`}
        />
    </>);
}

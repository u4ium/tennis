import React, { useEffect, useRef } from "react";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Row from "react-bootstrap/Row";
import { IScheduleSection } from "../../common/model/schedule/Schedule";
import { FormChangeEvent, FormElement } from "../forms/types/FormElements";
import { FIFTEEN_MINUTES_IN_SECONDS, toSeconds } from "../utils";
import IconButton from "./IconButton";

function cleanTime(time: string): string {
    const [hh, minutes] = time.split(":");
    const quarterHours = Math.round(Number(minutes) / 15);
    let mm: string;
    switch (quarterHours) {
        case 0: { mm = "00"; break; }
        case 1: { mm = "15"; break; }
        case 2: { mm = "30"; break; }
        case 3: { mm = "45"; break; }
        default: { mm = "00"; break; }
    }
    return `${hh}:${mm}:00`;
}

export function AvailabiltySelector({ type, min, max, values, setValues }: {
    type: string;
    min?: string;
    max?: string;
    values: IScheduleSection[];
    setValues(vs: IScheduleSection[]): void;
}) {
    const minimum = min || "07:00:00";
    const maximum = max || "21:00:00";

    const setSection = (index: number, value: IScheduleSection) => {
        values[index] = value;
        setValues(values);
    };
    const addSection = () => setValues([
        ...(values || []),
        {
            start: values?.[values.length - 1]?.end || minimum,
            end: maximum
        }
    ]);
    const removeSection = (index: number) => setValues(
        values.filter((v, i) => i !== index));

    return (<>
        <Form.Group as={Form.Row}>
            <Col xs={12} md={4}>
                <Form.Label>
                    Availability: {type}
                </Form.Label>
            </Col>
            <Col>
                {values.map((section, index) =>
                    <AvailabilitySection
                        key={index}
                        value={section}
                        setSectionValue={(v) => setSection(index, v)}
                        removeSelf={() => removeSection(index)}
                        minimum={minimum}
                        maximum={maximum}
                    />
                )}
                <Form.Row className="justify-content-end">
                    <Col xs={9} sm={11}>
                        <Form.Text>
                            {values.length
                                ? `Split your ${type} availabilty:`
                                : `Add availablity for ${type}:`}
                        </Form.Text>
                    </Col>
                    <Col xs="auto" sm={1}>
                        <IconButton
                            iconName="icono-plus"
                            variant="outline-success"
                            onClick={addSection}
                        />
                    </Col>
                </Form.Row>
            </Col>
        </Form.Group>
    </>);
}

function AvailabilitySection(
    { value, setSectionValue, removeSelf, minimum, maximum }:
        {
            value: IScheduleSection,
            setSectionValue: (section: IScheduleSection) => void,
            removeSelf: VoidFunction,
            minimum: string,
            maximum: string,
        }
) {

    const minSeconds = toSeconds(minimum);
    const maxSeconds = toSeconds(maximum);
    const refs = {
        start: useRef<FormElement>(null),
        end: useRef<FormElement>(null),
    };
    useEffect(() => {
        refs?.start?.current?.setCustomValidity(
            toSeconds(value.start) % FIFTEEN_MINUTES_IN_SECONDS === 0
                ? "" : "invalid"
        );
        refs?.end?.current?.setCustomValidity(
            toSeconds(value.end) % FIFTEEN_MINUTES_IN_SECONDS === 0
                && value.end > value.start
                ? "" : "invalid"
        );
    }, [refs, value]);

    return (
        <Form.Group as={Form.Row} className="justify-content-end">
            <Col xs={9} md={11}>
                <Row>
                    <Col sm={12} md={6}>
                        <InputGroup hasValidation>
                            <InputGroup.Prepend>
                                <InputGroup.Text>
                                    <pre className="mb-0">at</pre>
                                </InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                ref={refs.start}
                                placeholder="From"
                                type="time"
                                value={value.start}
                                onChange={(e: FormChangeEvent) =>
                                    setSectionValue({
                                        start: e.target.value,
                                        end: value.end
                                    })}
                                onBlur={() => setSectionValue({
                                    start: cleanTime(value.start),
                                    end: value.end
                                })}
                                onSubmit={() => setSectionValue({
                                    start: cleanTime(value.start),
                                    end: value.end
                                })}
                                min={minSeconds}
                                max={maxSeconds}
                                step={FIFTEEN_MINUTES_IN_SECONDS} />
                            <Form.Control.Feedback type="invalid" tooltip>
                                <small>
                                    Must be in 15-minute increments, and
                                    start must be before end.
                                </small>
                            </Form.Control.Feedback>
                        </InputGroup>
                    </Col>
                    <Col sm={12} md={6}>
                        <InputGroup hasValidation>
                            <InputGroup.Prepend>
                                <InputGroup.Text>
                                    <pre className="mb-0">to</pre>
                                </InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                ref={refs.end}
                                placeholder="To"
                                type="time"
                                value={value.end}
                                onChange={(e: FormChangeEvent) =>
                                    setSectionValue({
                                        start: value.start,
                                        end: e.target.value
                                    })}
                                onBlur={() => setSectionValue({
                                    start: value.start,
                                    end: cleanTime(value.end)
                                })}
                                onSubmit={() => setSectionValue({
                                    start: value.start,
                                    end: cleanTime(value.end)
                                })}
                                min={toSeconds(value.start)}
                                max={maxSeconds}
                                step={FIFTEEN_MINUTES_IN_SECONDS} />
                            <Form.Control.Feedback type="invalid" tooltip>
                                <small>
                                    Must be in 15-minute increments, and
                                    start must be before end.
                                </small>
                            </Form.Control.Feedback>
                        </InputGroup>
                    </Col>
                </Row>
            </Col>
            <Col xs="auto" sm={1}>
                <IconButton
                    className="d-flex align-self-stretch h-100"
                    iconName="icono-cross"
                    variant="outline-danger"
                    onClick={removeSelf}
                />
            </Col>
        </Form.Group>
    );
}

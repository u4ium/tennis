import React, { useContext } from "react";
import Accordion from "react-bootstrap/Accordion";
import Card from "react-bootstrap/Card";
import { useLocation } from "react-router-dom";
import { IMatch } from "../../common/model/match/MatchModel";
import { LeaguesContext } from "../contexts/LeaguesContext";
import { MatchesContext } from "../contexts/MatchesContext";
import { LeagueMatches } from "../props";
import { ContextAwareToggle } from "./ContextAwareToggle";
import { MatchCard } from "./MatchCard";
import { SeasonSummary } from "./SeasonSummary";

export function PlayedMatches() {
    const location = useLocation();
    const activeKey = location.pathname.split(
        "/").slice(0, 4).join("/");
    const { matchesState } = useContext(MatchesContext);
    const playedMatches = matchesState?.leagueMatches.filter((league) =>
        !!league.Seasons?.find((season) =>
            !!season.matches?.find(((match) => !!match.played))
        ));

    // TODO: League stats card/link to ranking
    return (<Accordion activeKey={activeKey}>
        <ImpromptuMatches
            eventKey="/leagues/singles/0"
            matches={matchesState?.singlesMatches}
            isDoubles={false}
        />
        <ImpromptuMatches
            eventKey="/leagues/doubles/0"
            matches={matchesState?.doublesMatches}
            isDoubles={true}
        />
        {playedMatches?.map((league, leagueIndex) =>
            <LeagueMatches
                key={leagueIndex}
                league={league}
                eventKey={`/leagues/${league.isDoubles
                    ? "doubles"
                    : "singles"}/${league.id}`}
            />)}
    </Accordion>);
}

function ImpromptuMatches({ eventKey, matches, isDoubles }:
    { eventKey: string, matches: IMatch[], isDoubles: boolean }
) {
    const name = `Impromptu ${isDoubles ? "Singles" : "Doubles"}`;

    return <CollapsableLevel
        eventKey={eventKey}
        heading={name}
    >
        {matches?.length
            ? matches.map((match) => <MatchCard
                key={eventKey}
                rootUrl={eventKey}
                match={match}
                isDoubles={isDoubles}
                didWin={false}
                wasSeed={false}
            />)
            : `No ${name} Matches Played...`
        }
    </CollapsableLevel >;
}

function CollapsableLevel({ eventKey, heading, children }: {
    eventKey: string,
    heading: string | React.ReactNode,
    children: React.ReactNode
}) {

    const location = useLocation();
    const activeKey = location.pathname.split(
        "/").slice(0, 5).join("/");
    return (<>
        <ContextAwareToggle eventKey={eventKey}>
            <h5>{heading}</h5>
        </ContextAwareToggle>
        <Accordion.Collapse eventKey={eventKey}>
            <Card>
                <Card.Body>
                    <Accordion activeKey={activeKey}>
                        {children}
                    </Accordion>
                </Card.Body>
            </Card>
        </Accordion.Collapse>
    </>);
}

function LeagueMatches({ eventKey, league }:
    { eventKey: string, league: LeagueMatches }) {

    const { leaguesState } = useContext(LeaguesContext);
    const leagueDetails = leaguesState.find(
        (l) => l.id === league.id && l.isDoubles === league.isDoubles);

    return (
        <CollapsableLevel
            eventKey={eventKey}
            heading={`${leagueDetails?.name} ${league.isDoubles
                ? "(Doubles)"
                : "(Singles)"}`}
        >
            {league.Seasons?.length
                ? league.Seasons?.map((season, seasonIndex) =>
                    <div key={seasonIndex}>
                        <SeasonSummary
                            rootUrl={eventKey}
                            season={season}
                            seasonDetails={leagueDetails?.Seasons?.
                                find((s) => s.id === season.id)}
                            isDoubles={league.isDoubles} />
                    </div>
                )
                : `No matches played for this league...`
            }
        </CollapsableLevel>
    );
}

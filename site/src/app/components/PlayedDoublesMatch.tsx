import React from "react";
import Card from "react-bootstrap/Card";
import { IDoublesMatch } from "../../common/model/match/MatchModel";

export function PlayedDoublesMatch({ match, isVisible }:
    { match: IDoublesMatch; isVisible: boolean; }
) {
    return (
        <Card>
            <Card.Body>
                Score: {match.sets.map((s) =>
                `${s.seedScore}-${s.challengerScore}`)}
                {JSON.stringify(match.played)}
            </Card.Body>
        </Card>
    );
}

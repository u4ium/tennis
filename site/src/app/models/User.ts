interface IPerson {
    firstName: string;
    lastName: string;
}

export interface IPlayer extends IPerson {
    id: number;
}

export interface IUser extends IPerson {
    email: string;
}

export interface IUserP extends IUser {
    password: string;
}

export interface IUserPP extends IUserP {
    password2: string;
}

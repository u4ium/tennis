import ILeague from "../../common/model/league/LeagueModel";
import RequestFunction, {
    handleRequest,
    MandatoryRequestBody,
    MandatoryRequestId
} from "./RequestFunction";

export const addLeague: RequestFunction<
    MandatoryRequestBody<ILeague>,
    { id: number },
    ILeague
> =
    async ({ body }) => handleRequest(new Request("/api/league", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(body)
    }));

export const joinSinglesLeague: RequestFunction<MandatoryRequestId> =
    async ({ id }) => handleRequest(
        new Request(`/api/league/singles/${id}/player`, {
            method: "PUT"
        }));

export const joinDoublesLeague: RequestFunction<MandatoryRequestId> =
    async ({ id }) => handleRequest(
        new Request(`/api/league/doubles/${id}/player`, {
            method: "PUT"
        }));

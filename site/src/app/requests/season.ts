import { ISeason } from "../../common/model/season/Season";
import RequestFunction, {
    handleRequest,
    MandatoryRequestOptions,
} from "./RequestFunction";

export const addSinglesSeason: RequestFunction<
    MandatoryRequestOptions<ISeason>,
    { id: number },
    ISeason
> =
    async ({ body, id }) => handleRequest(
        new Request(`/api/league/singles/${id}/season`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(body)
        }));

export const addDoublesSeason: RequestFunction<
    MandatoryRequestOptions<ISeason>,
    { id: number },
    ISeason
> =
    async ({ body, id }) => handleRequest(
        new Request(`/api/league/doubles/${id}/season`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(body)
        }));

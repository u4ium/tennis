import { IPlayerUser } from "../../common/model/user/PlayerUser";
import RequestFunction, {
    handleRequest,
    MandatoryRequestId,
    MandatoryRequestName
} from "./RequestFunction";

export const search: RequestFunction<
    MandatoryRequestName,
    IPlayerUser[],
    null
> = async ({ name }) =>
        handleRequest(new Request(`/api/player/search/${name}`));

export const getUser: RequestFunction<
    MandatoryRequestId,
    IPlayerUser,
    null
> = async ({ id }) =>
        handleRequest(new Request(`/api/user/${id}`));

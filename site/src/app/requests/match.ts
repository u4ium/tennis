import {
    IDoublesMatch,
    ISinglesMatch
} from "../../common/model/match/MatchModel";
import RequestFunction, {
    handleRequest,
    MandatoryRequestBody
} from "./RequestFunction";

export const addSinglesMatch: RequestFunction<
    MandatoryRequestBody<ISinglesMatch>, { id: number }, ISinglesMatch
> = async ({ body }) => handleRequest(new Request("/api/match/singles", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(body)
}));

export const addDoublesMatch: RequestFunction<
    MandatoryRequestBody<IDoublesMatch>, { id: number }, IDoublesMatch
> = async ({ body }) => handleRequest(new Request("/api/match/doubles", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(body)
}));

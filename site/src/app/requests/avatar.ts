import { IAvatar } from "../../common/model/avatar/Avatar";
import RequestFunction, {
    handleRequest,
    MandatoryRequestId,
} from "./RequestFunction";

export const getAvatar: RequestFunction<
    MandatoryRequestId,
    IAvatar,
    null
> = async ({ id }) => handleRequest(new Request(`/api/avatar/${id}`));

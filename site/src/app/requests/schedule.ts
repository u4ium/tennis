import { ISchedule } from "../../common/model/schedule/Schedule";
import RequestFunction, {
    handleRequest,
    MandatoryRequestOptions,
} from "./RequestFunction";

export const replaceSchedule: RequestFunction<
    MandatoryRequestOptions<ISchedule>,
    { id: number },
    ISchedule
> =
    async ({ body, id }) => handleRequest(
        new Request(`/api/schedule/${id}`,
            {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(body),
            }));

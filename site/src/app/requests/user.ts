import { IPatchUser } from "../../common/model/user/PatchUser";
import { ISignInUser } from "../../common/model/user/SignInUser";
import { ISignUpUser } from "../../common/model/user/SignUpUser";
import { AuthUserProps } from "../props";
import RequestFunction, {
    handleRequest,
    MandatoryRequestBody,
    MandatoryRequestOptions,
} from "./RequestFunction";

export const editUser: RequestFunction<
    MandatoryRequestOptions<Partial<IPatchUser>>,
    null,
    Partial<IPatchUser>
> =
    async ({ body, id }) =>
        handleRequest(
            new Request(`/api/user/${id}`, {
                method: "PATCH",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(body),
            }),
        );

export const login: RequestFunction<
    MandatoryRequestBody<ISignInUser>,
    AuthUserProps,
    ISignInUser
> =
    async ({ body }) => handleRequest(
        new Request("/api/auth/", {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(body),
        })
    );

export const signup: RequestFunction<
    MandatoryRequestBody<ISignUpUser>,
    AuthUserProps,
    ISignUpUser
> =
    async ({ body }) =>
        handleRequest(
            new Request("/api/auth/", {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(body),
            }),
        );

export const logout: RequestFunction =
    async () => handleRequest(
        new Request("/api/auth/", { method: "DELETE" })
    );

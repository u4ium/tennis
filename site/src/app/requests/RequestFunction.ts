export type ErrorHandlerFunction = (err: Error) => void;

interface RequestOptions<B> {
    body?: B;
    id?: number;
    name?: string;
}

export interface MandatoryRequestName extends RequestOptions<null> {
    name: string;
}

export interface MandatoryRequestOptions<B> extends RequestOptions<B> {
    body: B;
    id: number;
}

export interface MandatoryRequestBody<B> extends RequestOptions<B> {
    body: B;
    id?: number;
}

export interface MandatoryRequestId extends RequestOptions<undefined> {
    id: number;
}

export type RequestFunction<
    O extends RequestOptions<B> = {},
    V = undefined,
    B = undefined
    > = (options: O) => Promise<V>;

export default RequestFunction;

export async function handleRequest<R = any>(
    request: Request,
): Promise<R> {
    const response = await fetch(request);
    if (response.ok) {
        return response.status === 204 ? null : await response.json();
    } else {
        const msg = await response.text();
        throw new Error(msg);
    }
}

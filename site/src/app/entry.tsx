import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import "react-bootstrap-typeahead/css/Typeahead.css";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import { AppProps } from "./props";
import "./style/custom.css";
import "./style/icono.min.css";
// import "./style/About.css";
// import "./style/Admin.css";
// import "./style/App.css";
// import "./style/Button.css";
// import "./style/GamesView.css";
// import "./style/LadderView.css";
// import "./style/LoginScreen.css";
// import "./style/NavBar.css";
// import "./style/NewGameForm.css";
// import "./style/ProfileView.css";

declare const appProps: AppProps;
ReactDOM.hydrate(
    <BrowserRouter>
        <App {...appProps} />
    </BrowserRouter>,
    document.getElementById("app")
);

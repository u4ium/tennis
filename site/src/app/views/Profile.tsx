import React, { useContext } from "react";
import Accordion from "react-bootstrap/Accordion";
import Card from "react-bootstrap/Card";
import { useLocation } from "react-router-dom";
import { ContextAwareToggle } from "../components/ContextAwareToggle";
import { ProfileCard } from "../components/ProfileCard";
import { UserContext } from "../contexts/UserContext";
import { EditPasswordForm } from "../forms/EditPasswordForm";
import { ProfileForm } from "../forms/ProfileForm";
import { ScheduleSelectorForm } from "../forms/ScheduleSelectorForm";

export default function ProfileView() {
    const { userState, } = useContext(UserContext);
    const location = useLocation();
    const activeKey = location.pathname;

    // TODO: use router to control Accordion
    const close = () => { }; // tslint:disable-line
    return (
        <Accordion activeKey={activeKey} >
            <ContextAwareToggle eventKey="/profile/view">
                <h4>
                    View Your Profile
                </h4>
            </ContextAwareToggle>
            <Accordion.Collapse eventKey="/profile/view">
                <ProfileCard {...userState} />
            </Accordion.Collapse>

            <ContextAwareToggle eventKey="/profile/edit">
                <h4>
                    Edit Your Profile
                </h4>
            </ContextAwareToggle>
            <Accordion.Collapse eventKey="/profile/edit">
                <Card>
                    <Card.Body>
                        <ProfileForm close={close} />
                    </Card.Body>
                </Card>
            </Accordion.Collapse>

            <ContextAwareToggle eventKey="/profile/schedule">
                <h4>
                    Edit Your Schedule
                </h4>
            </ContextAwareToggle>
            <Accordion.Collapse eventKey="/profile/schedule">
                <Card>
                    <Card.Body>
                        <ScheduleSelectorForm />
                    </Card.Body>
                </Card>
            </Accordion.Collapse>

            <ContextAwareToggle eventKey="/profile/password">
                <h4>
                    Edit Your Password
                </h4>
            </ContextAwareToggle>
            <Accordion.Collapse eventKey="/profile/password">
                <Card>
                    <Card.Body>
                        <EditPasswordForm close={close} />
                    </Card.Body>
                </Card>
            </Accordion.Collapse>
        </Accordion>
    );
}

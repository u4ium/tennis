import React, { useContext, useState } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Modal from "react-bootstrap/Modal";
import { extractSignInUser } from "../../common/model/user/SignInUser";
import { extractSignUpUser } from "../../common/model/user/SignUpUser";
import { UserConstants } from "../../common/model/user/UserValidator";
import ErrorBar from "../components/ErrorBar";
import NameInput from "../components/NameInput";
import { UserContext } from "../contexts/UserContext";
import useErrorHandler from "../effects/useErrorHandler";
import useFormData from "../effects/useFormData";
import useFormValidateOnSubmit from "../effects/useFormValidateOnSubmit";
import { IUserPP } from "../models/User";
import { AuthUserProps } from "../props";
import { login, signup } from "../requests/user";
import { escapeRegex } from "../utils";

interface ILoginFormProps {
    inviteEmail?: string;
    isSignInMode: boolean;
    setIsSignInMode: React.Dispatch<React.SetStateAction<boolean>>;
    handleError(error: Error): void;
    close(): void;
}

export function LoginForm({
    inviteEmail,
    handleError,
    isSignInMode,
    setIsSignInMode,
    close,
}: ILoginFormProps) {
    const { setUserState } = useContext(UserContext);
    const { formData, updateFormData } = useFormData<IUserPP>({
        email: inviteEmail || "",
        password: "",
        password2: "",
        firstName: "",
        lastName: "",
    });

    const handleSuccess = (userResponse: AuthUserProps) => {
        setUserState(userResponse);
        close();
    };
    const { validated, handleSubmit } = useFormValidateOnSubmit(async () => {
        if (isSignInMode) {
            const user = await login({ body: extractSignInUser(formData) });
            handleSuccess(user);
        } else {
            const user = await signup({ body: extractSignUpUser(formData) });
            handleSuccess(user);
        }
        return false;
    }, handleError);

    return (
        <Container>
            <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <Form.Group as={Form.Row} controlId="formLoginEmail">
                    <Form.Label column md={4}>Email address</Form.Label>
                    <Col>
                        <Form.Control
                            required
                            value={formData.email}
                            onChange={updateFormData}
                            name="email"
                            type="email"
                            maxLength={UserConstants.EMAIL_MAX_LENGTH}
                            autoComplete="username"
                            placeholder="Enter email"
                        />
                        <Form.Control.Feedback type="invalid" tooltip>
                            Please enter your (valid) email address.
                        </Form.Control.Feedback>
                    </Col>
                </Form.Group>

                <Form.Group as={Form.Row} controlId="formLoginPassword">
                    <Form.Label column md={4}>Password</Form.Label>
                    <Col>
                        <Form.Control
                            required
                            value={formData.password}
                            onChange={updateFormData}
                            name="password"
                            type="password"
                            minLength={UserConstants.PASSWORD_MIN_LENGTH}
                            maxLength={UserConstants.PASSWORD_MAX_LENGTH}
                            autoComplete={
                                isSignInMode
                                    ? "current-password"
                                    : "new-password"
                            }
                            placeholder="Password"
                        />
                        <Form.Control.Feedback type="invalid" tooltip>
                            Please choose a password.
                        </Form.Control.Feedback>
                    </Col>
                </Form.Group>

                {isSignInMode ? null :
                    <>
                        <Form.Group
                            as={Form.Row}
                            controlId="formLoginPassword2"
                        >
                            <Form.Label column md={4}>
                                Confirm Password
                            </Form.Label>
                            <Col>
                                <Form.Control
                                    required
                                    value={formData.password2}
                                    onChange={updateFormData}
                                    name="password2"
                                    type="password"
                                    pattern={escapeRegex(formData.password)}
                                    autoComplete="new-password"
                                    placeholder="Repeat Password"
                                />
                                <Form.Control.Feedback type="invalid" tooltip>
                                    Passwords must match.
                                </Form.Control.Feedback>
                            </Col>
                        </Form.Group>
                        <Form.Group
                            as={Form.Row}
                            controlId="formLoginFirstName"
                        >
                            <Form.Label column md={4}>First Name</Form.Label>
                            <Col>
                                <NameInput
                                    formState={formData}
                                    updateFormState={updateFormData}
                                    name="firstName"
                                />
                                <Form.Control.Feedback type="invalid" tooltip>
                                    Please enter your
                                    given name (letters only).
                                </Form.Control.Feedback>
                            </Col>
                        </Form.Group>
                        <Form.Group
                            as={Form.Row}
                            controlId="formLoginLastName"
                        >
                            <Form.Label column md={4}>Last Name</Form.Label>
                            <Col>
                                <NameInput
                                    formState={formData}
                                    updateFormState={updateFormData}
                                    name="firstName"
                                />
                                <Form.Control.Feedback type="invalid" tooltip>
                                    Please enter your
                                    family name (letters only).
                                </Form.Control.Feedback>
                            </Col>
                        </Form.Group>
                    </>
                }

                <Form.Row className="justify-content-between">
                    <Col xs="auto">
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>
                                    {isSignInMode ?
                                        "Don't" :
                                        "Already"
                                    } have an account?
                                </InputGroup.Text>
                            </InputGroup.Prepend>
                            <Button
                                variant="secondary"
                                onClick={() => setIsSignInMode(!isSignInMode)}>
                                {isSignInMode ? "Sign Up" : "Sign In"}
                            </Button>
                        </InputGroup>
                    </Col>
                    <Col xs="auto">
                        <Button variant="primary" type="submit">
                            {isSignInMode ? "Sign In" : "Sign Up"}
                        </Button>
                    </Col>
                </Form.Row>
            </Form>
            <hr data-content="OR" className="hr-text" />
        </Container>
    );
}

interface LoginProps {
    inviteEmail: string;
    show: boolean;
    setShow: React.Dispatch<React.SetStateAction<boolean>>;
}

export default function Login({ inviteEmail, show, setShow }: LoginProps) {
    const [isSignInMode, setIsSignInMode] = useState(!inviteEmail);
    const handleClose = () => setShow(false);
    const { errors, onError } = useErrorHandler();

    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            size="lg"
            centered
            aria-labelledby="login-modal"
        >
            <Modal.Header closeButton>
                <Modal.Title id="login-modal">
                    {isSignInMode ?
                        "Log In to The Tennis Ladder" :
                        "Sign Up for The Tennis Ladder"}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <LoginForm
                    inviteEmail={inviteEmail}
                    handleError={onError}
                    isSignInMode={isSignInMode}
                    setIsSignInMode={setIsSignInMode}
                    close={handleClose}
                />
            </Modal.Body>
            <Modal.Footer>
                <ErrorBar errors={errors} />
            </Modal.Footer>
        </Modal>
    );
}

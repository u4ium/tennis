import React, { useContext, useState } from "react";
import Button from "react-bootstrap/Button";
import { LeaguesContext } from "../contexts/LeaguesContext";
import { UserContext } from "../contexts/UserContext";
import { ManageLeagueForm } from "../forms/ManageLeagueForm";
import { NewLeagueForm } from "../forms/NewLeagueForm";

export default function Manage() {
    const { userState } = useContext(UserContext);
    const { leaguesState } = useContext(LeaguesContext);
    const [isFormOpen, setIsFormOpen] = useState(false);
    const myLeagues = leaguesState.filter((l) => l.owner === userState.id);

    return (<>
        <h2>My Leagues</h2>
        {myLeagues.map((league, i) =>
            <ManageLeagueForm key={i} league={league} />
        )}

        {isFormOpen
            ? <NewLeagueForm close={() => setIsFormOpen(false)} />
            : <Button onClick={() => setIsFormOpen(true)} >
                Create New League
            </Button>}
    </>);
}

import React, { useContext, useState } from "react";
import Button from "react-bootstrap/Button";
import { PlayedMatches } from "../components/PlayedMatches";
import { MatchesContext } from "../contexts/MatchesContext";
// import { LeaguesContext } from "../contexts/LeaguesContext";
// import { UserContext } from "../contexts/UserContext";
import { NewMatchForm } from "../forms/NewMatchForm";

export default function Matches() {

    const [isFormOpen, setIsFormOpen] = useState(false);

    return (
        <>
            <h3>Upcoming Matches</h3>
            <UpcomingMatches />

            <h3>Played Matches</h3>
            <PlayedMatches />

            <h3>Record a New Match</h3>
            {isFormOpen
                ? <NewMatchForm close={() => setIsFormOpen(false)} />
                : <Button onClick={() => setIsFormOpen(true)} >
                    Record Match
                </Button>}
        </>
    );
}

function UpcomingMatches() {
    // const {userState} = useContext(UserContext);
    // const {leaguesState} = useContext(LeaguesContext);
    const { matchesState } = useContext(MatchesContext);

    const upcomingMatches = matchesState.leagueMatches.filter((league) =>
        !!league.Seasons?.find((season) =>
            !!season.matches?.find(((match) => !match.played))
        ));

    return (<>
        {upcomingMatches?.map((league, leagueIndex) =>
            <div key={leagueIndex}>
                <h5>{league.id} {league.isDoubles}</h5>
                {league.Seasons?.map((season, seasonIndex) =>
                    <div key={seasonIndex}>
                        {JSON.stringify(season.matches)}
                    </div>
                )}
            </div>)}
    </>);
}

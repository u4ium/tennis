import React, { useContext } from "react";
import Carousel from "react-bootstrap/Carousel";
import Container from "react-bootstrap/Container";
import Jumbotron from "react-bootstrap/Jumbotron";
import ILeague from "../../common/model/league/LeagueModel";
import { LeagueCard } from "../components/LeagueCard";
import { PlayedMatches } from "../components/PlayedMatches";
import { LeaguesContext } from "../contexts/LeaguesContext";

export default function Leagues() {
    const { leaguesState } = useContext(LeaguesContext);

    return (<>
        <h3>
            Join A League
        </h3>
        <Carousel>
            {leaguesState.map((league: ILeague, index: number) =>
                <Carousel.Item key={index}>
                    <Jumbotron className="h-100">
                        <Container
                            className="d-flex justify-content-center"
                        >
                            <LeagueCard league={league} />
                        </Container>
                    </Jumbotron>
                </Carousel.Item>
            )}
        </Carousel>
        <h3>
            My Leagues
        </h3>
        <PlayedMatches />
    </>);
}

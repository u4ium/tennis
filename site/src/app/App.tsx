import React, { useEffect, useMemo, useState } from "react";
import Container from "react-bootstrap/Container";
import { Route, Switch } from "react-router";
import { Redirect } from "react-router-dom";
import { IAvatar } from "../common/model/avatar/Avatar";
import ErrorBar from "./components/ErrorBar";
// import LadderView from "./LadderView";
import NavBar from "./components/NavBar";
import { AvatarsContext } from "./contexts/AvatarsContext";
import { ErrorContext } from "./contexts/ErrorContext";
import { LeaguesContext } from "./contexts/LeaguesContext";
import { MatchesContext } from "./contexts/MatchesContext";
import compose from "./contexts/MultiProvider";
import { RulesContext } from "./contexts/RulesContext";
import { UserContext } from "./contexts/UserContext";
import useCachedAvatars from "./effects/useCachedAvatars";
import useErrorHandler from "./effects/useErrorHandler";
import {
    AppProps,
    ClubProps,
    extractClubProps,
} from "./props";
// import GamesView from "./GamesView";
import About from "./views/About";
import Leagues from "./views/Leagues";
import Login from "./views/Login";
import Manage from "./views/Manage";
import Matches from "./views/Matches";
import Privacy from "./views/Privacy";
// import Admin from "./views/Admin";
import Profile from "./views/Profile";

export default function App({ user,
    inviteEmail,
    rules,
    leagues,
    matches,
    ...props }: AppProps
) {
    const [userState, setUserState] = useState(user);
    const userProviderValue = useMemo(
        () => ({ userState, setUserState }),
        [userState, setUserState]);

    const isAuthenticated = () => Boolean(userState?.id);

    const [showLogin, setShowLogin] = useState(false);

    const { errors, onError } = useErrorHandler();
    const errorProviderValue = useMemo(
        () => ({ errors, onError }),
        [errors, onError]);

    const isAdministrator = (): boolean => !!userState?.isAdministrator;

    const clubProps: ClubProps = extractClubProps(props);

    const [rulesState, setRulesState] = useState(rules);
    const rulesProviderValue = useMemo(
        () => ({ rulesState, setRulesState }),
        [rulesState, setRulesState]
    );

    const [leaguesState, setLeaguesState] = useState(leagues);
    const leaguesProviderValue = useMemo(
        () => ({ leaguesState, setLeaguesState }),
        [leaguesState, setLeaguesState]
    );

    const [matchesState, setMatchesState] = useState(matches);
    const matchesProviderValue = useMemo(
        () => ({ matchesState, setMatchesState }),
        [matchesState, setMatchesState]
    );

    const [
        avatarsState,
        setAvatarsState
    ] = useState<Record<number, IAvatar>>({});
    const { scheduleAvatarFetch } = useCachedAvatars(
        avatarsState,
        setAvatarsState,
        onError
    );

    const avatarsProviderValue = useMemo(
        () => ({ avatarsState, setAvatarsState, scheduleAvatarFetch }),
        [avatarsState, setAvatarsState, scheduleAvatarFetch, onError]
    );

    // Delay modal display to avoid breaking SSR
    useEffect(() => {
        const timeout = window.setTimeout(
            () => setShowLogin(!isAuthenticated()),
            0);
        return () => window.clearTimeout(timeout);
    }, []);

    return compose([
        [UserContext, userProviderValue],
        [ErrorContext, errorProviderValue],
        [RulesContext, rulesProviderValue],
        [LeaguesContext, leaguesProviderValue],
        [MatchesContext, matchesProviderValue],
        [AvatarsContext, avatarsProviderValue]
    ],
        <>
            <NavBar setShowLogin={setShowLogin} />
            <Container className="mt-1">
                <Switch>
                    <Route exact path="/about" >
                        <About {...clubProps} />
                    </Route>
                    <Route exact path="/privacy" >
                        <Privacy {...clubProps} />
                    </Route>
                    <Route path="/profile" >
                        {isAuthenticated() ?
                            <Profile /> :
                            <Redirect to="/about" />
                        }
                    </Route>
                    <Route path="/leagues">
                        {isAuthenticated() ?
                            <Leagues /> :
                            <Redirect to="/about" />
                        }
                    </Route>
                    <Route path="/matches">
                        {isAuthenticated() ?
                            <Matches /> :
                            <Redirect to="/about" />
                        }
                    </Route>
                    <Route exact path="/manage">
                        {isAdministrator() ?
                            <Manage /> :
                            <Redirect to="/about" />
                        }
                    </Route>
                </Switch>
            </Container>
            <Login
                show={showLogin}
                setShow={setShowLogin}
                inviteEmail={inviteEmail}
            />
            <div className="fixed-bottom">
                <ErrorBar errors={errors} />
            </div>
        </>);

}

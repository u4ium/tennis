import { useState } from "react";
import { ErrorHandlerFunction } from "../requests/RequestFunction";

export default function useFormValidateOnSubmit(
    onSubmit: () => Promise<boolean>,
    onError: ErrorHandlerFunction
) {
    const [validated, setValidated] = useState(false);
    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (event.currentTarget.checkValidity() === false) {
            event.stopPropagation();
            setValidated(true);
        } else if (onSubmit) {
            setValidated(true);
            try {
                if (await onSubmit()) {
                    setValidated(false);
                }
            } catch (err) {
                onError(err);
            }
        } else {
            setValidated(true);
        }
    };

    return { validated, setValidated, handleSubmit };
}

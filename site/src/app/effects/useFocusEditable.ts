import { useEffect, useRef, useState } from "react";
import { FormElement } from "../forms/types/FormElements";
import { FormActiveData } from "../forms/types/FormElements";

type InputRefs<T> = Record<keyof T, React.MutableRefObject<FormElement>>;

/**
 * Focus any input elements that have just become editable
 */
export default function useFocusEditable<T>(formActiveData: FormActiveData<T>) {
    const inputRefs = Object.keys(formActiveData).reduce(
        (p, k) => ({ ...p, [k]: useRef(null) }),
        {} as InputRefs<T>);
    const [
        prevFormActiveData,
        setPrevFormActiveData
    ] = useState(formActiveData);
    useEffect(() => {
        (Object.entries(formActiveData) as Array<[keyof T, boolean]>).forEach(
            ([k, v]) => {
                if (v && v !== prevFormActiveData[k]) {
                    inputRefs?.[k].current?.focus();
                }
            });
        setPrevFormActiveData(formActiveData);
    }, [formActiveData, inputRefs, prevFormActiveData, setPrevFormActiveData]);

    return inputRefs;
}

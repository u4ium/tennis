import { useState } from "react";

export default function useFormData<T>(initialData: T) {
    const [formData, setFormData] = useState<T>(initialData);
    const updateFormData = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newData = { [event.target.name]: event.target.value };
        setFormData((prevFormData) => ({ ...prevFormData, ...newData }));
    };
    return { formData, setFormData, updateFormData };
}

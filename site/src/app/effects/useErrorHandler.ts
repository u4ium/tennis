import { useEffect, useState } from "react";

export default function useErrorHandler() {
    const [newError, setNewError] = useState(false);
    const [errors, setErrors] = useState<Error[]>([]);
    const [timeouts, setTimeouts] = useState<number[]>([]);

    const onError = (error: Error) => {
        setNewError(true);
        setErrors((prevErrors) => prevErrors.concat([error]));
    };

    useEffect(() => {
        if (newError) {
            setTimeouts((prevTimeouts) =>
                prevTimeouts.concat([
                    window.setTimeout(
                        () => setErrors((prevErrors) => prevErrors.slice(1)),
                        5000)
                ]));
        }
        setNewError(false);
    }, [setNewError, errors, setErrors, setTimeouts]);

    useEffect(() => () => timeouts.forEach((timeout) =>
        window.clearTimeout(timeout)), []);

    return { errors, onError };
}

import { useEffect, useState } from "react";
import { AvatarMap } from "../contexts/AvatarsContext";
import { getAvatar } from "../requests/avatar";

export default function useCachedAvatars(
    avatarsState: AvatarMap,
    setAvatarsState: React.Dispatch<React.SetStateAction<AvatarMap>>,
    onError: (error: Error) => void,
) {
    const [fetchList, setFetchList] = useState<Set<number>>(new Set());
    const scheduleAvatarFetch = (id: number) => {
        if (id) { setFetchList((p) => p.add(id)); }
    };

    const fetchAvatar = async (id: number) => {
        try {
            const avatar = await getAvatar({ id });
            setAvatarsState((p) => {
                p[id] = avatar;
                return { ...p };
            });
        } catch (e) {
            onError(e);
        }
    };

    useEffect(
        () => {
            fetchList.forEach((id) => !avatarsState[id] && fetchAvatar(id));
        },
        [fetchList, ]
    );

    return { scheduleAvatarFetch };
}

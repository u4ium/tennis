import { IMatchPlayer, INamed, ISet } from "../common/model/match/MatchModel";
import { IDoublesMatch, IMatch, ISinglesMatch } from "../common/model/match/MatchModel";

export const toTitleCase = (s: string): string =>
    (s[0] || "").toUpperCase() + s.slice(1);

export const extract = <T>(properties: Record<keyof T, boolean>) =>
    <TActual extends T>(value: TActual) => {
        const result = {} as T;
        for (const [property, keep] of
            Object.entries(properties) as Array<[keyof T, boolean]>) {
            if (keep) {
                result[property] = value[property];
            }
        }
        return result;
    };

export const escapeRegex = (text: string) =>
    text.replace(/[[\]{}()*+?.\\^$|]/g, "\\$&");

// ~!@#$%^&*()_ +-=`{}|:"<>?,./;'[]\`

export const FIFTEEN_MINUTES_IN_SECONDS = 900;

export function toSeconds(time: string) {
    const [hours, minutes] = time.split(":").map(Number);
    return hours * 3600 + minutes * 60;
}

export function toValidationDate(date: Date | string) {
    if (typeof date === "object" && "toISOString" in date) {
        date = date.toISOString();
    }
    return date.slice(0, 10);
}

export function nextYear(today: Date) {
    const ret = new Date(today.getTime());
    ret.setFullYear(today.getFullYear() + 1);
    return ret;
}

export function didSeedWin(sets: ISet[]) {
    // TODO: just use last set score?
    return 0 < sets.reduce((p, set) =>
        p + (set.seedScore > set.challengerScore ? 1 : -1),
        0);
}

export function didWinMatch(userWasSeed: boolean, match: IMatch) {
    return userWasSeed
        ? didSeedWin(match.sets)
        : !didSeedWin(match.sets);
}

export function wasMatchSeed(
    userId: number,
    isDoubles: boolean,
    match: IMatch
) {
    return (isDoubles
        ? (userId === (match as IDoublesMatch).seed.player1
            || userId === (match as IDoublesMatch).seed.player2)
        : userId === (match as ISinglesMatch).seed.player
    );
}

export function toShortName(named?: INamed) {
    return `${named?.firstName?.[0]}. ${named?.lastName}`;
}

export function toShortNames(player: IMatchPlayer) {
    if ("Player" in player) {
        return toShortName(player.Player);
    }
    if ("Player1" in player && "Player2" in player) {
        return `${toShortName(
            player.Player1
        )} and ${toShortName(
            player.Player2
        )}`;
    }
    return "Unknown";
}

export function toPlayerName(player: IMatchPlayer) {
    if ("Player" in player) {
        return player.Player.firstName;
    }
    if ("Player1" in player && "Player2" in player) {
        return `${player.Player1.firstName} and ${player.Player2.firstName}`;
    }
    return "Unknown";
}

export function eqSet(as: Set<number>, bs: Set<number>) {
    if (as.size !== bs.size) { return false; }
    for (const a of as) { if (!bs.has(a)) { return false; } }
    return true;
}

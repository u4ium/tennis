import { IAvatar, INewAvatar } from "../common/model/avatar/Avatar";
import ILeague from "../common/model/league/LeagueModel";
import { IDoublesPlayer, IMatch, ISinglesPlayer } from "../common/model/match/MatchModel";
import IRule from "../common/model/rule/RuleModel";
import { ISchedule } from "../common/model/schedule/Schedule";
import { IMatches } from "./contexts/MatchesContext";
import { IUser } from "./models/User";
import { extract } from "./utils";

export interface ClubProps {
    id: number;
    name: string;
    shortName: string;
    website: string;
    owner: number;
}

export const extractClubProps = extract<ClubProps>({
    id: true,
    name: true,
    shortName: true,
    website: true,
    owner: true,
});

export const allViews = [
    "about",
    "privacy",
    "profile",
    "leagues",
    "matches",
    "manage"
] as const;
export type View = typeof allViews[number];
export const isValidView = (view: string) => view in allViews;
export const defaultView: View = "about";

const authViews = allViews.slice(2);
const unauthorizedViews = allViews.slice(0, 2);
export const isAuthorizedView = (view: string): boolean =>
    authViews.includes(view as View);
export const isUnauthorizedView = (view: string): boolean =>
    unauthorizedViews.includes(view as View);

export interface SeasonMatches<T = ISinglesPlayer | IDoublesPlayer> {
    id: number;
    matches: Array<IMatch<T>>;
}

export interface LeagueMatches {
    id: number;
    isDoubles: boolean;
    Seasons: SeasonMatches[];
}

export interface AppProps extends ClubProps {
    user?: AuthUserProps;
    inviteEmail?: string;
    rules: IRule[];
    leagues: ILeague[];
    matches: IMatches;
}

export interface CommonProps {
    onError: (e: Error) => void;
    logout: () => void;
    user?: AuthUserProps;
}

export interface AuthUserProps extends IUser {
    id: number;
    bio: string;
    isAdministrator: boolean;
    avatar?: number;
    Avatar: IAvatar | INewAvatar;
    schedule?: ISchedule;
    skill: number;
}

export const extractAuthUserProps = extract<AuthUserProps>({
    id: true,
    email: true,
    isAdministrator: true,
    firstName: true,
    lastName: true,
    bio: true,
    avatar: true,
    Avatar: true,
    schedule: true,
    skill: true,
});

import { createContext } from "react";
import { IAvatar } from "../../common/model/avatar/Avatar";

export type AvatarMap = Record<number, IAvatar>;

export const AvatarsContext = createContext<{
    avatarsState: AvatarMap,
    setAvatarsState: React.Dispatch<React.SetStateAction<AvatarMap>>,
    scheduleAvatarFetch: (id: number) => void,
}>({
    avatarsState: [],
    setAvatarsState: () => undefined,
    scheduleAvatarFetch: (id) => undefined,
});

import { createContext } from "react";
import IRule from "../../common/model/rule/RuleModel";

export const RulesContext = createContext<{
    rulesState: IRule[],
    setRulesState: React.Dispatch<React.SetStateAction<IRule[]>>,
}>({ rulesState: [], setRulesState: () => undefined });

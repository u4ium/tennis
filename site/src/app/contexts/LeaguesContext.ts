import { createContext } from "react";
import ILeague from "../../common/model/league/LeagueModel";

export const LeaguesContext = createContext<{
    leaguesState: ILeague[],
    setLeaguesState: React.Dispatch<React.SetStateAction<ILeague[]>>,
}>({ leaguesState: [], setLeaguesState: () => undefined });

import { createContext } from "react";
import { AuthUserProps } from "../props";

export const UserContext = createContext<{
    userState: AuthUserProps,
    setUserState: React.Dispatch<React.SetStateAction<AuthUserProps>>,
}>({ userState: null, setUserState: () => undefined });

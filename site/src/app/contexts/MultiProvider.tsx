import React from "react";

export default function compose(
   contexts: Array<[React.Context<any>, any]>,
   children: React.ReactElement
) {
   return contexts.reduce((
      acc: React.ReactElement,
      [Context, value]: [React.Context<any>, any]
   ) => <Context.Provider value={value}>{acc}</Context.Provider>,
      children);
}

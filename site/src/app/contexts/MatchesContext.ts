import { createContext } from "react";
import {
    IDoublesMatch,
    ISinglesMatch
} from "../../common/model/match/MatchModel";
import { LeagueMatches } from "../props";

export interface IMatches {
    leagueMatches: LeagueMatches[];
    singlesMatches: ISinglesMatch[];
    doublesMatches: IDoublesMatch[];
}

export const MatchesContext = createContext<{
    matchesState: IMatches,
    setMatchesState: React.Dispatch<React.SetStateAction<IMatches>>,
}>({
    matchesState: {
        leagueMatches: [],
        singlesMatches: [],
        doublesMatches: [],
    }, setMatchesState: () => undefined
});

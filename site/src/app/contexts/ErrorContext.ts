import { createContext } from "react";

export const ErrorContext = createContext<{
    errors: Error[], onError: (err: Error) => void
}>({ errors: [], onError: (err: Error) => undefined });

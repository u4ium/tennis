interface IResizeImageOptions {
    maxSize: number;
    file: File;
}

export function resizeImage(settings: IResizeImageOptions): Promise<string> {
    const file = settings.file;
    const maxSize = settings.maxSize;
    const reader = new FileReader();
    const image = new Image();
    const canvas = document.createElement("canvas");
    /*
    const dataURItoBlob = (dataURI: string) => {
        const bytes = dataURI.split(",")[0].indexOf("base64") >= 0 ?
            atob(dataURI.split(",")[1]) :
            unescape(dataURI.split(",")[1]);
        const mime = dataURI.split(",")[0].split(":")[1].split(";")[0];
        const max = bytes.length;
        const ia = new Uint8Array(max);
        for (let i = 0; i < max; i++) { ia[i] = bytes.charCodeAt(i); }
        return new Blob([ia], { type: mime });
    };
    */
    const resize = () => {
        let sx = 0;
        let sy = 0;
        let sw = image.width;
        let sh = image.height;

        if (image.width > image.height) {
            if (image.height > maxSize) {
                sw = image.height;
                sx += (image.width - sw) / 2;
            }
        } else {
            if (image.width > maxSize) {
                sh = image.width;
                sy += (image.height - sh) / 2;
            }
        }

        canvas.width = maxSize;
        canvas.height = maxSize;
        canvas.getContext("2d").drawImage(image,
            sx, sy,
            sw, sh,
            0, 0,
            maxSize, maxSize);
        const dataUrl = canvas.toDataURL("image/jpeg");
        // return dataURItoBlob(dataUrl);
        return dataUrl;
    };

    return new Promise((ok, no) => {
        if (!file.type.match(/image.*/)) {
            return no(new Error("Not an image"));
        } else if (window.File && window.FileReader && window.FileList) {
            reader.onload = (readerEvent: any) => {
                image.onload = () => ok(resize());
                image.src = readerEvent.target.result;
            };
            return reader.readAsDataURL(file);
        } else {
            return no(new Error("Unsupported browser: File API missing"));
        }
    });
}

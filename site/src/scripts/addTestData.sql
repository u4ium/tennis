LOAD DATA LOCAL INFILE './src/scripts/test_data/testInvites.csv'
    INTO TABLE Invite
    FIELDS TERMINATED BY ','
    IGNORE 1 LINES
    (`email`, `code`, `confirmed`);

LOAD DATA LOCAL INFILE './src/scripts/test_data/testUsers.csv'
    INTO TABLE User
    FIELDS TERMINATED BY ','
    IGNORE 1 LINES (
        `id`,
        `email`,
        `first_name`,
        `last_name`,
        `bio`,
        `is_administator`,
        `password_salt`,
        `password_hash`
    );

LOAD DATA LOCAL INFILE './src/scripts/test_data/testClubs.csv'
    INTO TABLE Club 
    FIELDS TERMINATED BY ','
    IGNORE 1 LINES 
    (`name`, `short_name`, `website`, `owner`);

LOAD DATA LOCAL INFILE './src/scripts/test_data/testRules.csv'
    INTO TABLE Rule 
    FIELDS TERMINATED BY ','
    IGNORE 1 LINES (
    `name`,
    `sets_to_win`,
    `set_win_score`,
    `tiebreak_win_score`
);

LOAD DATA LOCAL INFILE './src/scripts/test_data/testSinglesLeagues.csv'
    INTO TABLE SinglesLeague
    FIELDS TERMINATED BY ','
    IGNORE 1 LINES 
    (`name`, `about`, `club`, `rule`);

LOAD DATA LOCAL INFILE './src/scripts/test_data/testSinglesPlayers.csv'
    INTO TABLE SinglesPlayer
    FIELDS TERMINATED BY ','
    IGNORE 1 LINES
     (`skill`, `privilege`, `user`, `league`);

--LOAD DATA LOCAL INFILE ./testSeasons.csv INTO TABLE Season FIELDS TERMINATED BY ',' IGNORE 1 LINES;
--LOAD DATA LOCAL INFILE ./testMatches.csv INTO TABLE Match FIELDS TERMINATED BY ',' IGNORE 1 LINES;
#/bin/bash:/usr/bin/bash
status=0

IN=.test
OUT=.expect

if [[ $# -ne 1 ]]
then  
  echo "usage $0 <PORT>"
  exit 1
fi

port=$1

for q in `ls -d test/server` 
do
  tests=`ls $q/*$IN 2>/dev/null`
  if [ $? -ne 0 ]
  then
    status=1
    echo -e "\e[31mno tests for $q\e[0m" >&2
  else      
    for ex in $tests
    do
      ex=${ex%$IN}
      dif=`./curler.sh $port < ${ex}$IN | diff -w - ${ex}$OUT`
      if [ $? -ne 0 ]
      then
        status=1
        echo -e "\e[31m$q test ${ex#q*/} failed\e[0m" >&2
        echo "$dif"
      else
        echo -e "\e[32m$q test ${ex#q*/} passed\e[0m" >&2
      fi
    done
  fi
done

exit $status

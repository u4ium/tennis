const crypto = require("crypto");

const SALT_LENGTH = 16;

function saltAndHash(password) {
    const salt = crypto.randomBytes(SALT_LENGTH).toString("base64");
    return { passwordSalt: salt, passwordHash: hashWithSalt(password, salt) };
}

function hashWithSalt(password, salt) {
    const hash = crypto.createHmac("sha512", salt);
    hash.update(password);
    return hash.digest("base64");
}

console.log(saltAndHash("Password1"));
console.log(saltAndHash("Password2"));
console.log(saltAndHash("Password1"));
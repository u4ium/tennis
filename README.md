# tennis

## Install and Run

### For Development

Use VSCode for the best experience.

- Clone this code and open the root of this repo as a workspace.
- Run the included Dev Container.
- Start webpack in watch mode:
  - `yarn build` OR
  - <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>B</kbd>
- Start the server with nodemon:

  - In a separate terminal: `yarn startDev` OR
  - <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>D</kbd> then <kbd>Enter</kbd>

- Open:
  - http://localhost:8000 to view the website
  - http://localhost:8080 to view the SQL "adminer"

Alternatively, without VSCode, run (in 3 separate terminals):

```sh
docker-compose up
docker exec nodejs yarn build
docker exec nodejs yarn startDev
```

Known issue: browser-sync does not work.
